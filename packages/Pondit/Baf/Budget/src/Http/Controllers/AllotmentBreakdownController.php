<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\AllotmentBreakdown;

class AllotmentBreakdownController extends Controller
{
    public function index()
    {
        $data = AllotmentBreakdown::latest()->get();
        return view('budget::allotment-brackdown.index', compact('data'));
    }
    public function create()
    {
        $budgetAllotments = BudgetAllotment::all();
        return view('budget::allotment-brackdown.create',compact('budgetAllotments'));
    }
    public function store(Request $request)
    {
        try
        {   

            $totalBaseAllotment = $request->base_bsr + $request->base_mtr + $request->base_zhr + $request->base_bbd + $request->base_pkp + $request->base_cxb + $request->base_air_hq + $request->base_201mu + $request->base_201u + $request->base_bru + $request->base_mru ;
            $budgetAllotment = BudgetAllotment::find($request->budgetcode_id);

            $totalSpent = $totalBaseAllotment + $request->spent_fc_army +$request->spent_airhq ;
            $balance =  $budgetAllotment->balance - $totalSpent ;
            $data = [
                'budget_allotment_id'           => $request->budgetcode_id,
                'old_code'                      => $budgetAllotment->budgetcode->oldcode,
                'new_code'                      => $budgetAllotment->budgetcode->newcode,
                'description'                   => $budgetAllotment->description,
                'fin_year'                      => $budgetAllotment->fin_year,
                'range'                         => $budgetAllotment->range_name,
                'activity_type'                 => $budgetAllotment->activity_type,
                'intial_budget'                 => $budgetAllotment->initial_amount,
                'base_bsr'                      => $request->base_bsr,
                'base_mtr'                      => $request->base_mtr,
                'base_zhr'                      => $request->base_zhr,
                'base_bbd'                      => $request->base_bbd,
                'base_pkp'                      => $request->base_pkp,
                'base_cxb'                      => $request->base_cxb,
                'base_air_hq'                   => $request->base_air_hq,
                'base_201mu'                    => $request->base_201mu,
                'base_201u'                     => $request->base_201u,
                'base_bru'                      => $request->base_bru,
                'base_mru'                      => $request->base_mru,
                'total_base_allotment'          => $totalBaseAllotment,
                'spent_fc_army'                 => $request->spent_fc_army,
                'spent_base_unit'               => $totalBaseAllotment,
                'spent_airhq'                   => $request->spent_airhq,
                'total_spent'                   => $totalSpent,
                'balance'                       => $balance,
            ];

            // dd($data);   
 
            // if ($files = $request->file('letter')) {
            //     $currentDate = Carbon::now()->toDateString();
            //     $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
            //     $dir = "storage/letter";
            //     $files->move($dir, $imageName);
            //     $data['letter'] ='storage/letter/' . $imageName;
            // }    
        

            AllotmentBreakdown::create($data);

            return redirect()->route('allotment_brackdown.index')->withMessage('Entity Create Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = AllotmentBreakdown::findOrFail($id);
        return \view('budget::allotment-brackdown.show', \compact('data'));
    }
    public function edit($id)
    {
        $budgetAllotments = BudgetAllotment::all();
        $data = AllotmentBreakdown::findOrFail($id);
        return \view('budget::allotment-brackdown.edit', compact('data','budgetAllotments'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = AllotmentBreakdown::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                
            $totalBaseAllotment = $request->base_bsr + $request->base_mtr + $request->base_zhr + $request->base_bbd + $request->base_pkp + $request->base_cxb + $request->base_air_hq + $request->base_201mu + $request->base_201u + $request->base_bru + $request->base_mru ;
            $budgetAllotment = BudgetAllotment::find($request->budget_allotment_id);
            $totalSpent = $totalBaseAllotment + $request->spent_fc_army +$request->spent_airhq ;
            $balance =  $budgetAllotment->balance - $totalSpent ;
            $data = [
                'budget_allotment_id'           => $request->budget_allotment_id,
                'old_code'                      => $budgetAllotment->budgetcode->oldcode,
                'new_code'                      => $budgetAllotment->budgetcode->newcode,
                'description'                   => $budgetAllotment->description,
                'fin_year'                      => $budgetAllotment->fin_year,
                'range'                         => $budgetAllotment->range_name,
                'activity_type'                 => $budgetAllotment->activity_type,
                'intial_budget'                 => $budgetAllotment->initial_amount,
                'base_bsr'                      => $request->base_bsr,
                'base_mtr'                      => $request->base_mtr,
                'base_zhr'                      => $request->base_zhr,
                'base_bbd'                      => $request->base_bbd,
                'base_pkp'                      => $request->base_pkp,
                'base_cxb'                      => $request->base_cxb,
                'base_air_hq'                   => $request->base_air_hq,
                'base_201mu'                    => $request->base_201mu,
                'base_201u'                     => $request->base_201u,
                'base_bru'                      => $request->base_bru,
                'base_mru'                      => $request->base_mru,
                'total_base_allotment'          => $totalBaseAllotment,
                'spent_fc_army'                 => $request->spent_fc_army,
                'spent_base_unit'               => $totalBaseAllotment,
                'spent_airhq'                   => $request->spent_airhq,
                'total_spent'                   => $totalSpent,
                'balance'                       => $balance,
            ];
    
 
            // if ($files = $request->file('letter')) {
            //     $currentDate = Carbon::now()->toDateString();
            //     $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
            //     $dir = "storage/letter";
            //     $files->move($dir, $imageName);
            //     $prepareData['letter'] ='storage/letter/' . $imageName;
            // }  

            $update = AllotmentBreakdown::where('id',$id)->update($data);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('allotment_brackdown.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = AllotmentBreakdown::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('allotment_brackdown.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }
}