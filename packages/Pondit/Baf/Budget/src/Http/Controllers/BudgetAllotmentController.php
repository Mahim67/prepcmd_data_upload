<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;
use Pondit\Baf\MasterData\Models\Range;

class BudgetAllotmentController extends Controller
{
    public function index()
    {
        $data = BudgetAllotment::latest()->get();
        return view('budget::budget-allotment.index', compact('data'));
    }
    public function create()
    {
        $budgetcodes = Budgetcode::pluck('newcode','id')->toArray();
        $ranges = Range::pluck('title','id')->toArray();
        return view('budget::budget-allotment.create',compact('budgetcodes','ranges'));
    }
    public function store(Request $request)
    {
        try
        {            

            $budgetcode = Budgetcode::find($request->budgetcode_id);
            if(is_null($budgetcode))
                throw new Exception("Budgetcode Not Found", 404);
                
            $range = Range::find($request->range_id);
            if(is_null($range))
                throw new Exception("Range Not Found", 404);

            $prepareData = [
                'fin_year'                  => $this->currentFY(),
                'budgetcode_id'             => $budgetcode->id,
                'description'               => $budgetcode->description,
                'range_name'                => $range->title,
                'range_id'                  => $range->id,
                'activity_type'             => $budgetcode->budget_type,
                'initial_amount'            => $request->initial_amount,
                'total_budgetcode_amount'   => $request->initial_amount,
                'balance'                   => $request->initial_amount,
            ];

            BudgetAllotment::create($prepareData);

            return redirect()->route('budget_allotment.index')->withMessage('Budget Allotment Create Successfull');
        }
        catch (Exception $th)
        {
            dd($th->getMessage());
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetAllotment::findOrFail($id);
        $suplimentries = SuplimentaryBudgetAllotment::where('budgetcode_id',$data->budgetcode_id)->get(); 
        return \view('budget::budget-allotment.show', \compact('data','suplimentries'));
    }
    public function edit($id)
    {
        $budgetcodes = Budgetcode::pluck('newcode','id')->toArray();
        $ranges = Range::pluck('title','id')->toArray();
        $data = BudgetAllotment::findOrFail($id);
        return \view('budget::budget-allotment.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{

            $budgetcode = Budgetcode::find($request->budgetcode_id);
            if(is_null($budgetcode))
                throw new Exception("Budgetcode Not Found", 404);
                
            $range = Range::find($request->range_id);
            if(is_null($range))
                throw new Exception("Range Not Found", 404);

            $prepareData = [
                'fin_year'                  => $this->currentFY(),
                'budgetcode_id'             => $budgetcode->id,
                'description'               => $request->description,
                'range_name'                => $range->title,
                'range_id'                  => $range->id,
                'activity_type'             => $budgetcode->budget_type,
                'initial_amount'            => $request->initial_amount,
                'total_budgetcode_amount'   => $request->initial_amount,
                'balance'                   => $request->initial_amount,
            ];

            BudgetAllotment::where('id', $id)->update($prepareData);
                

            return redirect()
                ->route('budget_allotment.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            
            $data = BudgetAllotment::findOrFail($id);
            $suplimentries = SuplimentaryBudgetAllotment::where('budgetcode_id',$data->budgetcode_id)->get(); 
            
            foreach($suplimentries  as $suplimentry){
                $suplimentry->delete();
            }

            $data->delete();
    
            return redirect()
                        ->route('budget_allotment.index')
                        ->withSuccess('Entity has been deleted successfully!');

        } catch (\Exception $th) {

            return redirect()->back()->withErrors($th->getMessage());
        }

    }

    public function rangeWiseReport()
    {
        $data = BudgetAllotment::all()->groupBy('range_name');
        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        return view('budget::budget-allotment.range-wise-report',compact('data','maxSuplymentry'));
    }

    public function TypeWiseReport()
    {
        $data = BudgetAllotment::all()->groupBy('activity_type');
        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        return view('budget::budget-allotment.type-wise-report',compact('data','maxSuplymentry'));
    }


    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }

    public function getBudgetInfo($id)
    {
        $data = BudgetAllotment::where('id', $id)->first();
        dd($data);
        $transferAmount  = $data->transfer;

        return \response()->json([
            'budget' => $data,
            'transfer' => $transferAmount

        ]);
    }
    
    
}