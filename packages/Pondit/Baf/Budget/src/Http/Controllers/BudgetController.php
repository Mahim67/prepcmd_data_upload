<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\Budget;
use Illuminate\Support\Facades\Session;
use App\Imports\initialBudgetExcelImport;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use App\Imports\SuplimentaryBudgetExcelImport;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;

class BudgetController extends Controller
{
    public function index()
    {
        $data = Budget::latest()->get();

        $sessionData = [];
        if (Session::has('errorMessage')) {
            $sessionData = Session::get('errorMessage[]');
        }

        return view('budget::budget.index', compact('data'));
    }
    
    public function initialBudgetCreate()
    {
        return view('budget::budget.initial-budget-create');
    }
    public function initialBudgetStore(Request $request)
    {
        try
        {   
            $data = [
                'fin_year'                  => $this->currentFY(),
                'title'                     => $request->title,
                'amount'                    => $request->amount,
                'reference_no'              => $request->reference_no,
                'type'                      => 'Initial Budget',
            ];
 
            if ($files = $request->file('letter')) {
                $data['letter'] =$this->fileUpload($files, 'storage/letter');
            }    
            if ($files = $request->file('allotment_excel')) {
                $data['excel_path'] =$this->fileUpload($files, 'storage/excels');
            }    
            DB::beginTransaction();

            $budget = Budget::create($data);

            if(!is_null($budget->excel_path)){
                $excelUpload = $this->importStore($budget);

                if($excelUpload['status'] == 'NotOk' )  
                    throw new Exception($excelUpload['message'], 403);

            }
            
            DB::commit();
            if (is_null($excelUpload['message'])) {
                return redirect()->route('budget.index')->withSuccess('Entity Created Successfull');
            }
            else{
                return redirect()->route('budget.index')->withErrors($excelUpload['message']);
            }
        }
        catch (Exception $th)
        {
            DB::rollback();

            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }

    public function suplimentaryBudgetCreate()
    {
        return view('budget::budget.suplimentary-budget-create');
    }
    public function suplimentaryBudgetStore(Request $request)
    {
        try
        {   
            $data = [
                'fin_year'                  => $this->currentFY(),
                'title'                     => $request->title,
                'amount'                    => $request->amount,
                'reference_no'              => $request->reference_no,
                'type'                      => 'Suplimentary Budget',
            ];
 
            if ($files = $request->file('letter')) {
                $data['letter'] =$this->fileUpload($files, 'storage/letter');
            }    
            if ($files = $request->file('suplimentary_allotment_excel')) {
                $data['excel_path'] =$this->fileUpload($files, 'storage/excels');
            }    
            DB::beginTransaction();

            $budget = Budget::create($data);

            if(!is_null($budget->excel_path)){
                $excelUpload = $this->importSuplimentaryStore($budget);

                if($excelUpload['status'] == 'NotOk' )  
                    throw new Exception($excelUpload['message'], 403);

            }
            DB::commit();

            if (is_null($excelUpload['message'])) {

                return redirect()->route('budget.index')->withSuccess('Entity Create Successfull');
            }
            else{
                return redirect()->route('budget.index')->withErrors($excelUpload['message']);
            }
        }
        catch (Exception $th)
        {
            DB::rollback();
            // dd($th->getMessage());
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }



    public function show($id)
    {
        $data = Budget::findOrFail($id);
        return \view('budget::budget.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = Budget::findOrFail($id);
        return \view('budget::budget.edit', compact('data'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = [   
                'fin_year'                  => $this->currentFY(),
                'amount'                    => $request->amount,
                'reference_no'              => $request->reference_no,
                'activity_type'             => $request->activity_type,
            ];
 
            if ($files = $request->file('letter')) {
                $data['letter'] =$this->fileUpload($files, 'storage/letter');
            }    
            if ($files = $request->file('allotment_excel')) {
                $data['excel_path'] =$this->fileUpload($files, 'storage/excels');
            }   

            $update = Budget::where('id',$id)->update($data);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('budget.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            dd($th->getMessage());
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Budget::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('budget.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }

    public function fileUpload($files, $path){
        $currentDate = Carbon::now()->toDateString();
        $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
        $files->move($path, $imageName);

        return $path . '/' . $imageName;
    }

    public function importStore($budget)
    {
        try {
            $filePathData = $budget->excel_path;
            $columnsInfo  = Budget::$modelColumnsInfo;
            
            $validationInfo = [
                // "*.name_of_equipment"                       => ['required'],
                // "*.qty"                                     => ['nullable'],
                // "*.estimated_unit_price"                    => ['nullable','min:0','max:99999999','regex:/^\d+(\.\d{1,2})?$/'],
                // "*.total_cost"                              => ['nullable','min:0','max:99999999','regex:/^\d+(\.\d{1,2})?$/'],

            ];
            $whiteListedExcel = [
                //excel column => 'db column'
                'range'         => 'range_name',
                
            ];
            
            $hiddenParseData = [
                'fin_year'      => $this->currentFY(),
                'budget_id'     => $budget->id
            ];



            $itemsInport = new initialBudgetExcelImport(BudgetAllotment::class, $columnsInfo, $validationInfo, $whiteListedExcel, $hiddenParseData);
            $itemsInport->import($filePathData);

            return [
                'status'    => 'ok',
                'message'   => session('excelErrors'),
            ];
            
        }
        catch (Exception $e) {
            return [
                'status'    => 'NotOk',
                'message'   => $e->getMessage()
            ];
        }
    }

    public function importSuplimentaryStore($budget)
    {
        try {
            $filePathData = $budget->excel_path;
            $columnsInfo  = SuplimentaryBudgetAllotment::$modelColumnsInfo;
            
            $validationInfo = [];
            $whiteListedExcel = [
                //excel column => 'db column'
                'range'         => 'range_name',
                
            ];
            
            $hiddenParseData = [
                'fin_year'      => $this->currentFY(),
                'budget_id'     => $budget->id
            ];



            $itemsInport = new SuplimentaryBudgetExcelImport(SuplimentaryBudgetAllotment::class, $columnsInfo, $validationInfo, $whiteListedExcel, $hiddenParseData);
            $itemsInport->import($filePathData);

            return [
                'status'    => 'ok',
                'message'   => session('excelErrors'),
            ];
            
        }
        catch (Exception $e) {
            return [
                'status'    => 'NotOk',
                'message'   => $e->getMessage()
            ];
        }
    }
}