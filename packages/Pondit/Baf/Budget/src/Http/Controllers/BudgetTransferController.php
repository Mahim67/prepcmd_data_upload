<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\BudgetTransfer;


class BudgetTransferController extends Controller
{
    public function index()
    {
        $data = BudgetTransfer::latest()->get();
        return view('budget::budget-transfer.index', compact('data'));
    }
    public function create()
    {
        $budgetCodes = BudgetAllotment::all();
        return view('budget::budget-transfer.create',compact('budgetCodes'));
    }
    public function store(Request $request)
    {
        try
        {   
                
            $data = [

                'from_code'                 => $request->from_code,
                'to_code'                   => $request->to_code,
                'fin_year'                  => $this->currentFY(),
                'amount'                    => $request->amount,
                'transfer_at'               => Carbon::now()->toDateTimeString(),
                'transfer_by'               => Auth::user()->id ?? 1 ,
                'reference_no'              => $request->reference_no,
            ];

            $path = storage_path().'/app/public/letter/';

            if (!file_exists($path)) {
                \File::makeDirectory($path, $mode = 0777, true, true);
            }

            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $data['letter'] ='storage/letter/' . $imageName;
            }

            $transter = BudgetTransfer::create($data);

            $transferJSON = json_encode($transter);
            
            $fromCodeAllotment = BudgetAllotment::where('budgetcode_id',$request->from_code)->first();
            $fromCodeUpdateValue = [

                'total_budgetcode_amount'       => $fromCodeAllotment->total_budgetcode_amount - $transter->amount ,
                'balance'                       => $fromCodeAllotment->balance -  $transter->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $fromCodeAllotment->update($fromCodeUpdateValue);

            $toCodeAllotment = BudgetAllotment::where('budgetcode_id',$request->to_code)->first();
            $toCodeUpdateValue = [

                'total_budgetcode_amount'       => $toCodeAllotment->total_budgetcode_amount + $transter->amount ,
                'balance'                       => $toCodeAllotment->balance +  $transter->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $toCodeAllotment->update($toCodeUpdateValue);

            return redirect()->route('budget-transfer.index')->withSuccess('Budget Transfer Successfull');
        }
        catch (Exception $th)
        {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = BudgetTransfer::findOrFail($id);
        return \view('budget::budget-transfer.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = BudgetTransfer::findOrFail($id);
        return \view('budget::budget-transfer.edit', compact('data'));
    }
    public function update(Request $request,  $id)
    {
        try{

            $data = BudgetTransfer::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);

            $prepareData = [
                'amount'                    => $request->amount,
                'reference_no'              => $request->reference_no,
            ];

            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $prepareData['letter'] ='storage/letter/' . $imageName;
            }

            $update = BudgetTransfer::where('id',$id)->update($prepareData);
            if(!$update)
                throw new Exception("Unable To Update", 403);


            $transfereddata = BudgetTransfer::find($id);

            $transferJSON = json_encode($transfereddata);
        
            $fromCodeAllotment = BudgetAllotment::where('budgetcode_id',$transfereddata->from_code)->first();
            $fromCodeUpdateValue = [

                'total_budgetcode_amount'       => ($fromCodeAllotment->total_budgetcode_amount + $data->amount) - $transfereddata->amount ,
                'balance'                       => ($fromCodeAllotment->balance + $data->amount) -  $transfereddata->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;



            $fromCodeAllotment->update($fromCodeUpdateValue);

            $toCodeAllotment = BudgetAllotment::where('budgetcode_id',$transfereddata->to_code)->first();
            $toCodeUpdateValue = [

                'total_budgetcode_amount'       => ($toCodeAllotment->total_budgetcode_amount - $data->amount ) + $transfereddata->amount ,
                'balance'                       => ($toCodeAllotment->balance - $data->amount ) +  $transfereddata->amount,
                'transfered_budget_detail'      => $transferJSON,
            ] ;
            $toCodeAllotment->update($toCodeUpdateValue);

            return redirect()
                ->route('budget-transfer.index')
                ->withSuccess('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = BudgetTransfer::findOrFail($id);

        $fromCodeAllotment = BudgetAllotment::where('budgetcode_id',$data->from_code)->first();
        $fromCodeUpdateValue = [

            'total_budgetcode_amount'       => $fromCodeAllotment->total_budgetcode_amount + $data->amount ,
            'balance'                       => $fromCodeAllotment->balance + $data->amount,
            'transfered_budget_detail'      => null,
        ] ;



        $fromCodeAllotment->update($fromCodeUpdateValue);

        $toCodeAllotment = BudgetAllotment::where('budgetcode_id',$data->to_code)->first();
        $toCodeUpdateValue = [

            'total_budgetcode_amount'       => $toCodeAllotment->total_budgetcode_amount - $data->amount ,
            'balance'                       => $toCodeAllotment->balance - $data->amount,
            'transfered_budget_detail'      => null,
        ] ;
        $toCodeAllotment->update($toCodeUpdateValue);
        
        $data->delete();

        return redirect()
                    ->route('budget-transfer.index')
                    ->withSuccess('Entity has been deleted successfully!');
    }

    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }
}
