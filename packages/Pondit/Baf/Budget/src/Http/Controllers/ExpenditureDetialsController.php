<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\ExpenditureDetials;
use Pondit\Baf\MasterData\Models\Range;

class ExpenditureDetialsController extends Controller
{
   public function index(){

       $expenditureDetials = ExpenditureDetials::latest()->get();
       return view('budget::expenditure-detials.index', compact('expenditureDetials'));
       
    }

    public function create(){
        $budgetCodes = Budgetcode::all();
        $ranges = Range::pluck('title')->toArray();
        return view('budget::expenditure-detials.create' ,compact('budgetCodes','ranges'));
    }

    public function store(Request $request){
        try 
        {
            $data = $request->except('_token');
            $data['fin_year'] = $this->currentFY();

            ExpenditureDetials::create($data);
            return redirect()->route('expenditure_detials.index')->withSuccess('Entity Create Successfull');

        }
         catch (Exception $th) 
         {
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }

    public function show($id){
        $expenditureDetial = ExpenditureDetials::find($id);
        return view('budget::expenditure-detials.show' ,compact('expenditureDetial'));
    }

    public function edit($id){

        $expenditureDetial = ExpenditureDetials::find($id);
        $budgetCodes = Budgetcode::all();
        $ranges = Range::pluck('title')->toArray();
        return view('budget::expenditure-detials.edit' ,compact('expenditureDetial','budgetCodes' , 'ranges'));
    }

    public function update(Request $request , $id){
        try 
        {
            $data = $request->except('_method','_token');

            ExpenditureDetials::where('id',$id)->update($data);

            return redirect()->route('expenditure_detials.index')->withSuccess('Entity Updated Successfull');
            
        }
        catch (Exception $th) 
        {
           return redirect()
                   ->back()
                   ->withErrors($th->getMessage());
       }
    }

    public function destroy($id)
    {
        try {
            $expenditureDetial = ExpenditureDetials::find($id);
            $expenditureDetial->delete();
            return redirect()
                        ->route('expenditure_detials.index')
                        ->withSuccess('Entity has been deleted successfully!');

        } 
        catch (Exception $th) 
        {
           return redirect()
                   ->back()
                   ->withErrors($th->getMessage());
       }

    }

    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }

}
