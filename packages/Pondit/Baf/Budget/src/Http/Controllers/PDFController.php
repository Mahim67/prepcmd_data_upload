<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetAllotment;

class PDFController extends Controller
{
   

    public function rangeWiseReportPDF()
    {
        $data = BudgetAllotment::all()->groupBy('range_name');
        $FY = $this->currentFY();
        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        $view =  view('budget::budget-allotment.range-wise-report-pdf',compact('data','FY','maxSuplymentry'))->render();
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($view);
        $mpdf->Output('Range-wise-report_'. time() . ".pdf", "D");


    }

    public function TypeWiseReportPDF()
    {
        $data = BudgetAllotment::all()->groupBy('activity_type');
        $FY = $this->currentFY();
        $maxSuplymentry = BudgetAllotment::max('total_no_suplimentary_budgets');
        $view =  view('budget::budget-allotment.type-wise-report-pdf',compact('data','FY','maxSuplymentry'))->render();
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($view);
        $mpdf->Output('Type-wise-report_'. time() . ".pdf", "D");

    }


    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }
    
    
}