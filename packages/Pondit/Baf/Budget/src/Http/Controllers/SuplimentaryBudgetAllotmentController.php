<?php

namespace Pondit\Baf\Budget\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\Budget;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\Budget\Models\Budgetcode;
use Pondit\Baf\Budget\Models\SuplimentaryBudgetAllotment;

class SuplimentaryBudgetAllotmentController extends Controller
{
    public function index()
    {
        $data = SuplimentaryBudgetAllotment::latest()->get();
        return view('budget::suplimentary-budget-allotment.index', compact('data'));
    }
    public function create()
    {
        $ranges = Range::pluck('title','id')->toArray();
        return view('budget::suplimentary-budget-allotment.create',compact('ranges'));
    }
    public function store(Request $request)
    {
        try
        {   
            $input                  =      $request->all();
            $range_id = $input['range_id'];
            $budgetcode_id = $input['budgetcode_id'];
            $suplimentary_amount = $input['suplimentary_amount'];

            $budgetData = [   
                'fin_year'                  => $this->currentFY(),
                'reference_no'              => $request->reference_no,
                'type'                      => 'Suplimentary Budget',
            ];

            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $budgetData['letter'] ='storage/letter/' . $imageName;
            }  
            DB::beginTransaction();

            $budget = Budget::create($budgetData);

            for($i=0; $i< count($budgetcode_id); $i++) {

                $lastId = SuplimentaryBudgetAllotment::orderBy('id','desc')->first();
                $lastId ? $insertId = $lastId->id : $insertId = 0 ;
                $id = $insertId + 1 ;

                $suplimentaryData= [
                    'id'                        => $id,
                    'budgetcode_id'             => $budgetcode_id[$i],
                    'range_id'                  => $range_id[$i],
                    'budget_id'                 => $budget->id,
                    'fin_year'                  => $this->currentFY(),
                    'suplimentary_amount'       => $suplimentary_amount[$i],
                ];

                $initialBudgetAllotment = BudgetAllotment::where('budgetcode_id',$budgetcode_id[$i])->first();
                if(is_null($initialBudgetAllotment)){
                    continue;
                }
                $SuplimentaryBudgetAllotment =  SuplimentaryBudgetAllotment::create($suplimentaryData);

                
                    
                    
                $suplimentary_budget_detail = json_encode($SuplimentaryBudgetAllotment);
                $total_no_suplimentary_budgets = $initialBudgetAllotment->total_no_suplimentary_budgets ? $initialBudgetAllotment->total_no_suplimentary_budgets+1 : 1 ;
                $total_suplimentary_amount  = $initialBudgetAllotment->total_suplimentary_amount != null ? $initialBudgetAllotment->total_suplimentary_amount + $suplimentary_amount[$i] : $suplimentary_amount[$i];
                $prepareData = [
                    'total_no_suplimentary_budgets'         => $total_no_suplimentary_budgets ,
                    'suplimentary_budget_detail'            => $suplimentary_budget_detail,
                    'total_suplimentary_amount'             => $total_suplimentary_amount,
                    'total_budgetcode_amount'               => $initialBudgetAllotment->initial_amount + $total_suplimentary_amount,
                ];
              
                BudgetAllotment::where('budgetcode_id',$budgetcode_id[$i])->update($prepareData);
    
            }

            DB::commit(); 
        
            return redirect()->route('suplimentary_budget_allotment.index')->withMessage('Entity Create Successfull');
        }
        catch (Exception $th)
        {
            DB::rollBack();
            dd($th->getMessage());
            return redirect()
                    ->back()
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        return \view('budget::suplimentary-budget-allotment.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        $budgetcodes = Budgetcode::pluck('newcode','id')->toArray();
        $ranges = Range::pluck('title','id')->toArray();
        return \view('budget::suplimentary-budget-allotment.edit', compact('data','budgetcodes','ranges'));
    }
    public function update(Request $request,  $id)
    {
        try{
            $data = SuplimentaryBudgetAllotment::find($id);
            if(is_null($data))
                throw new Exception("Data Not Found", 404);
                 
            $prepareData = [
                'budgetcode_id'             => $request->budgetcode_id,
                'range_id'                  => $request->range_id,
                'fin_year'                  => $this->currentFY(),
                'suplimentary_amount'       => $request->suplimentary_amount,
            ];
 
            if ($files = $request->file('letter')) {
                $currentDate = Carbon::now()->toDateString();
                $imageName =  $currentDate . '-' . uniqid() . '.' . $files->getClientOriginalExtension();
                $dir = "storage/letter";
                $files->move($dir, $imageName);
                $prepareData['letter'] ='storage/letter/' . $imageName;
            }  

            $update = SuplimentaryBudgetAllotment::where('id',$id)->update($prepareData);
            if(!$update)
                throw new Exception("Unable To Update", 403);
                

            return redirect()
                ->route('suplimentary_budget_allotment.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (Exception $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = SuplimentaryBudgetAllotment::findOrFail($id);
        $data->delete();

        return redirect()
                    ->route('suplimentary_budget_allotment.index')
                    ->withMessage('Entity has been deleted successfully!');
    }
    
    private function currentFY()
    {
        if (date('m') > 6) {
            $fin_year = (date('Y') + 1) . "-" . (date('Y') + 2);
        } else {
            $fin_year = date('Y') . "-" . (date('Y') + 1);
        }
        return $fin_year;
    }
}