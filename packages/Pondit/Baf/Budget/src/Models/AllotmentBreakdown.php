<?php

namespace Pondit\Baf\Budget\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllotmentBreakdown extends Model
{
    use HasFactory;
    protected $connection = 'budget';
    protected $guarded = [];
    protected $table = 'allotment_breakdowns';

}
