<?php

namespace Pondit\Baf\Budget\Models;

// use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\Budget\Models\Budgetcode as ModelsBudgetcode;

// use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetAllotment extends Model
{
    // use SoftDeletes, RecordSequenceable;
    
    // protected $dates    = ['deleted_at'];
    protected $connection = 'budget';
    protected $table    = 'budget_allotments';

    protected $guarded = [];
    
    public function budgetcode(){
        return $this->belongsTo(Budgetcode::class);
    }

    public function transfer()
    {
        return $this->hasMany(BudgetTransfer::class, 'budget_allotment_id');
    }

}
