@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Transfer">
<x-pondit-form action="{{route('allotment_brackdown.store')}}" >
        
        <div class='form-group row'>
            <label for="budget_allotment_id" class="col-sm-2 col-form-label">Budget Allotment</label>
            <div class='col-sm-4'>
                <select name="budget_allotment_id" id="budget_allotment_id" class="form-control">
                    <option value="">--Select Badget Code--</option>
                    @foreach ($budgetAllotments as $allotment)
                        <option value="{{ $allotment->id }}">{{ $allotment->budgetcode->newcode }} ({{ $allotment->range_name }})</option>    
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bsr" class="col-form-label">BSR</label>
                    <input type="text" name="base_bsr" id="base_bsr" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mtr" class="col-form-label">MTR</label>
                    <input type="text" name="base_mtr" id="base_mtr" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_zhr" class=" col-form-label">ZHR</label>
                    <input type="text" name="base_zhr" id="base_zhr" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bbd" class="col-form-label">BBD</label>
                    <input type="text" name="base_bbd" id="base_bbd" class="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_pkp" class=" col-form-label">PKB</label>
                    <input type="text" name="base_pkp" id="base_pkp" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_cxb" class="col-form-label">CXB</label>
                    <input type="text" name="base_cxb" id="base_cxb" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group '>
                    <label for="base_air_hq" class="col-form-label">AIR HQ</label>
                    <input type="text" name="base_air_hq" id="base_air_hq" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201mu" class="col-form-label">201 MU</label>
                    <input type="text" name="base_201mu" id="base_201mu" class="form-control" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_201u" class=" col-form-label">201 U</label>
                    <input type="text" name="base_201u" id="base_201u" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_bru" class=" col-form-label">BRU</label>
                    <input type="text" name="base_bru" id="base_bru" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="base_mru" class=" col-form-label">MRU</label>
                    <input type="text" name="base_mru" id="base_mru" class="form-control" />
                </div>
            </div>
            <div class="col-3">
                <div class='form-group'>
                    <label for="spent_fc_army" class=" col-form-label">Spent FC Army</label>
                    <input type="text" name="spent_fc_army" id="spent_fc_army" class="form-control" />
                </div>
            </div>
        </div>

        <div class='form-group row'>
            <label for="spent_airhq" class="col-sm-2 col-form-label">Spent AIR HQ</label>
            <div class='col-sm-10'>
                <input type="text" name="spent_airhq" id="spent_airhq" class="form-control" />
            </div>
        </div>


        {{-- <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-10'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
        </div> --}}

       
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('allotment_brackdown.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection

@push('css')
<style>
    label{
        font-weight: bold
    }
</style>
    
@endpush
