@extends('pondit-limitless::layouts.master')

@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Allotment Brackdown">
    <x-pondit-datatable tableClass="table-responsive">
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Old Code')}}</th>
                <th>{{__('New Code')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Intial Budget')}}</th>
                <th>{{__('BSR Base')}}</th>
                <th>{{__('MTR Base')}}</th>
                <th>{{__('ZHR Base')}}</th>
                <th>{{__('BBD Base')}}</th>
                <th>{{__('PKB Base')}}</th>
                <th>{{__('CXB Base')}}</th>
                <th>{{__('201MU')}}</th>
                <th>{{__('201U')}}</th>
                <th>{{__('BRU')}}</th>
                <th>{{__('MRU')}}</th>
                <th>{{__('Total Base allotment')}}</th>
                <th>{{__('Spent FC Army')}}</th>
                <th>{{__('Spent Base Unit')}}</th>
                <th>{{__('Spent Air HQ')}}</th>
                <th>{{__('Total Spent')}}</th>
                <th>{{__('Balance')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @forelse ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->old_code }}</td>
            <td>{{ $datam->new_code }}</td>
            <td>{{ $datam->range }}</td>
            <td>{{ $datam->intial_budget }}</td>
            <td>{{ $datam->base_bsr }}</td>
            <td>{{ $datam->base_mtr }}</td>
            <td>{{ $datam->base_zhr }}</td>
            <td>{{ $datam->base_bbd }}</td>
            <td>{{ $datam->base_pkp }}</td>
            <td>{{ $datam->base_cxb }}</td>
            <td>{{ $datam->base_air_hq }}</td>
            <td>{{ $datam->base_201mu }}</td>
            <td>{{ $datam->base_201u }}</td>
            <td>{{ $datam->base_bru }}</td>
            <td>{{ $datam->base_mru }}</td>
            <td>{{ $datam->total_base_allotment }}</td>
            <td>{{ $datam->spent_fc_army }}</td>
            <td>{{ $datam->spent_airhq }}</td>
            <td>{{ $datam->total_spent }}</td>
            <td>{{ $datam->balance }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('allotment_brackdown.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('allotment_brackdown.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('allotment_brackdown.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="10" class="center text-success" > <b> No Data Found </b></td>
        </tr>
        @endforelse
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('allotment_brackdown.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
