@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title="Budget Allotment Brackdown">
    <x-pondit-form action="{{ route('budget_allotment_brackdown.update', $data->id) }}">
        @method('PUT')
        
        <div class='form-group row'>
            <label for="budgetcode_id" class="col-sm-2 col-form-label">New Code</label>
            <div class='col-sm-4'>
                <select name="budgetcode_id" id="budgetcode_id" class="form-control">
                    <option value="">--Select Badget Code--</option>
                    @foreach ($budgetAllotmentBrackdons as $id => $description)
                        @if ($data->budgetcode_id == $id)
                            <option value="{{ $id }}" selected>{{ $description }}</option>    
                        @else
                            <option value="{{ $id }}">{{ $description }}</option>    
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class='form-group row'>
            <label for="amount" class="col-sm-2 col-form-label">Amount</label>
            <div class='col-sm-10'>
                <input type="number" name="amount" id="amount" value="{{ $data->amount ?? '' }}"  class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="alloted_to" class="col-sm-2 col-form-label">Alloted To</label>
            <div class='col-sm-10'>
                <input type="text" name="alloted_to" id="alloted_to" value="{{ $data->alloted_to ?? '' }}"  class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
            <div class='col-sm-10'>
                <input type="text" name="reference_no" id="reference_no" value="{{ $data->reference_no ?? '' }}" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-4'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
            <div class="col-sm-6">
                <img src="{{ asset('') . $data->letter }}" height="300" width="300" alt="">
            </div>
        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget_allotment_brackdown.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budget_allotment_brackdown.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('budget_allotment_brackdown.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('budget_allotment_brackdown.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection