@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budgetcode">
<x-pondit-form action="{{route('budget_allotment.store')}}" >
    <div class='form-group row'>
        <label class="col-sm-2 col-form-label" for="budget_type">Range</label>
        <div class='col-sm-4'>
            <select name="range_id" id="range_id" class="form-control select-search">
                <option value="">--Select Range--</option>
                @foreach ( $ranges as $id => $title)
                    <option value="{{ $id }}">{{ $title }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class='form-group row'>
        <label class="col-sm-2 col-form-label" for="budget_type">Budgetcode</label>
        <div class='col-sm-4'>
            <select name="budgetcode_id" id="budgetcode_id" class="form-control select-search">
                <option value="">--Select Budgetcode--</option>
                @foreach ( $budgetcodes as $id => $newcode)
                    <option value="{{ $id }}">{{ $newcode }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class='form-group row'>
        <label for="initial_amount" class="col-sm-2 col-form-label">Initial Amount</label>
        <div class='col-sm-10'>
            <input type="number" name="initial_amount" id="initial_amount" class="form-control" />
        </div>
    </div>
       
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budget_allotment.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
