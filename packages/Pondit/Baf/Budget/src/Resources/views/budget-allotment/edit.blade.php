@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Ranges">
    <x-pondit-form action="{{ route('budget_allotment.update', $data->id) }}">
        @method('PUT')

        <div class='form-group row'>
            <label class="col-sm-2 col-form-label" for="budget_type">Range</label>
            <div class='col-sm-4'>
                <select name="range_id" id="range_id" class="form-control">
                    <option value="">--Select Range--</option>
                    @foreach ( $ranges as $id => $title)
                    @if ($data->range_id == $id)
                        <option value="{{ $id }} " selected > {{ $title }}</option>
                    @else
                        <option value="{{ $id }}">{{ $title }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class='form-group row'>
            <label class="col-sm-2 col-form-label" for="budget_type">Budgetcode</label>
            <div class='col-sm-4'>
                <select name="budgetcode_id" id="budgetcode_id" class="form-control">
                    <option value="">--Select Budgetcode--</option>
                    @foreach ( $budgetcodes as $id => $newcode)
                        @if ($data->budgetcode->id == $id)
                            <option value="{{ $id }}" selected>{{ $newcode }}</option>
                        @else
                            <option value="{{ $id }}">{{ $newcode }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="initial_amount" class="col-sm-2 col-form-label">Description</label>
            <div class='col-sm-10'>
                <textarea name="description" id="description"  rows="3" class="form-control">{{ $data->description ?? '' }}</textarea>
            </div>
        </div>
        <div class='form-group row'>
            <label for="initial_amount" class="col-sm-2 col-form-label">Initial Amount</label>
            <div class='col-sm-10'>
                <input type="number" name="initial_amount" id="initial_amount" value="{{ $data->initial_amount ?? '' }}" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('budget_allotment.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('budget_allotment.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('budget_allotment.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('budget_allotment.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection