@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Budget Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modalBody">
        <div id="budgetData"> </div>
          <table class="table table-bordered w-100">
              <thead>
                    <th> Transfer </th>
                    <th>To Code </th>
                    <th> Transfer Date</th>
                    <th>Amount</th>

              </thead>
              <tbody id="tbody">

              </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<x-pondit-card title="Budgetcode">
    <a href="{{ route('budget_allotment.range_wise_report') }}" class="btn bg-success" >Range Wise Report</a>
    <a href="{{ route('budget_allotment.type_wise_report') }}" class="btn bg-success" >Type Wise Report</a>
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Budget Code')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Initial Amount')}}</th>
                <th>{{__('total Suplimentary Amount')}}</th>
                <th>{{__('total Budgetcode Amount')}}</th>
                <th>{{__('Balance')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        {{-- @dd($datam->budgetcode) --}}
       <?php $id = $datam->id?>
        <tr>
            <td>{{ ++$key }}</td>
            <td ondblclick="getData('<?php echo $id;?>');">{{ $datam->budgetcode->newcode ?? '' }}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td>{{ $datam->range_name ?? '' }}</td>
            <td>{{ $datam->initial_amount ?? '' }}</td>
            <td>{{ $datam->total_suplimentary_amount ?? '' }}</td>
            <td>{{ $datam->total_budgetcode_amount ?? '' }}</td>
            <td>{{ $datam->balance ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budget_allotment.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budget_allotment.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budget_allotment.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budget_allotment.create')}}" />
        </div>
        <div></div>
    </x-slot>
    
</x-pondit-card>


@endsection
@push('js')
<script>

    function getData(id){
        const url = 'http://budget.test/getBudgetData/' + id;
            axios.get(url)
                .then(function (response) {
                    $('#modalForm').modal('show');

                     document.getElementById("budgetData").innerHTML = `
                        <div class="card">
                            <div class="card-body">
                                <p>${response.data.budget.description ?? ''}</p>
                                <p> Initial Amount : ${response.data.budget.initial_amount ?? ''}</p>
                                <p> Balance : ${response.data.budget.balance ?? ''}</p>
                            </div>
                        </div>


                     `;

                    document.getElementById('tbody').innerHTML = null;

                    let content = ``;
                    let i = 1;

                    $.each(response.data.transfer, function (index  ,value) {

                        content += `<tr>
                                    <td>${i++}</td>
                                    <td>${value.to_code ?? 'N/A'}</td>

                                    <td>${value.transfer_at ?? 'N/A'}</td>
                                    <td>${value.amount ?? 'N/A'}</td>
                                 </tr>
                                `

                    });

                    $('#tbody').html(content);

                })
                .catch(function (error) {
                    console.log("error")
                })
    }
    
    

</script>
    
@endpush