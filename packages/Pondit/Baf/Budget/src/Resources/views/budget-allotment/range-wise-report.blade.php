@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->
    <x-pondit-card title="Range Wise Report">

        <div class="single-show">
            <div class="card-body p-0">
                <button class="btn btn-primary mb-1 btn-sm " onclick="javascrit:window.history.back()"><i
                        class="fa fa-arrow-left"></i></button>
                <table class="table table-bordered table-responsive table-sm" id="table">
                    <thead>
                        <tr class="bg-success">
                            <th>#</th>
                            <th>Budget Code</th>
                            <th>Fin Year</th>
                            <th>Description</th>
                            <th>Activity Type</th>
                            <th>Initial Amount</th>
                            @for ($i = 1; $i <= $maxSuplymentry; $i++)
                                <th>Suplimentary {{ $i }}</th>
                            @endfor
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $range => $allotmentes)
                            <tr>
                                <td colspan="7" class="font-weight-bold text-primary">{{ $range }}</td>
                            </tr>
                            @foreach ($allotmentes as $key => $allotmente)
                            @php
                                $suplimantary = getSuplimentary($allotmente->id);
                            @endphp
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $allotmente->budgetcode->newcode }}</td>
                                    <td>{{ $allotmente->fin_year }}</td>
                                    <td>{{ $allotmente->description }}</td>
                                    <td>{{ $allotmente->activity_type }}</td>
                                    <td>{{ $allotmente->initial_amount }}</td>
                                    @for ($i = 0; $i < $maxSuplymentry; $i++)
                                        @if (isset($suplimantary[$i]))
                                            
                                        <td>{{ $suplimantary[$i]->suplimentary_amount ?? '' }}</td>
                                        @else
                                            <td> &nbsp; </td>
                                        @endif
                                    @endfor
                                    <td>{{ $allotmente->balance }}</td>
                                </tr>

                            @endforeach
                        @empty
                        <tr>
                            <td class="text-center" colspan="7">No Data Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <x-slot name="cardFooter">
            <div></div>
            <div></div>
            <div>
                <a href="{{ route('pdf.range_wise_report_pdf') }}" data-popup="tooltip" title="Range Wise Report PDF"
                    data-original-title="Range Wise Report"><i class="fas fa-file-pdf px-1 py-1 bg-brown"></i></a>
                <a href="#" id="downloadLink" data-popup="tooltip" onclick="exportF(this)" title="Range Wise Report EXCEL"
                    data-original-title="Range Wise Report"><i class="fas fa-file-excel px-1 py-1 bg-success"></i></a>
            </div>
        </x-slot>
    </x-pondit-card>

    <!-- /basic example -->

@endsection
@push('js')
    <script>
        function exportF(elem) {
            var table = document.getElementById("table");
            var html = table.outerHTML;
            var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
            elem.setAttribute("href", url);
            elem.setAttribute("download", "Range-wise-report.xlsx"); // Choose the file name
            return false;
        }

    </script>
@endpush
