@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Budget Transfer">
<x-pondit-form action="{{route('budget-transfer.store')}}" >
        <div class='form-group row'>
            <label for="from_code" class="col-sm-2 col-form-label">From Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="from_code" id="from_code" class="form-control" /> --}}
                <select name="from_code" class="form-control" required>
                    <option value="">--Select From Code--</option>
                    @foreach ($budgetCodes as  $budgetCode)
                        <option value="{{ $budgetCode->budgetcode_id }}">{{ $budgetCode->budgetcode->newcode }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="to_code" class="col-sm-2 col-form-label">To Code</label>
            <div class='col-sm-10'>
                {{-- <input type="text" name="to_code" id="to_code" class="form-control" /> --}}
                <select name="to_code" class="form-control" required >
                    <option value="">--Select To Code--</option>
                    @foreach ($budgetCodes as  $budgetCode)
                        <option value="{{ $budgetCode->budgetcode_id }}">{{ $budgetCode->budgetcode->newcode }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="amount" class="col-sm-2 col-form-label">Amount</label>
            <div class='col-sm-10'>
                <input type="text" name="amount" id="amount" class="form-control" required />
            </div>
        </div>
        <div class='form-group row'>
            <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
            <div class='col-sm-10'>
                <input type="text" name="reference_no" id="reference_no" class="form-control" />
            </div>
        </div>

        <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-10'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
        </div>


        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budget-transfer.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
