@extends('pondit-limitless::layouts.master')
@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')


<x-pondit-card title=" Suplimentary Budget">
<x-pondit-form action="{{route('budget.suplimentary_budget_store')}}" >

    <div class='form-group row'>
        <label for="amount" class="col-sm-3 col-form-label">Title</label>
        <div class='col-sm-9'>
            <input type="text" name="title" id="title" value="Suplimentary Budget Allotmnt For " class="form-control" />
        </div>
    </div>
    <div class='form-group row'>
        <label for="amount" class="col-sm-3 col-form-label">Amount</label>
        <div class='col-sm-9'>
            <input type="text" name="amount" id="amount" class="form-control" />
        </div>
    </div>
    <div class='form-group row'>
        <label for="reference_no" class="col-sm-3 col-form-label">Reference No</label>
        <div class='col-sm-9'>
            <input type="text" name="reference_no" id="reference_no" class="form-control" />
        </div>
    </div>

    <div class='form-group row'>
        <label for="letter" class="col-sm-3 col-form-label">Letter</label>
        <div class='col-sm-9'>
            <input type="file" name="letter" id="letter" class="form-control" />
        </div>
    </div>



    <div class='form-group row'>
        <label for="allotment_excel" class="col-sm-3 col-form-label">Suplimentary Allotment Excel</label>
        <div class='col-sm-9'>
            <input type="file" name="suplimentary_allotment_excel" id="suplimentary_allotment_excel" class="form-control" />
        </div>
    </div>
       
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
        <x-pondit-act-i url="{{route('budget.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
