@extends('pondit-limitless::layouts.master')
@section('content')
    <x-pondit-card title="Budget Transfer">
        <x-pondit-form action="{{ route('budgetcode_to_item.store') }}">

            <div class='form-group row'>
                <label for="budgetcode_id" class="col-sm-2 col-form-label">Budget Code</label>
                <div class='col-sm-6'>
                    <select name="budgetcode_id" id="budgetcode_id" class="form-control select-search" onchange="getRangeForBudgetCode()">
                        <option value="">-- Select Badget Code --</option>
                        @foreach ($budgetcodes as $budgetcode)
                            <option value="{{ $budgetcode->id }}">Old Code : {{ $budgetcode->oldcode }} || New Code :
                                {{ $budgetcode->newcode }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class='form-group row'>
                <label for="range_id" class="col-sm-2 col-form-label">Range</label>
                <div class='col-sm-6'>
                    <select name="range_id" id="range_id" class="form-control select-search">
                        <option value="">-- Select Range --</option>
                        @foreach ($ranges as $id => $range)
                            <option value="{{ $id }}">{{ $range }}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class='form-group row'>
                <label for="item_id" class="col-sm-2 col-form-label"> Item </label>
                <div class='col-sm-6'>
                    <select name="item_id" id="item_id" class="form-control select-search">
                        <option value="">-- Select Item --</option>
                        @foreach ($prepcmDatas as $item_id => $item_name)
                            <option value="{{ $item_id }}">{{ $item_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <x-pondit-btn icon="check" title="{{ __('save') }}" />
            <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger"
                title="{{ __('cancel') }}" />
        </x-pondit-form>
        <x-slot name="cardFooter">
            <div></div>
            <div>
                <x-pondit-act-i url="{{ route('budgetcode_to_item.index') }}" />
            </div>
            <div></div>
        </x-slot>
    </x-pondit-card>
@endsection

@push('js')
<script>
    function getRangeForBudgetCode() {
        let budgetcode_id = $("#budgetcode_id").val();

        $.ajax({
            url: "{{ route('budgetcode_to_item.get_range') }}",
            method: 'GET',
            data: {
                budgetcode_id
            },
            success: function(res) {
                if (res.status == 'ok') {
                    let range_id = $("#range_id");
                    let html = [] ;
                    res.data.forEach(element => {
                        html += `<option value="${element.id}">${element.title}</option>`;
                    });
                    range_id.html(html);
                }else{
                    alert(res.message);
                }
            },
            error: function(err) {
                console.log(err)
            },
        })
    }

</script>
@endpush
