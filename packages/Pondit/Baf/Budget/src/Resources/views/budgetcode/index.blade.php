@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budgetcode">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Old Code')}}</th>
                <th>{{__('New Code')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Budget Type')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->oldcode }}</td>
            <td>{{ $datam->newcode }}</td>
            <td>{{ $datam->description ?? '' }}</td>
            <td>
                @foreach ($datam->range as $range)
                    <span class="badge badge-dark">{{$range->title ?? ''}}</span>
                @endforeach
            </td>
            <td>{{ $datam->budget_type ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('budgetcode.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('budgetcode.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('budgetcode.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('budgetcode.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
