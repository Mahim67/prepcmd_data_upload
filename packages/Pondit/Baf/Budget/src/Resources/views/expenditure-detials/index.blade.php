@extends('pondit-limitless::layouts.master')


@section('content')

@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')

<x-pondit-card title="Budget Allotment Brackdown">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('DGDP')}}</th>
                <th>{{__('Acceptance No')}}</th>
                <th>{{__('Contract No')}}</th>
                <th>{{__('Type Of Acc')}}</th>
                <th>{{__('Old Code')}}</th>
                <th>{{__('New Code')}}</th>
                <th>{{__('File Rep No')}}</th>
                <th>{{__('Date')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('DTE')}}</th>
                <th>{{__('Range')}}</th>
                <th>{{__('Qty')}}</th>
                <th>{{__('Spent By Air HQ')}}</th>
                <th>{{__('Spent By Base')}}</th>
                <th>{{__('Status')}}</th>
                <th>{{__('Currency')}}</th>
                <th>{{__('Print Code')}}</th>
                <th>{{__('Fin Year')}}</th>
                <th>{{__('LC Commission')}}</th>
                <th>{{__('Collection Charge')}}</th>
                <th>{{__('Swift Charge')}}</th>
                <th>{{__('Agency Comm')}}</th>
                <th>{{__('Country')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($expenditureDetials as $key=>$expenditureDetial)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $expenditureDetial->dgdp ?? '' }}</td>
            <td>{{ $expenditureDetial->acceptance_no ?? '' }}</td>
            <td>{{ $expenditureDetial->contract_no ?? '' }}</td>
            <td>{{ $expenditureDetial->type_of_acc ?? '' }}</td>
            <td>{{ $expenditureDetial->old_code ?? '' }}</td>
            <td>{{ $expenditureDetial->new_code ?? '' }}</td>
            <td>{{ $expenditureDetial->file_rep_no ?? '' }}</td>
            <td>{{ $expenditureDetial->date ?? '' }}</td>
            <td>{{ $expenditureDetial->description ?? '' }}</td>
            <td>{{ $expenditureDetial->dte ?? '' }}</td>
            <td>{{ $expenditureDetial->range ?? '' }}</td>
            <td>{{ $expenditureDetial->qty ?? '' }}</td>
            <td>{{ $expenditureDetial->spent_by_airhq ?? '' }}</td>
            <td>{{ $expenditureDetial->spent_by_base ?? '' }}</td>
            <td>{{ $expenditureDetial->status ?? '' }}</td>
            <td>{{ $expenditureDetial->currency ?? '' }}</td>
            <td>{{ $expenditureDetial->print_code ?? '' }}</td>
            <td>{{ $expenditureDetial->fin_year ?? '' }}</td>
            <td>{{ $expenditureDetial->lc_commission ?? '' }}</td>
            <td>{{ $expenditureDetial->collection_charge ?? '' }}</td>
            <td>{{ $expenditureDetial->swift_charge ?? '' }}</td>
            <td>{{ $expenditureDetial->agency_comm ?? '' }}</td>
            <td>{{ $expenditureDetial->country ?? '' }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('expenditure_detials.show', $expenditureDetial->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('expenditure_detials.edit', $expenditureDetial->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('expenditure_detials.delete', $expenditureDetial->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('expenditure_detials.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
