@extends('pondit-limitless::layouts.master')
@section('content')
html form 
    <div>
            {!! Form::open([
             'route'=>'urdr.store',
             'method'=>'post',
             'id'=>'createForm'
             ]) !!}

            <div class="p-4" id="urdrSection">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border" id="urdrTitle">Create UR DR </legend>
                    <input type="hidden" value="{{$data->id}}" name="contract_id">
                    <div>
                        <label>
                            <input type="radio" value="ur" class="option-input radio" name="urdr" />
                            UR
                        </label>
                        <label class="ml-3">
                            <input type="radio"  value="dr" class="option-input radio" name="urdr" />
                            DR
                        </label>
                        <span class="ml-5"> UR-DR NO : </span> <input type="text" style="border-color: #63a0ff" name="ur_no" placeholder="Enter urdr no" required>
                        <span class="ml-3"> Date of UR DR : </span>
                        <input type="date" name="urdr_date" required/>
                    </div>
                    <div class="row mt-3">
                        <div class="col">
                            <div class="form-group" id="urSection">
                                <div id="parametersBox">
                                    <div class="form-row">
                                        <div class="col-md">
                                            <label>Description </label>
                                            <select class="form-control  selectexample" name="item_id[]">
                                                <option value="" class="text-muted">Select Description</option>
                                                @foreach($items as $key=>$singleItem)
                                                    <option value="{{$key ?? ''}}">{{$singleItem}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md">
                                            <label> Part No </label>
                                            <input name="partno[]" id="partno1" value="" class="part form-control" style="background: #bbf1ff !important;" readonly>
                                        </div>

                                        <div class="col-md">
                                            <label>Equip Ser No </label>
                                            <input type="text" name="equip_serno[]" class="form-control"/>
                                        </div>

                                        <div class="col-md">
                                            <label>Contract Item Ser No </label>
                                            <input type="text" name="contract_item_serno[]" class="form-control contract_item_serno"  style="background: #bbf1ff !important;" readonly>
                                        </div>
                                        <div class="col-md">
                                            <label> Present Location </label>
                                            <input type="text" name="present_location[]" class="form-control" placeholder="Presnet Status">
                                        </div>

                                        <div class="col-md">
                                            <label> Fault </label>
                                            <textarea name="item_remarks[]" class="form-control" required> </textarea>
                                        </div>

                                        <button id="addParam" class="btn btn-primary mt-4" style="height: 32px !important; padding-top:7px !important;">
                                            Add More
                                        </button>

                                    </div>
                                    <div id="params">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-row">
                        <label> Write Remarks</label>
                        <textarea name="remarks" style="height: 100px !important;" class="form-control" cols="2" rows="20">

                            </textarea>
                    </div>
                </fieldset>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-round btn-info"><i class="fas fa-check"></i> Submit </button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>


<script>
    let addedParamCount = 0;
        let btn = document.getElementById('addParam');
        btn.addEventListener('click', function (e) {
            e.preventDefault();
            let params = document.getElementById('params');
            let string = `
                                                  <div class="form-row">
                                                        <div class="col-md">
                                                            <label>Items Name </label>
                                               <select class="form-control selectexample" name="item_id[]">
                                                 <option value="" class="text-muted">Select Description</option>

                                                    @foreach($items as $key=>$singleItem)
                                                        <option value="{{$key ?? ''}}">{{$singleItem}}</option>
                                                    @endforeach
                                                </select>

</div>

<div class="col-md">
<label> Part No </label>
                <input name="partno[]" id="partno1" value="" class="part form-control" style="background: #bbf1ff !important;" readonly>
         </div>
              <div class="col-md">
                     <label>Equip Ser No </label>
                      <input type="text" name="equip_serno[]" class="form-control"/>
                  </div>


            <div class="col-md">
               <label>Contract Item Ser No </label>
              <input type="text" name="contract_item_serno[]" class="form-control contract_item_serno"  style="background: #bbf1ff !important;" readonly>
            </div>


           <div class="col-md">
             <label> Present Location</label>
             <input type="text" name="present_location[]" class="form-control" placeholder="Presnet Location">
            </div>

            <div class="col-md">
              <label> Fault </label>
                <textarea name="item_remarks[]" class="form-control" required> </textarea>
            </div>

           <button style="height: 32px !important; padding-top:7px !important;"
               class="btn btn-danger mt-4 deleteParam ">
               Remove
           </button>
</div>
</div>`;

            let paramElement = getElementFromString(string);

            params.appendChild(paramElement);

            let deleteParam = document.getElementsByClassName('deleteParam');
            for (item of deleteParam){
                item.addEventListener('click', (e)=>{
                    e.target.parentElement.remove();
                })
            }
            addedParamCount++;

        })
</script>

@php
    
    public function store(Request $request)
    {
//    	dd($request->all());
        try {
            $request->validate([
                'ur_no'  => 'required',
                'urdr'   => 'required',
            ]);

            $input                  =      $request->all();
            $contract_item_serno    =      $request->input('contract_item_serno');
            $items                  =      $request->input('item_id');
            $partno                 =      $request->input('partno');
            $preLocation            =      $request->input('present_location');
            $itemRemarks            =      $request->input('item_remarks');
            $equipSerno             =      $request->input('equip_serno');

            for($i=0; $i< count($input['item_id']); $i++) {
                $urdr = new UrDr();
                $urdr->contract_id              =    $request->contract_id;
	            $urdr->item_id                  =    $items[$i];
	            $urdr->remarks                  =    $request->remarks;
	            $urdr->urdr                     =    $request->urdr;
	            $urdr->ur_no                    =    $request->ur_no;
	            $urdr->urdr_date                =    $request->urdr_date;
	            $urdr->contract_item_serno      =    $contract_item_serno[$i];
	            $urdr->partno                   =    $partno[$i];
	            $urdr->present_location         =    $preLocation[$i];
	            $urdr->equip_serno              =    $equipSerno[$i];
	            $urdr->item_remarks             =    $itemRemarks[$i];
                $urdr->save();
            }
            return redirect()->back()->withMessage('Ur/Dr Inserted Successfully.');
        }

        catch (QueryException $e) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(
                    $e->getMessage()
                );
        }
    }
@endphp
        @endsection
