@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Ranges">
    <x-pondit-form action="{{ route('suplimentary_budget_allotment.update', $data->id) }}">
        @method('PUT')

        <div class='form-group row'>
            <label for="budgetcode_id" class="col-sm-2 col-form-label">Budget Code</label>
            <div class='col-sm-4'>
                <select name="budgetcode_id" id="budgetcode_id" class="form-control">
                    <option value="">--Select Badget Code--</option>
                    @foreach ($budgetcodes as $id => $newcode)
                        @if ($data->budgetcode_id == $id)
                            <option value="{{ $id }}" selected>{{ $newcode }}</option>    
                        @else
                            <option value="{{ $id }}">{{ $newcode }}</option>    
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class='form-group row'>
            <label for="range_id" class="col-sm-2 col-form-label">New Code</label>
            <div class='col-sm-4'>
                <select name="range_id" id="range_id" class="form-control">
                    <option value="">--Select Range--</option>
                    @foreach ($ranges as $id => $title)
                        @if ($data->range_id == $id)
                            <option value="{{ $id }}" selected >{{ $title }}</option>    
                        @else
                            <option value="{{ $id }}">{{ $title }}</option>    
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        {{-- <div class='form-group row'>
            <label for="activity_type" class="col-sm-2 col-form-label">Activity Type</label>
            <div class='col-sm-10'>
                <input type="text" name="activity_type" id="activity_type" value="{{ $data->activity_type }}" class="form-control" />
            </div>
        </div> --}}
        <div class='form-group row'>
            <label for="suplimentary_amount" class="col-sm-2 col-form-label">Suplimentary Amount</label>
            <div class='col-sm-10'>
                <input type="number" name="suplimentary_amount" id="suplimentary_amount" value="{{ $data->suplimentary_amount }}" class="form-control" />
            </div>
        </div>

     
        {{-- <div class='form-group row'>
            <label for="reference_no" class="col-sm-2 col-form-label">Reference No</label>
            <div class='col-sm-10'>
                <input type="number" name="reference_no" id="reference_no" value="{{ $data->reference_no }}" class="form-control" />
            </div>
        </div>
     
        <div class='form-group row'>
            <label for="letter" class="col-sm-2 col-form-label">Letter</label>
            <div class='col-sm-4'>
                <input type="file" name="letter" id="letter" class="form-control" />
            </div>
            <div class="col-sm-6">
                <img src="{{ asset('') . $data->letter }}" height="300" width="300" alt="">
            </div>
        </div> --}}
   
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" onclick="javascript:window.history.back()" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('suplimentary_budget_allotment.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('suplimentary_budget_allotment.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('suplimentary_budget_allotment.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('suplimentary_budget_allotment.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection