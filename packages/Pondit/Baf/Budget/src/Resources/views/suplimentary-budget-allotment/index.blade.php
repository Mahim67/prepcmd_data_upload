@extends('pondit-limitless::layouts.master')


@section('content')


@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Budgetcode">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('New Code')}}</th> 
                <th>{{__('Fin Year')}}</th>
                <th>{{__('Suplimentary Amount')}}</th>
                <th class="text-center">{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->budgetcode->newcode ?? ''}}</td>
            <td>{{ $datam->fin_year ?? '' }}</td>
            <td>{{ $datam->suplimentary_amount ?? '' }}</td>
            <td class="d-flex justify-content-center">
                <x-pondit-act-link url="{{route('suplimentary_budget_allotment.show', $datam->id)}}" icon="eye" bg="success" />
                {{-- <x-pondit-act-link url="{{route('suplimentary_budget_allotment.edit', $datam->id)}}" icon="pen" bg="primary" /> --}}
                <x-pondit-act-link-d url="{{route('suplimentary_budget_allotment.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('suplimentary_budget_allotment.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
