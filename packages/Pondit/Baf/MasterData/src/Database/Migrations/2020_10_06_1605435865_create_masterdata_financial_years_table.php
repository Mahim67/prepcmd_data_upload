<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataFinancialYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_financial_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year_range');
            $table->string('from_year');
            $table->string('to_year');
            
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            
            $table->timestamps();
            
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_financial_years', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}