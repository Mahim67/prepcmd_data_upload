<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataWorkflowTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('masterdata_workflow_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('number')->nullable();
            $table->string('purpose')->nullable();
            $table->string('template_path')->nullable();
            $table->longText('htmlized_template')->nullable();

            $table->json('dynamic_values')->nullable();
            $table->string('number_of_copies')->nullable();
            $table->json('copy_type')->nullable();
            $table->string('from_storage_table')->nullable();
            $table->boolean('is_storage_exists')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('masterdata_workflow_templates', function (Blueprint $table) {

            $table->dropSoftDeletes();

        });
    }
}