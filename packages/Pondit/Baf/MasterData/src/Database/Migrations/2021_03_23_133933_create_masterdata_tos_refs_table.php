<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterdataTosRefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterdata_tos_refs', function (Blueprint $table) {
            $table->id();
            $table->string('tos_ref_manual_name')->nullable();
            $table->string('tos_ref_manual_page_no')->nullable();
            $table->string('tos_ref_manual_fig_no')->nullable();
            $table->string('tos_ref_manual_code_no')->nullable();
            $table->string('tos_ref_manual_index')->nullable();
            $table->string('tos_ref_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masterdata_tos_refs');
    }
}
