<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\Aircraft;

class AircraftController extends Controller
{
    public function index()
    {
        $data = Aircraft::all();
        return view('masterdata::aircrafts.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::aircrafts.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data  = new Aircraft();
            $data->title        = $request->title;
            $data->description  = $request->description;
            // dd($data);
            $data->save();
            return redirect()
                    ->route('aircraft.index')
                    ->withMessage('Aircraft has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('aircraft.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Aircraft::findOrFail($id);
        return \view('masterdata::aircrafts.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = Aircraft::findOrFail($id);
        return \view('masterdata::aircrafts.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            // dd($request->all());
            $data = Aircraft::find($id);
            // dd($data);
            
            $data->title        = $request->title;
            $data->description  = $request->description;
            // dd($data);
            $data->save();

            return redirect()
                ->route('aircraft.index')
                ->withMessage('Aircraft has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Aircraft::find($id);
        // dd($data);
        $data->delete();

        return redirect()
                    ->route('aircraft.index')
                    ->withMessage('Aircraft has been deleted successfully!');
    }
    
    
}
