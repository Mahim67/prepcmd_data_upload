<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\FinancialYear;
use Pondit\Baf\MasterData\Http\Requests\FinancialYearRequest;

class FinancialYearApiController extends Controller
{
    public function index()
    {
        try {
            $data = FinancialYear::orderBy('year_range', 'DESC')->get();
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
        
    }

    public function store(FinancialYearRequest $request)
    {
        try
        {
            $validated = $request->validated();

            $data = FinancialYear::create($request->all());
            if(!$data)
                throw new Exception("Could not be saved!", 1);
                
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        }
        catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }
    }

    public function show($id)
    {
        try {
            $data = FinancialYear::find($id);
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }
    }

    public function update(FinancialYearRequest $request, $id)
    {
        try
        {
            $validated = $request->validated();

            $data = FinancialYear::findOrFail($id);

            // Field updating goes here
            $data->year_range          = $request->year_range;
            $data->from_year           = $request->from_year;
            $data->to_year             = $request->to_year;
            $data->deleted_by          = $request->deleted_by;
            $data->updated_by          = $request->updated_by;
            $data->created_by          = $request->created_by;

            $data->save();
            if(!$data)
                throw new Exception("Could not be updated", 1);
                
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        }
        catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $data = FinancialYear::findOrFail($id);

        $data->delete();
        if(!$data)
            throw new Exception("Could not be destroy!", 1);
        
        return response()->json([
            'status'    => 'success',
            'data'      => $data
        ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }
    }

    public function trash(){
        try{
            $data = FinancialYear::onlyTrashed()->orderBy('created_at', 'desc')->get();
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }
    }

    public function restore($id){
        try{
    	    FinancialYear::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();
         } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }  
    }

    public function permanentDelete($id){
        try{
            $data = FinancialYear::onlyTrashed()->where('id', $id)->first();
            $data->forceDelete();
            if(!$data)
                throw new Exception("Could not be destroy!", 1);

            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage(),
                'data'      => $data
            ]);
        }

    }

}
