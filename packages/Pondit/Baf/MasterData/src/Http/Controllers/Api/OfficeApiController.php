<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;


use Pondit\Baf\MasterData\Models\Office;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Http\Requests\OfficeRequest;

class OfficeApiController extends Controller
{
    public function index()
    {
        try {
            $data = Office::all();
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
    }

    public function store(OfficeRequest $request)
    {
        try {
            $data               = new Office();
            $data->name         = $request->name;
            $data->description  = $request->description;
            $data->parent_id    = $request->office_id;
            $data->save();
            if(!$data)
                throw new Exception("Could not be saved!", 1);
                
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
            
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
    
    }

    public function show($id)
    {
        try {
            $data = Office::find($id);
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
    }

    public function update(OfficeRequest $request, $id)
    {
        try {
            $data = Office::find($id);
            $data->name         = $request->name;
            $data->description  = $request->description;
            $data->parent_id    = $request->office_id;

            $data->update();

            if(!$data)
                throw new Exception("Could not be updated!", 1);
                
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
            
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $data = Office::find($id);
            $data->delete();
            if(!$data)
                throw new Exception("Could not be destroy!", 1);
                
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
    }

    public function tree () 
    {
        $nodes = Office::get()->toTree();

        $traverse = function ($offices, $prefix = '-') use (&$traverse) {
            foreach ($offices as $office) {
                echo PHP_EOL . $prefix . ' ' . $office->title;
        
                $traverse($office->children, $prefix.'-');
            }
        };
        // dd($traverse($nodes));
        return response()->json([
            'status'    => 'success',
            'nodes'      => $nodes
        ]);
    }

    
    public function trash(){
        $data = Office::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return response()->json($data);
    }

    public function restore($id)
    {    	
        try {
            $data = Office::onlyTrashed()->where('id', $id)->first()->restore();
            return response()->json([
                'status'    => 'success',
                'data'      => $data
            ]);
        } catch (QueryException $th)
        {
            return response()->json([
                'status'    => $th->getMessage()
            ]);
        }
                            
    }

    public function permanentDelete($id){
       
        $data = Office::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

    public function treeOptions($selected_id = null){
        $nodes = Office::get()->toTree();
        
        $data = '<option value=""> Root/Top node </option>';
        
        $traverse = function ($offices, $prefix = '-', $parent_office_name = null) use (&$traverse,&$data,$selected_id) {

            foreach ($offices as $office) {
                // echo PHP_EOL . $prefix . ' ' . $office->title;
               if($selected_id && ($selected_id == $office->id)){
                    $data .= '<option selected value='.$office->id.'>' . ' ' .$prefix . ' ' . $office->name.'</option>'.PHP_EOL;
                }else{
                    $data .= '<option value='.$office->id.'>' . ' ' .$prefix . ' ' . $office->name . ($parent_office_name == null?'':("  (".$parent_office_name.") ")). $office->description .'</option>'.PHP_EOL;
                }
                $parent_office_name = $office->name;

                $traverse($office->children, $prefix.'-', $parent_office_name);
            }
            return $data;
        };
        $data = $traverse($nodes);

        return $data;        
    }


}