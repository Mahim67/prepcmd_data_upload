<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\WorkflowPath;

class WorkflowPathApiController extends Controller
{
    // public function range ()
    // {
    //     return view('masterdata::ranges.api.index');
    // }

    public function index()
    {
        $data = WorkflowPath::all();
        
        return response()->json($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        try
        {
            $data = $request->all();
            $data['code'] = time();

            WorkflowPath::create($data);
         
            return response()->json($data);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
    }

    public function show($id)
    {
        $data = WorkflowPath::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = WorkflowPath::findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        try{
            $data = [
                'if'    => $request->if,
                'than'    => $request->than,
            ] ;

            $workflowpath = WorkflowPath::where('id',$id)->update($data);

            if(! $workflowpath)
                throw new Exception("Unable To Update", 400);
              

            return response()->json($data);
        }
        catch (QueryException $th)
        {
            return response()->json($th->getMessage());
        }
       
    }

    public function destroy($id)
    {
       try {
            $data = WorkflowPath::findOrFail($id);

            $data->delete();
            return response()->json($data);

       } catch (QueryException $th) {
           return response()->json($th->getMessage());
       }

    }

}
