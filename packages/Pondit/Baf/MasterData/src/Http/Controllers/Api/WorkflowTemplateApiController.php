<?php

namespace Pondit\Baf\MasterData\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\WorkflowTemplate;
use Pondit\Baf\MasterData\Http\Requests\WorkflowTemplateRequest;

class WorkflowTemplateApiController extends Controller
{
    private $res;

    private function prepareResponse($success, $message)
    {
        $this->res['success']   =   $success;
        $this->res['message']   =   $message;

        return $this->res;
    }

    public function index()
    {
        $data = WorkflowTemplate::orderBy('id', 'DESC')->get();

        return response()->json($data);
    }

    public function store(WorkflowTemplateRequest $request)
    {
        try
        {
            $validated = $request->validated();

            $data = $request->all();
            $data['dynamic_values'] = json_encode($request->dynamic_values);
            $data['copy_type'] = json_encode($request->copy_type);

            $data = WorkflowTemplate::create($data);


            $this->prepareResponse(true, 'Entity has been created successfully!');

            $this->res['data'] = $data;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function show($id)
    {
        $data = WorkflowTemplate::findOrFail($id);

        return response()->json($data);
    }

    public function edit($id)
    {
        $data = WorkflowTemplate::findOrFail($id);

        return response()->json($data);
    }

    public function update(WorkflowTemplateRequest $request, $id)
    {
        try
        {
            $validated = $request->validated();

            $data = WorkflowTemplate::findOrFail($id);

            // Field updating goes here
            $data->title                = $request->title;
            $data->number               = $request->number;
            $data->purpose              = $request->purpose;
            $data->htmlized_template    = $request->htmlized_template;
            $data['dynamic_values'] = json_encode($request->dynamic_values);
            $data->number_of_copies = $request->number_of_copies;
            $data['copy_type'] = json_encode($request->copy_type);
            $data->from_storage_table   = $request->from_storage_table;
            $data->is_storage_exists    = $request->is_storage_exists;
            $data->created_by           = $request->created_by;
            $data->updated_by           = $request->updated_by;
            $data->deleted_by           = $request->deleted_by;

            if ($request->hasFile('template_path')) {
                $this->unlink($data->template_path);
                $data['template_path'] = $this->uploadFile($request->template_path, $request->title);
            }
            
            $data->deleted_by          = $request->deleted_by;
            $data->updated_by          = $request->updated_by;
            $data->created_by          = $request->created_by;

            $data->save();

            $this->prepareResponse(true, 'Entity has been updated successfully!');

            $this->res['data'] = $data;

            return response()->json($this->res);
        }
        catch (QueryException $th)
        {
            return response()->json($this->prepareResponse(false, $th->getMessage()));
        }
    }

    public function destroy($id)
    {
        $data = WorkflowTemplate::findOrFail($id);

        $data->delete();

        return response()->json($this->prepareResponse(true, 'Entity has been deleted successfully!'));
    }

    public function trash(){
        $data = WorkflowTemplate::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return response()->json($data);
    }

    public function restore($id){
    	WorkflowTemplate::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();
        return response()->json($this->prepareResponse(true, 'Entity has been restored successfully!'));
                            
    }

    public function permanentDelete($id){
       
        $data = WorkflowTemplate::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

}
