<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\EqptClass;

class EqptClassController extends Controller
{
    public function index()
    {
        $data = EqptClass::all();
        return view('masterdata::eqpt-classes.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::eqpt-classes.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data  = EqptClass::create($request->all());
            if(! $data){
                throw new Exception("Unable to create", 1);
            }
            return redirect()
                    ->route('eqpt_class.index')
                    ->withMessage('Equipment Class has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('eqpt_class.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = EqptClass::find($id);
        return \view('masterdata::eqpt-classes.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = EqptClass::find($id);
        return \view('masterdata::eqpt-classes.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            $data = EqptClass::find($id);
            $data->title  = $request->title;
            $data->save();

            return redirect()
                ->route('eqpt_class.index')
                ->withMessage('Equipment Class has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = EqptClass::find($id);
        // dd($data);
        $data->delete();

        return redirect()
                    ->route('eqpt_class.index')
                    ->withMessage('Equipment Class has been deleted successfully!');
    }
    
    
}
