<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\MasterData\Models\EquipmentType;

class EquipmentTypeController extends Controller
{
    public function index()
    {
        $data = EquipmentType::all();
        return view('masterdata::equipment-types.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::equipment-types.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data  = EquipmentType::create($request->all());
            if (! $data) {
                throw new Exception("Unable to Create!", 1);
            }
            return redirect()
                    ->route('equipment_type.index')
                    ->withMessage('Equipment Type has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('equipment_type.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = EquipmentType::find($id);
        return \view('masterdata::equipment-types.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = EquipmentType::find($id);
        return \view('masterdata::equipment-types.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            // dd($request->all());
            $data = EquipmentType::find($id);
            $data->title        = $request->title;
            $data->description  = $request->description;
            // dd($data);
            $data->save();

            return redirect()
                ->route('equipment_type.index')
                ->withMessage('Equipment Type has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = EquipmentType::find($id);
        // dd($data);
        $data->delete();

        return redirect()
                    ->route('equipment_type.index')
                    ->withMessage('Equipment Type has been deleted successfully!');
    }
    
    
}
