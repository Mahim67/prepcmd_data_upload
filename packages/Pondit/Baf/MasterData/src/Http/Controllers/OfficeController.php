<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\Office;
use Pondit\Baf\MasterData\Http\Requests\OfficeRequest;

class OfficeController extends Controller
{
    public $menudata = '';
    public function index()
    {   
        $treeData = $this->getMenu($activeId = null);
        $treeTitle = 'Offices';
        
        $data = Office::orderBy('id', 'desc')->get();
        
        return view('masterdata::offices.index', compact('data', 'treeData', 'treeTitle'));
    }


    public function testCreate()
    {
        $baseOffice = Office::where('is_base', 1)->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        // dd($baseOffice);
        return view('masterdata::offices.test-create')->with([
            'baseOffices' => $baseOffice
        ]);
    }



    public function create()
    {
        $tree_options = $this->treeOptions();
        return view('masterdata::offices.create',compact('tree_options'));
    }

    public function store(OfficeRequest $request)
    {
        try {
            if($request->is_base == 1){
                $base = $request;
            }
            elseif($request->is_wing == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $request;
            }
            elseif($request->is_sqn == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $parent->where('is_wing', '=', 1)->first();
                $sqn = $request;
            }
            elseif($request->is_sec_fly == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $parent->where('is_wing', '=', 1)->first();
                $sqn = $parent->where('is_sqn', '=', 1)->first();
                $sec_fly = $request;
            }
            // $list = [
            //     'base_id'    => $base->id??null,
            //     'base_name'  => $base->name??null,
            //     'wing_id'    => $wing->id??null,
            //     'wing_name'  => $wing->name??null,
            //     'sqn_id'     => $sqn->id??null,
            //     'sqn_name'   => $sqn->name??null,
            //     'sec_fly'    => $sec_fly->name??null,
            // ];
            
            // dd($list);

            $request->validated();

            $sequenceNumber = 1;

            if(is_null($request->parent_id)){
                $office = Office::where('parent_id', null)->orderBy('id', 'desc')->first();
            }else{
                $office = Office::where('parent_id', $request->parent_id)->orderBy('id', 'desc')->first();
            }
            
            $sequenceNumber = $office ? $office->sequence_number+1 : $sequenceNumber;

            $office                        = new Office();
            $office->name                  = $request->name;
            $office->description           = $request->description;
            $office->parent_id             = $request->office_id;
            $office->sequence_number       = $sequenceNumber;
            $office->is_hq_office          = ($request->is_hq_office == 1)?1:0;
            $office->is_branch             = ($request->is_branch == 1)?1:0;
            $office->is_dte                = ($request->is_dte == 1)?1:0;
            $office->is_base               = ($request->is_base == 1)?1:0;
            $office->is_wing               = ($request->is_wing == 1)?1:0;
            $office->is_unit               = ($request->is_unit == 1)?1:0;
            $office->is_sqn                = ($request->is_sqn == 1)?1:0;
            $office->is_sec_fly            = ($request->is_sec_fly == 1)?1:0;
            $office->is_independent_unit   = ($request->is_independent_unit == 1)?1:0;
            $office->unit_type             = $request->unit_type??null;

            $office->base_id               = $base->id??null;
            $office->base_name             = $base->name??null;
            $office->wing_id               = $wing->id??null;
            $office->wing_name             = $wing->name??null;
            $office->sqn_id                = $sqn->id??null;
            $office->sqn_name              = $sqn->name??null;
            $office->sec_fly_name          = $sec_fly->name??null;

            $office->save();
            
            return redirect(route('office.index'))->withMessage('success', 'Successfully saved');
            
        } catch(QueryException $e)
        {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = Office::find($id);

        $parent = $data->parent_id;
        $parent_name = Office::where('id', $parent)->pluck('name')->first();

        return view('masterdata::offices.show', compact('data','parent_name'));
    }

    public function edit($id)
    {
        
        $data = Office::find($id);
        
        $tree_options = $this->treeOptions($data->parent_id);
        return view('masterdata::offices.edit', compact('data','tree_options'));
    }

    public function update(OfficeRequest $request,$id)
    {
        // dd($request->all());
        try {

            if($request->is_base == 1){
                $base = $request;
            }
            elseif($request->is_wing == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $request;
            }
            elseif($request->is_sqn == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $parent->where('is_wing', '=', 1)->first();
                $sqn = $request;
            }
            elseif($request->is_sec_fly == 1){
                $node = Office::where('id', $request->office_id)->first();
                $parent = $node->ancestors;
                $base = $parent->where('is_base', '=', 1)->first();
                $wing = $parent->where('is_wing', '=', 1)->first();
                $sqn = $parent->where('is_sqn', '=', 1)->first();
                $sec_fly = $request;
            }
            
            $office                        =  Office::find($id);
            $office->name                  =  $request->name;  
            $office->parent_id             =  $request->office_id;
            $office->description           =  $request->description??$request->description;
            $office->is_hq_office          = ($request->is_hq_office == 1)?1:0;
            $office->is_branch             = ($request->is_branch == 1)?1:0;
            $office->is_dte                = ($request->is_dte == 1)?1:0;
            $office->is_base               = ($request->is_base == 1)?1:0;
            $office->is_wing               = ($request->is_wing == 1)?1:0;
            $office->is_unit               = ($request->is_unit == 1)?1:0;
            $office->is_sqn                = ($request->is_sqn == 1)?1:0;
            $office->is_sec_fly            = ($request->is_sec_fly == 1)?1:0;
            $office->is_independent_unit   = ($request->is_independent_unit == 1)?1:0;
            $office->unit_type             = $request->unit_type??null;

            $office->base_id               = $base->id??null;
            $office->base_name             = $base->name??null;
            $office->wing_id               = $wing->id??null;
            $office->wing_name             = $wing->name??null;
            $office->sqn_id                = $sqn->id??null;
            $office->sqn_name              = $sqn->name??null;
            $office->sec_fly_name          = $sec_fly->name??null;

            $office->update();
    
            if(!is_null($office->parent_id)){
                $office->reArrangeSequence($request->sequence_number, ['parent_id'=>$office->parent_id]);
            }
    
            return redirect(route('office.index'))->withMessage('success', 'Successfully Updated');;

        } catch(QueryException $e)
        {
            return redirect()->back()->withErrors($e->getMessage());
        }
        
    }

    public function destroy($id)
    {
        $data = Office::find($id);
        
        $data->delete();
        return redirect()->back();
    }

    public function tree () 
    {

        $data = $this->treeOptions();
        
        return view('masterdata::offices/tree', compact('data'));
    }

    public function treeOptions($selected_id = null){
        $nodes = Office::get()->toTree();
        
        $data = '<option value=""> Root/Top node </option>';
        
        $traverse = function ($offices, $prefix = '-', $parent_office_name = null) use (&$traverse,&$data,$selected_id) {

            foreach ($offices as $office) {
                // echo PHP_EOL . $prefix . ' ' . $office->title;
               if($selected_id && ($selected_id == $office->id)){
                    $data .= '<option selected value='.$office->id.'>' . ' ' .$prefix . ' ' . $office->name.'</option>'.PHP_EOL;
                }else{
                    $data .= '<option value='.$office->id.'>' . ' ' .$prefix . ' ' . $office->name . ($parent_office_name == null?'':("  (".$parent_office_name.")  ")). $office->description .'</option>'.PHP_EOL;
                }
                $parent_office_name = $office->name;

                $traverse($office->children, $prefix.'-', $parent_office_name);
            }
            return $data;
        };
        $data = $traverse($nodes);

        return $data;        
    }

    public function trash(){
        $data = Office::onlyTrashed()->orderBy('created_at', 'desc')->get();
    	return view('masterdata::offices.trash', compact('data'));
    }

    public function restore($id){
    	Office::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();

    	return redirect()->route('office.trash')
       	                 ->withMessage('Entity has been Restore Successfully');
    }

    public function permanentDelete($id){
       
        $data = Office::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

    public function getMenu($activeId = null){
        $menu = '';
        $nodes = Office::orderBy('sequence_number', 'asc')->get()->toTree();

        if(count($nodes)){
            $menu = $this->_printitem($nodes, $activeId);
        }
        return $menu;
    }

    private function _printitem($items,$activeId,$submenu_class = '',$link_class = '')
    {

        $this->menudata .='<ul class="nav nav-group-sub">'.PHP_EOL;
        foreach($items as $item){
            if( count($item->children) > 0 ){
                $submenu_class = 'nav-item-submenu';
                $link_class = 'legitRipple text-dark';
//                $route_link = '#';
            }else{
//                $route_link = '/category-handbook/'.$item->id;
            }
            if ($item->id === 11) {
                echo $activeId;
                $active = 'active';

            }else{
                $active = '';

            }

            $this->menudata .='<li class="nav-item '.$submenu_class.' ">'.PHP_EOL;

            $this->menudata .='<a href="#" class="nav-link text-dark'.$link_class.''. $active.' "><i class="icon-book"></i>'.$item->name.'</a>'.PHP_EOL;


            if( count($item->children) > 0 ){
                $child_items = $item->children;
                $submenu_class = '';
                $link_class = '';
                $this->_printitem($child_items,$submenu_class,$link_class);
            }

            $this->menudata .= '</li>'.PHP_EOL;
        }

        $this->menudata .='</ul>';

        return $this->menudata;
    }
    public function officeBaseEdit(){
        $bases = Office::where('is_base',1)->pluck('name','id')->toArray();
        return view('masterdata::offices.base-edit',compact('bases'));
    }

    public function getOfficeUser(){
                      
        $tree_options = $this->treeOptions();
        $bases = Office::where('is_base',1)->get();
        return view('masterdata::offices.office-wise-user',compact('tree_options','bases'));
    }

    public function getUser($id){
        $user = User::where('aims_officeid',$id)->get();
        if(count($user) == 0)
            throw new Exception("There Is No User In This Office", 1);
            
        return response()->json($user);
    }
    public function getUserBase($id){

        $users = Office::where('base_id',$id)->with('users')->get();

        if(count($users) == 0)
            throw new Exception("There Is No office In This Base", 1);
            
        return response()->json($users);
    }

    public function baseUserEdit($id)
    {
        $office = Office::find($id);
        return view('masterdata::offices.office-user-edit',compact('office'));
    }

    public function AirHQOffice()
    {
        $offices = Office::all()->toTree();
        return view('masterdata::offices.hq-office-user',compact('offices'));

    }

    public function officeUpdateFlag(){
        $office_id = [
            284,
            285,
            286,
            287,
            288,
            289,
            290,
            291,
            292,
            293,
            294,
            295,
            296,
            297,
            64,
            62,
            63,
            42,
            298,
            299,
            300,
            301,
            302,
            303,
            304,
            305,
            307,
            308,
            309,
            310,
            311,
            312,
            313,
            314,
            12,
            13,
            14,
            15,
            315,
            316,
            317,
            318,
            319,
            320,
            321,
          ];
          $update = Office::whereIn('id',$office_id)->update([
              'is_hq_office' => 1
          ]);
        
          return redirect()->back()
       	                 ->withMessage('Flag Updateed Successfully');
          
    }

}