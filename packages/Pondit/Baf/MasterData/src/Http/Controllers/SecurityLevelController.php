<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Image;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\SecurityLevel;
use Pondit\Baf\MasterData\Http\Requests\SecurityLevelRequest;
use Pondit\Baf\MasterData\Http\Controllers\MasterDataController;

class SecurityLevelController extends MasterDataController
{
    public function index()
    {
        $data  =  SecurityLevel::orderBy('id', 'DESC')->get();

        return view('masterdata::security-levels.index', compact('data'));
    }

    public function create()
    {
        
        return view('masterdata::security-levels.create');
    }

    public function store(SecurityLevelRequest $request)
    {
        try
        {
            $validated = $request->validated();

            $data = $request->all();
            if ($request->hasFile('picture')) {
                // dd($data['picture']);
                $data['picture'] = $this->uploadPicture($request->picture, $request->title);
            }
            $data = SecurityLevel::create($data);

            return redirect()
                        ->route('security_level.index')
                        ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('security_level.create')
                        ->withErrors($th->getMessage());
        }
    }

    public function show($id)
    {
        $data  =  SecurityLevel::findOrFail($id);
        

        return view('masterdata::security-levels.show', compact('data'));
    }

    public function edit($id)
    {
        $data  =   SecurityLevel::findOrFail($id);

        return view('masterdata::security-levels.edit', compact('data'));
    }

    public function update(SecurityLevelRequest $request, $id)
    {
        try
        {
            $validated  = $request->validated();

            $data = SecurityLevel::findOrFail($id);
            $data->title = $request->title;
            $data->description = $request->description;
            $data->is_status_approved = $request->is_status_approved;

            if ($request->hasFile('picture')) {
                $this->unlink($data->picture);
                $data['picture'] = $this->uploadPicture($request->picture, $request->title);
            }

            $data->save();

            return redirect()
                ->route('security_level.index')
                ->withMessage('Entity has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->route('security_level.edit', $id)
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = SecurityLevel::find($id);

        $data->delete();

        return redirect()
                    ->route('security_level.index')
                    ->withMessage('Entity has been deleted successfully!');
    }

    public function trash(){
        $data = SecurityLevel::onlyTrashed()->orderBy('created_at', 'desc')->get();
        
    	return view('masterdata::security-levels.trash', compact('data'));
    }

    public function restore($id){
    	SecurityLevel::onlyTrashed()->where('id', $id)
                                    ->first()
                                    ->restore();

    	return redirect()->route('security_level.trash')
       	                 ->withMessage('Entity has been Restore Successfully');
    }

    public function permanentDelete($id){
       
        $data = SecurityLevel::onlyTrashed()->where('id', $id)->first();

        $data->forceDelete();
           return response()->json(['status'=>'Entity has been Permanently Delete Successfully']);

    }

    private function uploadPicture($file, $name)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();
        $pathToUpload = storage_path().'/app/public/security-levels/';
        Image::make($file)->resize(634,792)->save($pathToUpload.$file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        $pathToUpload = storage_path().'/app/public/security-levels/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

}
