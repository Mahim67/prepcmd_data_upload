<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\Store;

class StoreController extends Controller
{
    public function index()
    {
        $data = Store::all();
        return view('masterdata::store.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::store.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data = Store::create($request->all());
            if(! $data){
                throw new Exception("Unable to Create!", 1);
            }
            return redirect()
                    ->route('store.index')
                    ->withMessage('Store has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('store.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Store::find($id);
        return \view('masterdata::store.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = Store::find($id);
        return \view('masterdata::store.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            $data = Store::find($id);
            $data->title            = $request->title;
            $data->description      = $request->description;
            $data->save();

            return redirect()
                ->route('store.index')
                ->withMessage('Store has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Store::find($id);
        // dd($data);
        $data->delete();

        return redirect()
                    ->route('store.index')
                    ->withMessage('Store has been deleted successfully!');
    }
    
    
}
