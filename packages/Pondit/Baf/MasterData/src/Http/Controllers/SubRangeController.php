<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\SubRange;

class SubRangeController extends Controller
{
    public function index()
    {
        $data = SubRange::all();
        return view('masterdata::sub-ranges.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::sub-ranges.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data = SubRange::create($request->all());
            if(! $data){
                throw new Exception("Unable to Create!", 1);
            }
            return redirect()
                    ->route('sub_range.index')
                    ->withMessage('Sub Range has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('sub_range.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = SubRange::find($id);
        return \view('masterdata::sub-ranges.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = SubRange::find($id);
        return \view('masterdata::sub-ranges.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            $data = SubRange::find($id);
            $data->range_id = $request->range_id;
            $data->title    = $request->title;
            // dd($data);
            $data->save();

            return redirect()
                ->route('sub_range.index')
                ->withMessage('Sub Range has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = SubRange::find($id);
        // dd($data);
        $data->delete();

        return redirect()
                    ->route('sub_range.index')
                    ->withMessage('Sub Range has been deleted successfully!');
    }
    
    
}
