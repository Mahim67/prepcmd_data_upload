<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\TosRef;

class TosRefController extends Controller
{
    public function index()
    {
        $data = TosRef::all();
        return view('masterdata::tos-refs.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::tos-refs.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data = TosRef::create($request->all());
            if(! $data){
                throw new Exception("Unable to Create!", 1);
            }
            return redirect()
                    ->route('tos_ref.index')
                    ->withMessage('TOs Ref has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('tos_ref.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = TosRef::find($id);
        return \view('masterdata::tos-refs.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = TosRef::find($id);
        return \view('masterdata::tos-refs.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            $data = TosRef::find($id);
            $data->update($request->all());
            if (! $data) {
                throw new Exception("Unable to update", 1);
            }
            return redirect()
                ->route('tos_ref.index')
                ->withMessage('TOs Ref has been updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $data = TosRef::find($id);
            $data->delete();
            return redirect()
                        ->route('tos_ref.index')
                        ->withMessage('TOs Ref has been deleted successfully!');
        } catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
        
    }
    
    
}
