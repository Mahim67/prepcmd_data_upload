<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\Trade;

class TradeController extends Controller
{
    public function index()
    {
        $data = Trade::all();
        return view('masterdata::trades.index', \compact('data'));
    }
    public function create()
    {
        return view('masterdata::trades.create');
    }
    public function store(Request $request)
    {
        try
        {
            $data  = new Trade();
            $data->title = $request->title;
            $data->save();
            if (! $data) {
                throw new Exception("Unable to saved!", 1);
            }
            return redirect()
                    ->route('trade.index')
                    ->withMessage('Entity has been created successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                    ->route('trade.create')
                    ->withErrors($th->getMessage());
        }
    }
    public function show($id)
    {
        $data = Trade::findOrFail($id);
        return view('masterdata::trades.show', \compact('data'));
    }
    public function edit($id)
    {
        $data = Trade::find($id);
        return view('masterdata::trades.edit', \compact('data'));
    }
    public function update(Request $request, $id)
    {
        try{
            $data = Trade::findOrFail($id);
            $data->title  = $request->title;
            $data->save();
            if (! $data) {
                throw new Exception("Unable to updated!", 1);
            }
            return redirect()
                ->route('trade.index')
                ->withMessage('Updated successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $data = Trade::find($id);
            $data->delete();
            if (! $data) {
                throw new Exception("Unable to deleted!", 1);
            }
            return redirect()
                        ->route('trade.index')
                        ->withMessage('Entity has been deleted successfully!');
        }
        catch (QueryException $th)
        {
            return redirect()
                        ->back()
                        ->withErrors($th->getMessage());
        }
    }

}
