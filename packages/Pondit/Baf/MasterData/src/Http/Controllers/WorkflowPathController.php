<?php

namespace Pondit\Baf\MasterData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\WorkflowPath;
use Pondit\Baf\MasterData\Http\Requests\WorkflowPathRequest;


class WorkflowPathController extends Controller
{
    public function index()
    {
        $data = WorkflowPath::all();

        return view('masterdata::workflow-paths.index', compact('data'));
    }

    public function create()
    {
        return view('masterdata::workflow-paths.create');
    }

    public function store(WorkflowPathRequest $request)
    {
        
        try {
            $data = $request->all();
            $data['code'] = substr(time(), 0, 6);

            WorkflowPath::create($data);
         
            return redirect()
                        ->route('workflow_path.index')
                        ->withMessage('success','Entity has been created successfully!');
        } catch(QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function show($id)
    {
        $data = WorkflowPath::find($id);
        return view('masterdata::workflow-paths.show', compact('data'));
    }

    public function edit($id)
    {
        $data = WorkflowPath::find($id);

        return view('masterdata::workflow-paths.edit', compact('data'));
    }

    public function update(WorkflowPathRequest $request,$id)
    {
        try {
            $data = [
                'if'    => $request->if,
                'than'    => $request->than,
            ] ;

            $workflow_path = WorkflowPath::where('id',$id)->update($data);

            if(!$workflow_path)
                throw new Exception("Unable to Update", 400);
                
            return redirect()
                        ->route('workflow_path.index')
                        ->withMessage('success','Entity has been Updated successfully!');
        } catch(QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try{
            $data = WorkflowPath::find($id);
            $data->delete();
            return redirect()->back()->withMessage('Deleted successfully!');
        }catch (QueryException $exception){
            return redirect()->back()
                             ->withInput()
                             ->withErrors($exception->getMessage());
        }
    }
    
}
