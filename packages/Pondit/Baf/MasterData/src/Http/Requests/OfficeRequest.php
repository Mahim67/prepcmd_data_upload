<?php

namespace Pondit\Baf\MasterData\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Pondit\Baf\MasterData\Rules\OfficeRule;

class OfficeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            // 'year_range' => ['required', new FinancialYearRule],
            'name' => ['required'],
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Office name is required',
        ];
                    
}
}
