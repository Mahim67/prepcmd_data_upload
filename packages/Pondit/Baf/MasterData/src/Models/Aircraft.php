<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_aircrafts';
    protected $fillable = ['id'
                            ,'title'
                            ,'description'
                        ];
}
