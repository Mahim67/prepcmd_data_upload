<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EqptClass extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_classes';
    protected $fillable = ['id'
                            ,'title'
                        ];
}
