<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_equipment_types';
    protected $fillable = ['id'
                            ,'title'
                            ,'description'
                        ];
}
