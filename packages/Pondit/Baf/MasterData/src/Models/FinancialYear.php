<?php

namespace Pondit\Baf\MasterData\Models;

use Pondit\Baf\MasterData\Models\MasterData;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinancialYear extends MasterData
{
    use SoftDeletes;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'masterdata_financial_years';
    protected $fillable = ['id','year_range','from_year','to_year', 'created_by', 'updated_by', 'deleted_by'];
}
