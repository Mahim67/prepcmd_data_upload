<?php

namespace Pondit\Baf\MasterData\Models;

use App\Models\User;
use Kalnoy\Nestedset\NodeTrait;
use App\Models\EntityOfficeRole;
use App\Traits\RecordSequenceable;
use App\Models\PasswordChangeRequest;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\PrePCM\Models\PrePcmDemand;
use Illuminate\Database\Eloquent\SoftDeletes;

class Office extends Model
{
    use NodeTrait, RecordSequenceable, SoftDeletes;

    protected $dates    = ['deleted_at'];

    public $table = 'masterdata_offices';

    protected $fillable = [ 'name','description', '_lft', '_rgt', 'parent_id', 'sequence_number', 'is_branch', 'is_dte', 'is_base', 'is_wing', 'is_unit', 'is_sqn', 'is_sec_fly', 'unit_type', 'is_independent_unit'];

    const SUPPLY_DIR_OFFICE_ID = 15;
    /*
     |--------------------------------------------------------------------------
     | Relationships
     |--------------------------------------------------------------------------
     */
    public function concernGroups()
    {
        return $this->hasMany(Concerngroups::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function entityOfficeRoles()
    {
        return $this->hasMany(EntityOfficeRole::class);
    }

    public function passwordChangeRequests()
    {
        return $this->hasMany(PasswordChangeRequest::class);
    }

    public function users()
    {
        return $this->hasMany(User::class,'aims_officeid');
    }
    //let
    public static function directorateOffices()
    {
        return [
            "dteSup"=>15,
            "dteEng"=>12,
            "dteCnE"=>13,
            "dteAnW"=>14,
            "dteMed"=>0,
            "dat"=>0,
        ];
    }
    public static function directorateOfficesName()
    {
        return [
            "dteSup"=>"dteSup",
            "dteEng"=>"dteEng",
            "dteCnE"=>"dteCnE",
            "dteAnW"=>"dteAnW",
            "dteMed"=>"dteMed",
            "dat"=>"dat",
        ];
    }

    public function prePcm()
    {
        return $this->hasOne(PrePcmDemand::class);
    }

}
