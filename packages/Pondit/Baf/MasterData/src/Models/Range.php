<?php

namespace Pondit\Baf\MasterData\Models;

use App\Traits\RecordSequenceable;
use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\MasterData\Models\SubRange;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pondit\Baf\Budget\MasterData\Models\Budgetcode;

class Range extends Model
{
    use SoftDeletes, RecordSequenceable;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'ranges';

    protected $fillable = ['id'
                            ,'uuid'
                            ,'title'
                            ,'sequence_number'
                            ,'description'
                        ];
    /*
    **  RELATIONSHIPS
    */
    public function budgetCode(){
        return $this->belongsToMany(Budgetcode::class,'range_budgetcode','budgetcode_id','range_id');
    }
    public function subRanges()
    {
        return $this->hasMany(SubRange::class, 'range_id', 'id');
    }

}
