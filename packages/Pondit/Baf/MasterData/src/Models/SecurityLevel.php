<?php

namespace Pondit\Baf\MasterData\Models;

use Pondit\Baf\MasterData\Models\MasterData;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityLevel extends MasterData
{
    use SoftDeletes;
    
    protected $dates    = ['deleted_at'];

    protected $table    = 'masterdata_security_levels';
    protected $fillable = ['id','title','description','picture', 'is_status_approved'];
}
