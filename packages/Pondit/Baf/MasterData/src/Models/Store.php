<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\MasterData\Models\Range;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Store extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_stores';

    protected $fillable = ['id'
                            ,'title'
                            ,'description'
                        ];
    /*
    **  RELATIONSHIP 
    */

}
