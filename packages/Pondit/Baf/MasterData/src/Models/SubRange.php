<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Model;
use Pondit\Baf\MasterData\Models\Range;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SubRange extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_sub_ranges';

    protected $fillable = ['id'
                            ,'range_id'
                            ,'title'
                        ];
    /*
    **  RELATIONSHIP 
    */
    public function range()
    {
        return $this->belongsTo(Range::class, 'range_id');
    }
}
