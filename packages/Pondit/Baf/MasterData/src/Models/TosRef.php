<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TosRef extends Model
{
    use HasFactory;
    protected $table = 'masterdata_tos_refs';
    protected $fillable = [
        'id'
        ,'tos_ref_manual_name'
        ,'tos_ref_manual_page_no'
        ,'tos_ref_manual_fig_no'
        ,'tos_ref_manual_code_no'
        ,'tos_ref_manual_index'
        ,'tos_ref_description'
    ];
}
