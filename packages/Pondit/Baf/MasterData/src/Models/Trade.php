<?php

namespace Pondit\Baf\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    use HasFactory;
    protected $table    = 'masterdata_trades';

    protected $fillable = ['id'
                            ,'title'
                        ];
}
