<?php

namespace Pondit\Baf\MasterData\Models;

use Pondit\Baf\MasterData\Models\MasterData;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkflowTemplate extends MasterData
{
    use SoftDeletes;
    
    protected $dates    = ['deleted_at'];
    protected $casts = [
        'template_path' => 'array'
    ];
    protected $table    = 'masterdata_workflow_templates';
    protected $fillable = ['id',
                            'title',
                            'number',
                            'purpose',
                            'template_path',
                            'htmlized_template',
                            'dynamic_values',
                            'number_of_copies',
                            'copy_type',
                            'from_storage_table',
                            'is_storage_exists',

                            'created_by',
                            'updated_by',
                            'deleted_by'
                        ];

}
