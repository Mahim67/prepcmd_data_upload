@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Aircrafts">
    <x-pondit-form action="{{ route('aircraft.update', $data->id) }}">
        @method('PUT')
        <div class='form-group row'>
            <label name="title" class="col-sm-2 col-form-label">{{__('Title')}}</label>
            <div class='col-sm-10'>
                <input type="text" name="title" value="{{old('title') ?? $data->title }}" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label name="description" class="col-sm-2 col-form-label">{{__('Description')}}</label>
            <div class='col-sm-10'>
                <input type="text" name="description" value="{{old('description') ?? $data->description }}" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('aircraft.index')}}" tooltip="{{__('list')}}" />
            <x-pondit-act-c url="{{route('aircraft.create')}}" tooltip="{{__('create')}}" />
            <x-pondit-act-v url="{{route('aircraft.show', $data->id)}}" tooltip="{{__('show')}}" />
            <x-pondit-act-d url="{{route('aircraft.delete', $data->id)}}" tooltip="{{__('remove')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection