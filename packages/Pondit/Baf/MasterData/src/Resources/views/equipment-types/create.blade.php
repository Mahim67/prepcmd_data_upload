@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Equipment Types">
    <x-pondit-form action="{{route('equipment_type.store')}}">
        <div class='form-group row'>
            <label name="title" class="col-sm-2 col-form-label">Title</label>
            <div class='col-sm-10'>
                <input type="text" name="title" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label name="description" class="col-sm-2 col-form-label">Description</label>
            <div class='col-sm-10'>
                <input type="text" name="description" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('equipment_type.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection