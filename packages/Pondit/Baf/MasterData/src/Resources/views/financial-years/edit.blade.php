@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">
            Financial Years</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {!! Form::model( $data,[
        'route' => ['financial_years.update', $data->id],
        'method' => 'POST',
        'enctype' => 'multipart/form-data'
        ]) !!}

        <div class='form-group row'>
            {!! Form::label('year_range', 'Year Range', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('year_range', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('from_year', 'From Year', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('from_year', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('to_year', 'To Year', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('to_year', null ,['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('financial_years.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i> Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">

        </div>
        <div class="text-muted d-inline-flex">
            <a href="{{ route('financial_years.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title="Financial Years List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('financial_years.show', $data->id) }}"
                class="btn btn-outline bg-success btn-icon text-success border-success border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Financial Years Single Show" data-original-title="Top tooltip">
                <i class="fas fa-eye"></i>
            </a>
            <a href="{{ route('financial_years.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Create New Financial Years" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <span>
                {!! Form::open([
                'route' => ['financial_years.destroy', $data->id],
                'method' => 'POST'
                ]) !!}
                <button class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Remove Financial Years" data-original-title="Top tooltip" onclick="return confirm('Are You Sure!')">
                    <i class="fas fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </span>
        </div>

        <span>

        </span>
    </div>
</div>
@endsection
