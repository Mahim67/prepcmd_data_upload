@extends('pondit-limitless::layouts.master')

@push('css')
<style>
</style>
@endpush

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<div class="card">

    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Financial Years</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <table class="table datatable-colvis-basic">
        <thead>
            <tr>
                <th>Ser No</th>
                <th>Year Range</th>
                <th>From Year</th>
                <th>To Year</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($data as $item)
            <tr>
                <td>{{ $sl++ }}</td>
                <td>{{ $item->year_range }}</td>
                <td>{{ $item->from_year }}</td>
                <td>{{ $item->to_year }}</td>
                <td class="d-flex justify-content-center">
                    <span>
                        <a href="{{route('financial_years.restore', $item->id)}}"
                            class="btn bg-success btn-circle btn-circle-sm">
                            <i class=" fas fa-redo"></i>
                        </a>
                    </span>
                    <span>
                        <p class="btn bg-danger btn-circle btn-circle-sm show-alert"
                            onclick="delete_fun(this)" data-each_id = "{{ $item->id }}">
                            <i class=" fas fa-trash"></i>
                        </p>
                    </span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted"></div>
        <div class="text-muted">
            <a href="{{ route('financial_years.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Financial Years List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('financial_years.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple"  data-popup="tooltip" title="Create New Financial Years" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
        </div>

        <span>

        </span>
    </div>
</div>
<!-- /basic example -->

@endsection

@push('js')
<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js">
</script>
<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/datatables_extension_colvis.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/notifications/sweet_alert.min.js"></script>


<script>
   function delete_fun(input) {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        swalInit.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                fetchDelete (input.dataset.each_id)
                swalInit.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                );
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                swalInit.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                );
            }
        });
   } 
  
</script>

<script>
    function fetchDelete(param_id) {
        // console.log(param_id);
        fetch("/masterdata/financial-years/perdelete/"+param_id,
        {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
</script>
@endpush