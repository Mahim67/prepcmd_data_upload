@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->

    <div class="card">
        <div class="card-header card-rounded-top header-elements-inline bg-success">
            <h5 class="card-title pl-2">User Information </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="{{ route('baseupdate') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="">Office ID</label>
                            <textarea name="office_ids" id="" rows="3" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Base</label>
                            <select name="base_id" id="bases" class="form-control">
                                <option value="">--Select Base--</option>
                                @foreach ($bases as $base_id => $base_name)
                                    <option value="{{ $base_id }}">{{ $base_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" id="base_name" name="base_name">
                        <button type="submit" class="btn bg-success text-center">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- /basic example -->
@endsection

@push('js')
    <script>
        $('#bases').on("change", function(e) {
            let select = document.getElementById("base_name");
            let get_value = $(this).find('option:selected').text();
            select.setAttribute('value',get_value);
        });
    </script>
@endpush
