@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Offices</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {{ Form::open([
            'route'=>['office.store'],
            'method' => 'POST',
        ]) }}
        <div class="form-group row">
            <label class="col-sm-2 col-form-label ">Parent</label>
            <div class="col-sm-10">
                <select class="form-control select-search" name="office_id" data-fouc>
                    {!! $tree_options !!}
                </select>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('name', 'Name', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class="col-sm-10">
                {!! Form::text('name',null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('description', 'Description ', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class="col-sm-10">
                {!! Form::text('description',null, ['class'=>'form-control', 'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is HQ Office</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_hq_office" name="is_hq_office" value="1">
                        <label for="is_hq_office">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Branch</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_branch" name="is_branch" value="1">
                        <label for="is_branch">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Dte</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_dte" name="is_dte" value="1">
                        <label for="is_dte">Yes</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Base</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_base" name="is_base" value="1">
                        <label for="is_base">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Wing</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_wing" name="is_wing" value="1">
                        <label for="is_wing">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Sqn</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_sqn" name="is_sqn" value="1">
                        <label for="is_sqn">Yes</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Unit</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_unit" name="is_unit" value="1">
                        <label for="is_unit">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Independent Unit</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_independent_unit" name="is_independent_unit" value="1">
                        <label for="is_independent_unit">Yes</label>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group row">
                    <label class="col-sm-5">Is Sec Fly</label>
                    <div class="col-sm-7">
                        <input type="checkbox" id="is_sec_fly" name="is_sec_fly" value="1">
                        <label for="is_sec_fly">Yes</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>
        <div class="form-group row">
            <label class="col-sm-2">Unit Type</label>
            <div class="col-sm-10">
                <select name="unit_type" id="unit_type" class="form-control select">
                    <option value="">Select Unit Type</option>
                    <option value="fly unit">Fly Unit</option>
                    <option value="log unit">Log Unit</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
            Save</button>
        <a href="{{ route('office.index') }}" class="btn btn-sm bg-danger float-right"> <i class="fas fa-times"></i>
            Cancel</a>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted"></div>
        <div class="text-muted">
            <a href="{{ route('office.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple"
                data-popup="tooltip" title=" List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('office.tree') }}"
                class="btn btn-outline bg-brown-300 btn-icon text-brown-300 border-brown-300 border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Tree" data-original-title="Top tooltip">
                <i class="fas fa-tree"></i>
            </a>
        </div>
        <span></span>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/form_select2.js"></script>

{{-- 
<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js">
</script>
<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/datatables_extension_colvis.js">
</script> --}}
@endpush