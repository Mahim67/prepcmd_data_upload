@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')

    <div class="card">
        <div class="card-header card-rounded-top header-elements-inline bg-success">
            <h5 class="card-title pl-2">User Information </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">

            <table class="table">
                <thead>
                    <tr class="bg-secondary">
                        <td>SL</td>
                        <td>Office Name</td>
                        <td>User Name</td>
                        <td>User Service Number</td>
                        <td class="text-center">Role</td>
                    </tr>
                </thead>

                <tbody>
                    @php
                        $sl = 1;
                    @endphp
                    @foreach ($offices as $key => $office)
                        @php
                            $office_count = count($office->users);
                        @endphp
                        <tr>
                            <td rowspan="{{ $office_count > 0 ? $office_count + 1 : 2 }}">{{ $sl++ }}</td>
                            <td rowspan="{{ $office_count > 0 ? $office_count + 1 : 2 }}">{{ $office->name }} 
                                <a class="float-right"
                                    href="{{ url('/masterdata/offices/base-user-edit', $office->id) }}"><i
                                        class="fa fa-pen"></i> </a> </td>
                        </tr>
                        @if ($office_count > 0)
                            @foreach ($office->users as $office_user)
                                <tr>
                                    <td>{{ $office_user->name }}</td>
                                    <td>{{ $office_user->servicenumber }}</td>
                                    <td class="text-center">
                                        <span class="badge badge-success">getRole($office_user->id)</span>
                                        <a href="{{ route('profile.user-edit',$office_user->id) }}"
                                            class="float-right"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3" class="text-danger">There Are No Data In This Office</td>
                            </tr>
                        @endif

                        @foreach ($office->children as $office_child)
                            @if ($office_child->name != 'BASES' && $office_child->name != 'ACADEMY')
                                @php
                                    $office_child_count = count($office_child->users);
                                @endphp
                                <tr>
                                    <td rowspan="{{ $office_child_count > 0 ? $office_child_count + 1 : 2 }}">
                                        {{ $sl++ }}</td>
                                    <td rowspan="{{ $office_child_count > 0 ? $office_child_count + 1 : 2 }}"> &emsp;
                                        {{ $office_child->name }} <a class="float-right" target="blank"
                                            href="{{ url('/masterdata/offices/base-user-edit', $office_child->id) }}"><i
                                                class="fa fa-pen"></i> </a> </td>
                                </tr>
                                @if ($office_child_count > 0)
                                    @foreach ($office_child->users as $office_child_user)
                                        <tr>
                                            <td>{{ $office_child_user->name }}</td>
                                            <td>{{ $office_child_user->servicenumber }}</td>
                                            <td class="text-center">
                                                <span class="badge badge-success">getRole($office_child_user->id)</span>
                                                <a href="{{ route('profile.user-edit',$office_child_user->id) }}"
                                                    class="float-right"><i class="fas fa-edit"></i></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3" class="text-danger"> There Are No Data In This Office </td>
                                    </tr>
                                @endif

                                @foreach ($office_child->children as $offfice_child2)

                                    @php
                                        $offfice_child2_count = count($offfice_child2->users);
                                    @endphp
                                    <tr>
                                        <td rowspan="{{ $offfice_child2_count > 0 ? $offfice_child2_count + 1 : 2 }}">
                                            {{ $sl++ }}</td>
                                        <td rowspan="{{ $offfice_child2_count > 0 ? $offfice_child2_count + 1 : 2 }}">
                                            &emsp; &emsp; {{ $offfice_child2->name }} <a class="float-right"
                                                target="blank"
                                                href="{{ url('/masterdata/offices/base-user-edit', $offfice_child2->id) }}"><i
                                                    class="fa fa-pen"></i> </a> </td>
                                    </tr>

                                    @if ($offfice_child2_count > 0)
                                        @foreach ($offfice_child2->users as $users)
                                            <tr>
                                                <td class="text-success"> {{ $users->name }} </td>
                                                <td>{{ $users->servicenumber }}</td>
                                                <td class="text-center">
                                                    <span class="badge badge-success"> {{ getRole($users->id) }} </span>
                                                    <a href="{{ route('profile.user-edit',$users->id) }}"
                                                        class="float-right"><i class="fas fa-edit"></i></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="3" class="text-danger">There Are No Data In This Office</td>
                                        </tr>
                                    @endif

                                    @foreach ($offfice_child2->children as $key => $subOffice)
                                        @php
                                            $subOffice_count = count($subOffice->users);
                                        @endphp
                                        <tr>
                                            <td rowspan="{{ $subOffice_count > 0 ? $subOffice_count + 1 : 2 }}">
                                                {{ $sl++ }}</td>
                                            <td rowspan="{{ $subOffice_count > 0 ? $subOffice_count + 1 : 2 }}"> &emsp;
                                                &emsp; {{ $subOffice->name }} <a class="float-right" target="blank"
                                                    href="{{ url('/masterdata/offices/base-user-edit', $subOffice->id) }}"><i
                                                        class="fa fa-pen"></i> </a> </td>
                                        </tr>

                                        @if ($subOffice_count > 0)
                                            @foreach ($subOffice->users as $subOffice_users)
                                                <tr>
                                                    <td class="text-success">{{ $subOffice_users->name }}</td>
                                                    <td>{{ $subOffice_users->servicenumber }}</td>
                                                    <td class="text-center">
                                                        <span class="badge badge-success">
                                                            {{ getRole($subOffice_users->id) }} </span>
                                                            <a href="{{ route('profile.user-edit',$subOffice_users->id) }}"
                                                                class=" float-right"><i class="fas fa-edit"></i></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" class="text-danger">There Are No Data In This Office</td>
                                            </tr>
                                        @endif
                                        @foreach ($subOffice->children as $subOffices2)
                                            @php
                                                $subOffices2_count = count($subOffices2->users);
                                            @endphp
                                            <tr>
                                                <td rowspan="{{ $subOffices2_count > 0 ? $subOffices2_count + 1 : 2 }}">
                                                    {{ $sl++ }}</td>
                                                <td rowspan="{{ $subOffices2_count > 0 ? $subOffices2_count + 1 : 2 }}">
                                                    &emsp; &emsp;&emsp; &emsp; {{ $subOffices2->name }} <a
                                                        class="float-right" target="blank"
                                                        href="{{ url('/masterdata/offices/base-user-edit', $subOffices2->id) }}"><i
                                                            class="fa fa-pen"></i> </a> </td>
                                            </tr>

                                            @if ($subOffices2_count > 0)
                                                @foreach ($subOffices2->users as $subOffices2_users)
                                                    <tr>
                                                        <td class="text-success">{{ $subOffices2_users->name }}</td>
                                                        <td>{{ $subOffices2_users->servicenumber }}</td>
                                                        <td class="text-center">
                                                            <span class="badge badge-success"> {{ getRole($subOffices2_users->id) }} </span>
                                                            <a href="{{ route('profile.user-edit',$subOffices2_users->id) }}"
                                                                class="float-right"><i class="fas fa-edit"></i></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3" class="text-danger">There Are No Data In This Office
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endforeach
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('js')

@endpush
