@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Offices</h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <ul class="nav nav-sidebar w-50" data-nav-type="accordion">

        <li class="nav-item nav-item-submenu ">
            <a href="#" class="nav-link text-dark">
                <i class="icon-tree5"></i>
                <span id="top{{$treeTitle}}"> {{ __($treeTitle) }}</span>
            </a>
            {!! $treeData !!}
        </li>

    </ul>
    <hr>
    <table class="table datatable-colvis-basic">
        <thead>
            <tr>
                <th> Ser No </th>
                <th>Name</th>
                <th>Description</th>
                <th>Parent ID</th>
                <th>Sequence Number</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl = 1 ?>
            @foreach ($data as $single_data)
            <tr>
                <td>{{ $single_data->id }}</td>
                <td>{{ $single_data->name }}</td>
                <td>{{ $single_data->description }}</td>
                <td>{{ $single_data->parent_id }}</td>
                <td>{{ $single_data->sequence_number }}</td>

                <td class="d-flex justify-content-center">
                    <span>
                        <a href="{{route('office.show', $single_data->id)}}"
                            class="btn bg-success btn-circle btn-circle-sm">
                            <i class=" fas fa-eye"></i>
                        </a>
                    </span>
                    <span>
                        <a href="{{route('office.edit', $single_data->id)}}"
                            class="btn bg-primary btn-circle btn-circle-sm">
                            <i class=" fas fa-pen"></i>
                        </a>
                    </span>
                    <span>
                        {!! Form::open([
                        'route' => ['office.destroy', $single_data->id],
                        'method' => 'POST'
                        ]) !!}
                        <button class="btn bg-danger btn-circle btn-circle-sm"
                            onclick="return confirm('Are You Sure!')"><i class=" fas fa-trash"></i></button>
                        {!! Form::close() !!}
                    </span>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">{{count($data)}} </div>
        <div class="text-muted">
            <a href="{{ route('office.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple"
                data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <a href="{{ route('office.trash') }}"
                class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple"
                data-popup="tooltip" title="Trash List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('office.tree') }}"
                class="btn btn-outline bg-brown-300 btn-icon text-brown-300 border-brown-300 border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Tree" data-original-title="Top tooltip">
                <i class="fas fa-tree"></i>
            </a>
        </div>

        <span>

        </span>
    </div>
</div>
<!-- /basic example -->
@endsection

@push('js')

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/datatables.min.js">
</script>
<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/datatables_extension_colvis.js">
</script>

<script>
    $(document).ready(function () {
        $('#top{{$treeTitle}}').click();
    })
</script>
@endpush