@extends('pondit-profile::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="">

    <div class="card">
        <div class="card-header card-rounded-top header-elements-inline bg-success">
            <h5 class="card-title pl-2">User Information </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">
            <div class="row">
                <div class="col-md-3">
                    <a class="btn btn-primary" href="{{ url('/masterdata/offices/get-office-user') }}">Back</a>

                </div>
                <div class="col-md-6">
                    <form action="{{ route('baseuserupdate') }}" method="post">
                        @csrf
                        <input type="hidden" name="office_id" value="{{ $office->id }}">
                        <input type="hidden" name="office_name" value="{{ $office->name }}">


                        <div class="form-group row">
                            <label for="">Service Number Of Demand Raisor</label>
                            <textarea name="servicenumber_of_dimand_raisor" id="" rows="3" class="form-control" ></textarea>
                        </div>
                        <div class="form-group row">
                            <label for="">Service Number Of Authorisor</label>
                            <textarea name="servicenumber_of_auhorisor" id="" rows="3" class="form-control" ></textarea>
                        </div>
                        <button type="submit" class="btn bg-success text-center">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
{{-- 
<script>
    $(document).ready(function(){
        // $('.consolidatorBaseRange').hide()
        // $('.consolidatorHqRange').hide()
        $('.ranges').change(function () {
            let val = $(this).val()
            console.log(val);
        })
    });
    $(function () {
      $('#is_consolidator_base').click(function () {
          let range = `
          <x-pondit-baf-ranges name="range" class="ranges select-search" />`;
        $('.consolidatorBaseRange').append(range)
        
      });
      $('#is_consolidator_hq').click(function () {
        let range = `
          <x-pondit-baf-ranges name="range" class="ranges select-search" />`;
        $('.consolidatorHqRange').append(range)
      });

    });
</script> --}}

@endpush