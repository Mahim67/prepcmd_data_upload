@extends('pondit-limitless::layouts.master')

@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <!-- Basic example -->

    <div class="card">
        <div class="card-header card-rounded-top header-elements-inline bg-success">
            <h5 class="card-title pl-2">User </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0">
            <div class="row" >
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="">Office</label>
                        <select class="form-control select-search" name="office_id" id="office_id" data-fouc>
                            {!! $tree_options !!}
                        </select>
                    </div>
                    <button id="submit" class="btn bg-success float-right">Save</button>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="">Bases</label>
                        <select class="form-control select-search" name="base" id="basae" data-fouc>
                            <option value="">--Select Base--</option>
                            @foreach ($bases as $base)
                                <option value="{{ $base->id }}">{{ $base->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button id="base_btn" class="btn bg-success float-right">Save</button>
                </div>
                <div class="col-md-2 mt-4">
                    <a href="{{ route('office.air-hq-office') }}" class="btn btn-primary mt-5 float-right"> HQ Data</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header card-rounded-top header-elements-inline bg-success">
            <h5 class="card-title pl-2">User Information </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card card-body border-top-0 rounded-0 rounded-bottom tab-content mb-0" >
            <div id="office_user_content">
                <h2>Office Name: <u><b><span id="office_name" class="text-primary"></span></b> <span id="office_edit_button"></span> </u></h2>
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-secondary">
                            <td>User Name</td>
                            <td>User Service Number</td>
                            <td class="text-center">Role</td>
                        </tr>
                    </thead>
                    
                    <tbody id="tbody">
                        <tr id="tbody-row">
                            <td colspan="3" class="text-danger" ><b> Please Select Office Fist </b></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="base_user_content">
                <h2>Base Name: <b><u><span id="base_name" class="text-primary"></span></u></b></h2>
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-secondary">
                            <td>SL</td>
                            <td>Office Name</td>
                            <td>User Name</td>
                            <td>User Service Number</td>
                            <td class="text-center">Role</td>
                        </tr>
                    </thead>
                    
                    <tbody id="base-tbody">
                        <tr id="base-tbody-row">
                            <td colspan="5" class="text-danger" ><b>Please Select Base Fist</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    


    <!-- /basic example -->
@endsection

@push('js')
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
    </script>
    <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/form_select2.js"></script>


    <script>
        $(document).ready(function() {
            $('#submit').on("click", function(e) {
                $("#base_user_content").hide();
                $("#office_user_content").show();
                let office_id = $("#office_id").val();
                
                let office_name=  $("#office_name");
                office_name.html($("#office_id option:selected").text());
                $("#office_edit_button").html(
                            `
                            (<a href="{{ url('/masterdata/offices/base-user-edit/${office_id}') }}" ><i class="fas fa-edit"></i></a>)
                            `
                        )

                $.ajax({
                    url: "/masterdata/offices/get-user/" + office_id,
                    beforeSend  : function(){
                        let tr = `
                                <tr>
                                    <td class="text-center" colspan="3"><i class="fa fa-4x fa-sync fa-spin text-muted"></i></td>
                                </tr>
                            `;
                        $('#tbody').html(tr);
                    },
                    success: function(result) {
                        $("#office_name").html(result[0].aims_officename);
                        $("#office_edit_button").html(
                            `
                            (<a href="{{ url('/masterdata/offices/base-user-edit/${result[0].aims_officeid}') }}" ><i class="fas fa-edit"></i></a>)
                            `
                        )

                        console.log(result);     
                        let html = '';
                        let role = 'N/A';
                        result.forEach(function(value){
                            if (value.is_demand_raisor == '1') {
                                role = 'Demand Raisor';
                            }
                            if (value.is_authorisor_office == '1'){
                                role = 'Authorisor Office';
                            }
                            if(value.is_authorisor_base == '1'){
                                role = 'Authorisor Base';
                            }
                            if(value.is_consolidator_base == 1){
                                role = 'Consolidator Base';
                            }
                            if(value.is_consolidator_hq == 1){
                                role = 'Consolidator HQ';
                            }

                            html += `
                                    <tr>
                                        <td>${value.name}</td>
                                        <td>${value.servicenumber}</td>
                                        <td class="text-center">
                                            <span class="badge badge-success">${role}</span>
                                            <a href="{{ url('/profile/user-edit/${value.id}') }}" class="float-right"><i class="fas fa-edit"></i></a>
                                        </td>           
                                    </tr>
                                    `;
                        });
                        $("#tbody").html(html);
                    },
                    error : function(err){
                        console.log(err);
                        let msg = err.status>0 && err.responseJSON ? err.responseJSON.message : 'No Data Fount!',
                        tr  = `
                            <tr>
                                <td class="text-danger text-center h3" colspan="4">${msg}</td>
                            </tr>
                        `;
                        $('#tbody').html(tr);
                        },
                });
            });


            $('#base_btn').on("click", function(e) {
                $("#office_user_content").hide();
                $("#base_user_content").show();

                let base_id = $("#basae").val();

                $.ajax({
                    url: "/masterdata/offices/get-user-base/" + base_id,
                    beforeSend  : function(){
                        let tr = `
                                <tr>
                                    <td class="text-center" colspan="5"><i class="fa fa-2x fa-sync fa-spin text-muted"></i></td>
                                </tr>
                            `;
                        $('#base-tbody').html(tr);
                    },
                    success: function(result) {

                        $("#base_name").html(result[0].base_name);

                        console.log(result);     
                        let html = '';
                        let role = 'N/A';
                        let sl = 0 ;
                        result.forEach(function(value){
                            let count = value.users.length;
                            html +=`<tr>
                                        <td rowspan="${count > 0 ? count + 1 : 2}"><b>${++sl}</b></td>
                                        <td rowspan="${count > 0 ? count + 1 : 2}"><b>${value.name}</b> <a href="{{ url('/masterdata/offices/base-user-edit/${value.id}') }}" class="float-right"> <i class="fa fa-pen"></i></a></td>
                                    </tr>
                                    `;
                            if (value.users.length > 0) {
                                value.users.forEach(function(users){
                                    if (users.is_demand_raisor == '1') {
                                        role = 'Demand Raisor';
                                    }
                                    if (users.is_authorisor_office == '1'){
                                        role = 'Authorisor Office';
                                    }
                                    if(users.is_authorisor_base == '1'){
                                        role = 'Authorisor Base';
                                    }
                                    if(users.is_consolidator_base == 1){
                                        role = 'Consolidator Base';
                                    }
                                    if(users.is_consolidator_hq == 1){
                                        role = 'Consolidator HQ';
                                    }

                                    html += `
                                            <tr>
                                                <td>${users.name}</td>
                                                <td>${users.servicenumber}</td>
                                                <td class="text-center">
                                                    <span class="badge badge-success">${role}</span>
                                                    <a href="{{ url('/profile/user-edit/${users.id}') }}" class="float-right"><i class="fas fa-edit"></i></a>
                                                 </td>           
                                            </tr>
                                            `;
                                }); 
                            }else{
                                html += `
                                        <tr>
                                            <td colspan="3" class="text-danger">There Are No Data In This Office</td>          
                                        </tr>
                                        `
                            }
                            
                            $("#base-tbody").html(html);
                            });
                            
                    },
                    error : function(err){
                        console.log(err);
                        let msg = err.status>0 && err.responseJSON ? err.responseJSON.message : 'No Data Fount!',
                        tr  = `
                            <tr>
                                <td class="text-danger text-center h3" colspan="4">${msg}</td>
                            </tr>
                        `;
                        $('#base-tbody').html(tr);
                        },
                });
            });
        });

    </script>
@endpush
