@extends('pondit-limitless::layouts.master')


@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Offices </h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="single-show w-75 m-auto">
        <div class="card-body text-center card-img-top">
            <div class="card-img-actions d-inline-block">

                <img class="img-fluid "
                    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/images/placeholders/placeholder.jpg"
                    width="250" height="200" alt="">

            </div>
        </div>
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="  fas fa-arrows-alt"></i> Parent ID :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$parent_name}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="  fas fa-arrows-alt"></i> Name :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->name}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Description :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->description}}</div>
                    </div>
                </li>
                <div class="row">
                    <div class="col-sm-6">
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Branch :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_branch==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Dte :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_dte==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Base :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_base==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Wing :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_wing==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                    </div>
                    <div class="col-sm-6">
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Unit :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_unit==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Sqn :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_sqn==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="d-flex nav-link">
                                <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Is Sec Fly :</div>
                                <div class="mt-2 mt-sm-0 ml-3">{{$data->is_sec_fly==1?"Yes":"No"}}</div>
                            </div>
                        </li>
                    </div>
                </div>
                
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> Unit Type :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->unit_type}}</div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">

        </div>
        <div class="text-muted d-flex justify-content-between ">
            <a href="{{ route('office.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('office.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <a href="{{ route('office.edit', $data->id) }}"
                class="btn btn-outline bg-primary btn-icon text-primary border-primary border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Edit" data-original-title="Top tooltip">
                <i class="fas fa-pen"></i>
            </a>
            <span>
                {!! Form::open([
                'route' => ['office.destroy', $data->id],
                'method' => 'POST'
                ]) !!}
                <button
                    class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple mr-1"
                    data-popup="tooltip" title="Remove" data-original-title="Top tooltip"
                    onclick="return confirm('Are You Sure!')">
                    <i class="fas fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </span>
            <a href="{{ route('office.tree') }}"
                class="btn btn-outline bg-brown-300 btn-icon text-warning border-warning border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Tree" data-original-title="Top tooltip">
                <i class="fas fa-tree"></i>
            </a>
        </div>

        <span>
        </span>
    </div>
</div>
@endsection

@push('js')

{{-- <script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}

@endpush