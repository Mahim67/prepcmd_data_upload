@extends('pondit-limitless::layouts.master')

@section('content')

<x-pondit-card title="Offices Create Alter Option">
    <div class="form-group">
        <label for="">Base Office</label>
        <select name="" id="baseOffice" class="form-control select-search">
            @foreach ($baseOffices as $key=>$baseOffice)
            <option value="{{ $key }}">{{ $baseOffice }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group row">
        <div class="form-group col-sm-6">
            <label> Is Wg</label>
            <input type="radio" name="label1" value="is_wg" onclick="getLabel1(this.value)" class="ml-5">
            <label class="ml-5" >Is Unit </label>
            <input type="radio" name="label1" value="is_unit" onclick="getLabel1(this.value)" class="ml-5">
        </div>
        <div class="form-group col-sm-6"></div>
        
    </div>
</x-pondit-card>

@endsection
@push('js')
<script>

// function getLabel1(params) {
//     let label1 = params;
    
// }

let base_office = document.getElementById('baseOffice');
base_office.addEventListener('change', function () {
    let base_id = this.value;
    console.log(base_id);
    

});


</script>
@endpush