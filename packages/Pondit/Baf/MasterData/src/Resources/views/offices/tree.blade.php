@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">Offices </h5>
        <div class="header-elements">

            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        {{ Form::open([
            'route'=>['office.store'],
            'method' => 'POST',
        ]) }}
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-center">Parent</label>
            <div class="col-sm-10">
                <select class="form-control select-search" name="office_id" data-fouc>
                    {!! $data !!}
                </select>
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('name', 'Name', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class="col-sm-10">
                {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('description', 'Description', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class="col-sm-10">
                {!! Form::text('description', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <button type="submit" class="btn btn-sm bg-success float-right mb-2">Submit</button>
        {!! Form::close() !!}
    </div>

    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted"></div>
        <div class="text-muted d-flex justify-content-between ">
            <a href="{{ route('office.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('office.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
        </div>

        <span>
        </span>
    </div>
</div>
<!-- /basic example -->

@endsection

@push('js')
{{-- <script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/form_select2.js"></script>

<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/extensions/jquery_ui/core.min.js">
</script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/extensions/cookie.js"></script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/styling/uniform.min.js"></script> --}}
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/trees/fancytree_all.min.js"></script>
<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/plugins/trees/fancytree_childcounter.js"></script>

<script src="{{ asset('') }}vendor/pondit/themes/limitless/global_assets/js/demo_pages/extra_trees.js"></script>

<script>

function addNode(){
	var node = $.ui.fancytree.getTree(".tree-editable").getActiveNode();
	if( !node ) {
		alert("Please activate a parent node.");
		return;
	}
	console.log(node.key)
	node.editCreateNode("child", "New Node");

}
</script>
@endpush