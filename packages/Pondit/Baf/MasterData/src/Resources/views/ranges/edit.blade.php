@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Ranges">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-form action="{{ route('range.update', $data->id) }}">
        @method('PUT')
        <div class='form-group row'>
            <label name="sequence_number" class="col-sm-2 col-form-label">{{__('Sequence Number')}}</label>
            <div class='col-sm-10'>
                <input type="number" name="sequence_number" value="{{old('sequence_number') ?? $data->sequence_number }}" class="form-control" required/>
            </div>
        </div>
        <div class='form-group row'>
        <label name="title" class="col-sm-2 col-form-label">{{__('Title')}}</label>
            <div class='col-sm-10'>
            <input type="text" name="title" value="{{old('title') ?? $data->title }}" class="form-control"/>
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{__('Description')}}</label>
            <div class='col-sm-10'>
            <input type="text" name="description" value="{{old('description') ?? $data->description}}" class="form-control">
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('range.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('range.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('range.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('range.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}