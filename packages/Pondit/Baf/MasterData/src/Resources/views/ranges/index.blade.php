@extends('pondit-limitless::layouts.master')

@section('content')
<x-pondit-card title="Ranges">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('SL')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Sequence Number')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @foreach ($data as $key=>$datam)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $datam->title }}</td>
            <td>{{ $datam->sequence_number }}</td>
            <td>{{ $datam->description }}</td>
            <td class="d-flex">
                <x-pondit-act-link url="{{route('range.show', $datam->id)}}" icon="eye" bg="success" />
                <x-pondit-act-link url="{{route('range.edit', $datam->id)}}" icon="pen" bg="primary" />
                <x-pondit-act-link-d url="{{route('range.delete', $datam->id)}}" icon="trash" />
            </td>
        </tr>
        @endforeach
    </x-pondit-datatable>
    {{-- card footer --}}
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('range.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
