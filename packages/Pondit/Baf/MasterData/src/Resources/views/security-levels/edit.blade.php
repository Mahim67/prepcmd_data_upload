@extends('pondit-limitless::layouts.master')

@push('css')

@endpush

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">
            Security Gradings</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {!! Form::model( $data,[
        'route' => ['security_level.update', $data->id],
        'method' => 'POST',
        'enctype' => 'multipart/form-data'
        ]) !!}

        <div class='form-group row'>
            {!! Form::label('title', 'Title', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10'>
                {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('description', 'Description', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10'>
                {!! Form::text('description', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('is_status_approved', 'Status', ['class'=>'col-sm-2 col-form-label text-center']) !!}
            <div class='col-sm-10 mt-2'>
                {!! Form::radio('is_status_approved', '1', null) !!}
                {!! Form::label('is_status_approved', 'Active') !!}
                {!! Form::radio('is_status_approved', '0', null) !!}
                {!! Form::label('is_status_approved', 'Inactive') !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('picture', 'Picture', ['class'=>'col-sm-2 col-form-label text-center']) !!}

            <div class='col-sm-10'>
                {!! Form::file('picture', null ,['class'=>'form-control']) !!}
                @if ($data->picture)
                <img src="/storage/security-levels/{{ $data->picture }}" width="150px" height="auto" class="pt-2">
                @else
                <p class="text-danger mt-2">No image found</p>
                @endif
            </div>
        </div>
        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('security_level.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i>
                Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">

        </div>
        <div class="text-muted d-inline-flex">
            <a href="{{ route('security_level.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title="  List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('security_level.show', $data->id) }}"
                class="btn btn-outline bg-success btn-icon text-success border-success border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="  Single Show" data-original-title="Top tooltip">
                <i class="fas fa-eye"></i>
            </a>
            <a href="{{ route('security_level.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Create New  " data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <span>
                {!! Form::open([
                'route' => ['security_level.destroy', $data->id],
                'method' => 'POST'
                ]) !!}
                <button class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple mr-1" data-popup="tooltip" title="Remove  " data-original-title="Top tooltip" onclick="return confirm('Are You Sure!')">
                    <i class="fas fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </span>
        </div>

        <span>

        </span>
    </div>
</div>
@endsection



@push('js')
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script
    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js">
</script>
<script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/plugins/forms/selects/select2.min.js">
</script>
{{-- <script src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/js/app.js"></script> --}}
@endpush