@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Create Store">
    <x-pondit-form action="{{ route('sub_range.update', $data->id) }}">
        @method('PUT')
        <div class="row">
            <div class="col-md-2">
                <label for="">Parent Range</label>
            </div>
            <div class="col-md-10">
                <x-pondit-baf-ranges class="select-search" name="range_id" otherAttr="required" selected="{{$data->range_id}}"/>
            </div>
        </div>
        <div class='form-group row'>
        <label name="title" class="col-sm-2 col-form-label">{{__('Title')}}</label>
            <div class='col-sm-10'>
            <input type="text" name="title" value="{{old('title') ?? $data->title }}" class="form-control"/>
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('sub_range.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('sub_range.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('sub_range.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('sub_range.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}