@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Sub Ranges">
    <x-pondit-form action="{{route('sub_range.store')}}">
        <div class="row">
            <div class="col-md-2">
                <label for="">Parent Range</label>
            </div>
            <div class="col-md-10">
                <x-pondit-baf-ranges class="select-search" name="range_id" otherAttr="required"/>
            </div>
        </div>
        <div class='form-group row'>
            <label name="title" class="col-sm-2 col-form-label">Title</label>
            <div class='col-sm-10'>
                <input type="text" name="title" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('sub_range.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}