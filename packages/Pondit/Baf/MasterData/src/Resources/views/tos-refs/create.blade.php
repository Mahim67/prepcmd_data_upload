@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Sub Ranges">
    <x-pondit-form action="{{route('tos_ref.store')}}">
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{ __('Tos Ref Manual Name') }}</label>
            <div class='col-sm-10'>
                <input type="text" name="tos_ref_manual_name" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{ __('Tos Ref Manual Page No') }}</label>
            <div class='col-sm-10'>
                <input type="text" name="tos_ref_manual_page_no" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{ __('Tos Ref Manual Fig No') }}</label>
            <div class='col-sm-10'>
                <input type="text" name="tos_ref_manual_fig_no" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{ __('Tos Ref Manual Code No') }}</label>
            <div class='col-sm-10'>
                <input type="text" name="tos_ref_manual_code_no" class="form-control" />
            </div>
        </div>
        <div class='form-group row'>
            <label class="col-sm-2 col-form-label">{{ __('Tos Ref Manual Index') }}</label>
            <div class='col-sm-10'>
                <input type="text" name="tos_ref_manual_index" class="form-control" />
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-i url="{{route('tos_ref.index')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}