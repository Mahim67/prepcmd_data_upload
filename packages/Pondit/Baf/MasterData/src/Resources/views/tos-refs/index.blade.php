@extends('pondit-limitless::layouts.master')

@section('content')
<x-pondit-card title="TOs Refs">
    <x-pondit-datatable>
        <x-slot name="thead">
            <tr>
                <th>{{__('Ser No')}}</th>
                <th>{{__('TOs Ref Manual Name')}}</th>
                <th>{{__('TOs Ref Manual Page No')}}</th>
                <th>{{__('TOs Ref Manual Fig No')}}</th>
                <th>{{__('Tos Ref Manual Code No')}}</th>
                <th>{{__('TOs Ref Manual Index')}}</th>
                <th>{{__('Actions')}}</th>
            </tr>
        </x-slot>
        @if (isset($data) && ! is_null($data))
            @foreach ($data as $key=>$datam)
                <tr>
                    <td>{{ ++$key }}</td>
                    <td>{{ $datam->tos_ref_manual_name }}</td>
                    <td>{{ $datam->tos_ref_manual_page_no }}</td>
                    <td>{{ $datam->tos_ref_manual_fig_no }}</td>
                    <td>{{ $datam->tos_ref_manual_code_no }}</td>
                    <td>{{ $datam->tos_ref_manual_index }}</td>
                    <td class="d-flex">
                        <x-pondit-act-link url="{{route('tos_ref.show', $datam->id)}}" icon="eye" bg="success" />
                        <x-pondit-act-link url="{{route('tos_ref.edit', $datam->id)}}" icon="pen" bg="primary" />
                        <x-pondit-act-link-d url="{{route('tos_ref.delete', $datam->id)}}" icon="trash" />
                    </td>
                </tr>
            @endforeach
        @endif
    </x-pondit-datatable>
    {{-- card footer --}}
    <x-slot name="cardFooter">
        <div></div>
        <div>
            <x-pondit-act-c url="{{route('tos_ref.create')}}" />
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection