@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<!-- Basic example -->
<x-pondit-card title="TOs Refs">

    <div class="single-show w-75 m-auto">
        <div class="card-body text-center card-img-top">
            <div class="card-img-actions d-inline-block">
                <img class="img-fluid "
                    src="{{ asset("") }}vendor/pondit/themes/limitless/global_assets/images/placeholders/placeholder.jpg"
                    width="200" height="150" alt="">
            </div>
        </div>
        <div class="card-body p-0">
            <ul class="nav nav-sidebar mb-2">
                <li class="nav-item-header mt-0">Details</li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('TOs Ref Manual Name')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->tos_ref_manual_name}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('TOs Ref Manual Page No')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->tos_ref_manual_page_no}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('TOs Ref Manual Fig No')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->tos_ref_manual_fig_no}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('Tos Ref Manual Code No')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->tos_ref_manual_code_no}}</div>
                    </div>
                </li>
                <li class="nav-item">
                    <div class="d-flex nav-link">
                        <div class="font-weight-semibold"><i class="fas fa-arrows-alt"></i> {{__('TOs Ref Manual Index')}} :</div>
                        <div class="mt-2 mt-sm-0 ml-3">{{$data->tos_ref_manual_index}}</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('tos_ref.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('tos_ref.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-e url="{{route('tos_ref.edit', $data->id)}}" tooltip="{{__('edit')}}"/>
            <x-pondit-act-d url="{{route('tos_ref.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>

<!-- /basic example -->

@endsection
{{-- 
@push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}