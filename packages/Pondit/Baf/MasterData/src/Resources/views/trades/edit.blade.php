@extends('pondit-limitless::layouts.master')
@section('content')
<x-pondit-card title="Trades">
    {{-- {{ csrf_token() }} --}}
    <x-pondit-form action="{{ route('trade.update', $data->id) }}">
        @method('PUT')
        <div class='form-group row'>
        <label name="title" class="col-sm-2 col-form-label">{{__('Title')}}</label>
            <div class='col-sm-10'>
            <input type="text" name="title" value="{{old('title') ?? $data->title }}" class="form-control"/>
            </div>
        </div>
        <x-pondit-btn icon="check" title="{{ __('save') }}" />
        <x-pondit-btn type="reset" icon="times" bg="danger" title="{{ __('cancel') }}" />
    </x-pondit-form>

    <x-slot name="cardFooter">
        <div></div>
        <div class="d-flex">
            <x-pondit-act-i url="{{route('trade.index')}}" tooltip="{{__('list')}}"/>
            <x-pondit-act-c url="{{route('trade.create')}}" tooltip="{{__('create')}}"/>
            <x-pondit-act-v url="{{route('trade.show', $data->id)}}" tooltip="{{__('show')}}"/>
            <x-pondit-act-d url="{{route('trade.delete', $data->id)}}" tooltip="{{__('remove')}}"/>
        </div>
        <div></div>
    </x-slot>
</x-pondit-card>
@endsection
{{-- @push('js')
<script src="{{ asset('') }}{{$themepath}}assets/js/app.js"></script>
@endpush --}}