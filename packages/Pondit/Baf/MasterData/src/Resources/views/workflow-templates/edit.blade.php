@extends('pondit-limitless::layouts.master')

@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-header card-rounded-top header-elements-inline bg-success">
        <h5 class="card-title">
            Workflow Templates</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        {!! Form::model( $data,[
        'route' => ['workflow_template.update', $data->id],
        'method' => 'POST',
        'enctype' => 'multipart/form-data'
        ]) !!}
        <div class='form-group row'>
            {!! Form::label('title', 'Title', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('title', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('number', 'Number', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('number', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('purpose', 'Purpose', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('purpose', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('htmlized_template', 'Htmlized Template', ['class'=>'col-sm-2 col-form-label '])
            !!}
            <div class='col-sm-10'>
                {!! Form::textarea('htmlized_template', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('dynamic_values', 'Dynamic Values', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('dynamic_values', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('number_of_copies', 'Number of Copies', ['class'=>'col-sm-2 col-form-label '])
            !!}
            <div class='col-sm-10'>
                {!! Form::text('number_of_copies', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('copy_type', 'Copy Type', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('copy_type', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('from_storage_table', 'From Storage Table', ['class'=>'col-sm-2 col-form-label
            ']) !!}
            <div class='col-sm-10'>
                {!! Form::text('from_storage_table', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('is_storage_exists', 'Storage Status', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10 mt-2'>
                {!! Form::radio('is_storage_exists', '1', ) !!}
                {!! Form::label('is_storage_exists', 'Active') !!}
                {!! Form::radio('is_storage_exists', '0', null) !!}
                {!! Form::label('is_storage_exists', 'Inactive') !!}
            </div>
        </div>
        <div class='form-group row'>
            {!! Form::label('template_path', 'Template Path', ['class'=>'col-sm-2 col-form-label ']) !!}
            <div class='col-sm-10'>
                {!! Form::file('template_path', null, ['class'=>'form-control']) !!}
                @if ($data->template_path)
                <i class="fas fa-file fa-2x mt-2"></i>
                @else
                <p class="text-danger mt-2">Not found</p>
                @endif
            </div>
        </div>
        <div class='text-right mt-3'>
            <button type="submit" class="btn btn-sm bg-success float-right ml-1"><i class="fas fa-check"></i>
                Save</button>
            <a href="{{ route('workflow_template.index') }}" class="btn btn-sm bg-danger float-right"> <i
                    class="fas fa-times"></i>
                Cancel</a>
        </div>
        {!! Form::close() !!}
    </div>
    <div class="card-footer d-flex justify-content-between align-items-center ">
        <div class="text-muted">

        </div>
        <div class="text-muted d-inline-flex">
            <a href="{{ route('workflow_template.index') }}"
                class="btn btn-outline bg-indigo btn-icon text-indigo border-indigo border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="List" data-original-title="Top tooltip">
                <i class="fas fa-list"></i>
            </a>
            <a href="{{ route('workflow_template.show', $data->id) }}"
                class="btn btn-outline bg-success btn-icon text-success border-success border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Single Show" data-original-title="Top tooltip">
                <i class="fas fa-eye"></i>
            </a>
            <a href="{{ route('workflow_template.create') }}"
                class="btn btn-outline bg-brown btn-icon text-brown border-brown border-2 rounded-round legitRipple mr-1"
                data-popup="tooltip" title="Create" data-original-title="Top tooltip">
                <i class="fas fa-plus"></i>
            </a>
            <span>
                {!! Form::open([
                'route' => ['workflow_template.destroy', $data->id],
                'method' => 'POST'
                ]) !!}
                <button
                    class="btn btn-outline bg-danger btn-icon text-danger border-danger border-2 rounded-round legitRipple mr-1"
                    data-popup="tooltip" title="Remove" data-original-title="Top tooltip"
                    onclick="return confirm('Are You Sure!')">
                    <i class="fas fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </span>
        </div>

        <span>

        </span>
    </div>
</div>
@endsection
