<?php

use Illuminate\Support\Facades\Route;

use Pondit\Baf\MasterData\Http\Controllers\RangeController;
use Pondit\Baf\MasterData\Http\Controllers\TradeController;
use Pondit\Baf\MasterData\Http\Controllers\OfficeController;
use Pondit\Baf\MasterData\Http\Controllers\TosRefController;
use Pondit\Baf\MasterData\Http\Controllers\AircraftController;
use Pondit\Baf\MasterData\Http\Controllers\SubRangeController;
use Pondit\Baf\MasterData\Http\Controllers\EqptClassController;
use Pondit\Baf\MasterData\Http\Controllers\WorkflowPathController;
use Pondit\Baf\MasterData\Http\Controllers\EquipmentTypeController;
use Pondit\Baf\MasterData\Http\Controllers\FinancialYearController;
use Pondit\Baf\MasterData\Http\Controllers\SecurityLevelController;
use Pondit\Baf\MasterData\Http\Controllers\StoreController;
use Pondit\Baf\MasterData\Http\Controllers\WorkflowTemplateController;



Route::group(['prefix' => 'masterdata', 'middleware' => ['web'] ], function () {

    Route::group([ 'prefix' => 'financial-years', 'as' => 'financial_years.'], function () {
        Route::get('/', [FinancialYearController::class, 'index'])->name('index');
        Route::get('/show/{id}', [FinancialYearController::class, 'show'])->name('show');
        Route::get('/create', [FinancialYearController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [FinancialYearController::class, 'edit'])->name('edit');
        Route::post('/store', [FinancialYearController::class, 'store'])->name('store');
        Route::post('/update/{id}', [FinancialYearController::class, 'update'])->name('update');
        Route::post('/{id}', [FinancialYearController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [FinancialYearController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [FinancialYearController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [FinancialYearController::class, 'permanentDelete'])->name ('permanentDelete');
    });
    
    Route::group([ 'prefix' => 'offices', 'as' => 'office.'], function () {
        Route::get('/tree', [OfficeController::class, 'tree'])->name('tree');
        Route::get('/', [OfficeController::class, 'index'])->name('index');
        Route::get('/create', [OfficeController::class, 'create'])->name('create');
        Route::post('/', [OfficeController::class, 'store'])->name('store');
        Route::get('/show/{id}', [OfficeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [OfficeController::class, 'edit'])->name('edit');
        Route::put('/update/{id}', [OfficeController::class, 'update'])->name('update');
         Route::post('/{id}', [OfficeController::class, 'destroy'])->name('destroy');
         /* soft deletes */
         Route::get ('/trash', [OfficeController::class, 'trash'])->name ('trash');
         Route::get ('/{id}/restore', [OfficeController::class, 'restore'])->name ('restore');
         Route::get ('/perdelete/{id}', [OfficeController::class, 'permanentDelete'])->name ('permanentDelete');

         Route::get ('/office-base-edit', [OfficeController::class, 'officeBaseEdit'])->name ('office-base-edit');
         
         Route::get ('/get-office-user', [OfficeController::class, 'getOfficeUser'])->name ('get-office-user');
         
         Route::get ('/get-user/{id}', [OfficeController::class, 'getUser']);

         Route::get ('/get-user-base/{id}', [OfficeController::class, 'getUserBase']);

         Route::get ('/base-user-edit/{id}', [OfficeController::class, 'baseUserEdit'])->name('base-user-edit');

         Route::get ('/air-hq-office', [OfficeController::class, 'AirHQOffice'])->name('air-hq-office');
         
         Route::get ('/office-update-flag', [OfficeController::class, 'officeUpdateFlag']);

         Route::get ('/test-create', [OfficeController::class, 'testCreate'])->name('test-create');


    });
    
    Route::group(['prefix' => 'ranges', 'as' => 'range.'], function () {
        Route::get('/', [RangeController::class, 'index'])->name('index');
        Route::get('/create', [RangeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [RangeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [RangeController::class, 'edit'])->name('edit');
        Route::post('/', [RangeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [RangeController::class, 'update'])->name('update');
        Route::post('/{id}', [RangeController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'sub-ranges', 'as' => 'sub_range.'], function () {
        Route::get('/', [SubRangeController::class, 'index'])->name('index');
        Route::get('/create', [SubRangeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [SubRangeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [SubRangeController::class, 'edit'])->name('edit');
        Route::post('/', [SubRangeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [SubRangeController::class, 'update'])->name('update');
        Route::post('/{id}', [SubRangeController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'trades', 'as' => 'trade.'], function () {
        Route::get('/', [TradeController::class, 'index'])->name('index');
        Route::get('/create', [TradeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [TradeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [TradeController::class, 'edit'])->name('edit');
        Route::post('/', [TradeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [TradeController::class, 'update'])->name('update');
        Route::post('/{id}', [TradeController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'aircrafts', 'as' => 'aircraft.'], function () {
        Route::get('/', [AircraftController::class, 'index'])->name('index');
        Route::get('/create', [AircraftController::class, 'create'])->name('create');
        Route::get('/show/{id}', [AircraftController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [AircraftController::class, 'edit'])->name('edit');
        Route::post('/', [AircraftController::class, 'store'])->name('store');
        Route::put('/update/{id}', [AircraftController::class, 'update'])->name('update');
        Route::post('/{id}', [AircraftController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'equipment-classes', 'as' => 'eqpt_class.'], function () {
        Route::get('/', [EqptClassController::class, 'index'])->name('index');
        Route::get('/create', [EqptClassController::class, 'create'])->name('create');
        Route::get('/show/{id}', [EqptClassController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [EqptClassController::class, 'edit'])->name('edit');
        Route::post('/', [EqptClassController::class, 'store'])->name('store');
        Route::put('/update/{id}', [EqptClassController::class, 'update'])->name('update');
        Route::post('/{id}', [EqptClassController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'equipment-types', 'as' => 'equipment_type.'], function () {
        Route::get('/', [EquipmentTypeController::class, 'index'])->name('index');
        Route::get('/create', [EquipmentTypeController::class, 'create'])->name('create');
        Route::get('/show/{id}', [EquipmentTypeController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [EquipmentTypeController::class, 'edit'])->name('edit');
        Route::post('/', [EquipmentTypeController::class, 'store'])->name('store');
        Route::put('/update/{id}', [EquipmentTypeController::class, 'update'])->name('update');
        Route::post('/{id}', [EquipmentTypeController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'tos-refs', 'as' => 'tos_ref.'], function () {
        Route::get('/', [TosRefController::class, 'index'])->name('index');
        Route::get('/create', [TosRefController::class, 'create'])->name('create');
        Route::get('/show/{id}', [TosRefController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [TosRefController::class, 'edit'])->name('edit');
        Route::post('/', [TosRefController::class, 'store'])->name('store');
        Route::put('/update/{id}', [TosRefController::class, 'update'])->name('update');
        Route::post('/{id}', [TosRefController::class, 'destroy'])->name('delete');
    });

    Route::group([ 'prefix' => 'workflow-templates', 'as' => 'workflow_template.'], function () {
        Route::get('/', [WorkflowTemplateController::class, 'index'])->name('index');
        Route::get('/preview/{id}', [WorkflowTemplateController::class, 'preview'])->name('preview');
        Route::get('/show/{id}', [WorkflowTemplateController::class, 'show'])->name('show');
        Route::get('/create', [WorkflowTemplateController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [WorkflowTemplateController::class, 'edit'])->name('edit');
        Route::post('/store', [WorkflowTemplateController::class, 'store'])->name('store');
        Route::post('/update/{id}', [WorkflowTemplateController::class, 'update'])->name('update');
        Route::post('/{id}', [WorkflowTemplateController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [WorkflowTemplateController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [WorkflowTemplateController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [WorkflowTemplateController::class, 'permanentDelete'])->name ('permanentDelete');
    });

    Route::group(['prefix' => 'workflow-paths', 'as' => 'workflow_path.'], function () {
        Route::get('/', [WorkflowPathController::class, 'index'])->name('index');
        Route::get('/create', [WorkflowPathController::class, 'create'])->name('create');
        Route::get('/show/{id}', [WorkflowPathController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [WorkflowPathController::class, 'edit'])->name('edit');
        Route::post('/store', [WorkflowPathController::class, 'store'])->name('store');
        Route::put('/update/{id}', [WorkflowPathController::class, 'update'])->name('update');
        Route::post('/{id}', [WorkflowPathController::class, 'destroy'])->name('destroy');
    });
    Route::group([ 'prefix' => 'security-levels', 'as' => 'security_level.'], function () {

        Route::get('/', [SecurityLevelController::class, 'index'])->name('index');
        Route::get('/show/{id}', [SecurityLevelController::class, 'show'])->name('show');
        Route::get('/create', [SecurityLevelController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [SecurityLevelController::class, 'edit'])->name('edit');
        Route::post('/store', [SecurityLevelController::class, 'store'])->name('store');
        Route::post('/update/{id}', [SecurityLevelController::class, 'update'])->name('update');
        Route::post('/{id}', [SecurityLevelController::class, 'destroy'])->name('destroy');
        // soft deletes
        Route::get ('/trash', [SecurityLevelController::class, 'trash'])->name ('trash');
        Route::get ('/{id}/restore', [SecurityLevelController::class, 'restore'])->name ('restore');
        Route::get ('/perdelete/{id}', [SecurityLevelController::class, 'permanentDelete'])->name ('permanentDelete');

    });
    Route::group([ 'prefix' => 'stores', 'as' => 'store.'], function () {

        Route::get('/', [StoreController::class, 'index'])->name('index');
        Route::get('/show/{id}', [StoreController::class, 'show'])->name('show');
        Route::get('/create', [StoreController::class, 'create'])->name('create');
        Route::get('/{id}/edit', [StoreController::class, 'edit'])->name('edit');
        Route::post('/store', [StoreController::class, 'store'])->name('store');
        Route::post('/update/{id}', [StoreController::class, 'update'])->name('update');
        Route::post('/{id}', [StoreController::class, 'destroy'])->name('delete');

    });

});