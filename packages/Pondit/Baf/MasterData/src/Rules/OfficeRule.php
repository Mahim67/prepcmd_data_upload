<?php

namespace Pondit\Baf\MasterData\Rules;

use Illuminate\Contracts\Validation\Rule;

class OfficeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // return preg_match('/\s/', $value) === 0 && $value == ('digits:9');
        if (preg_match('/\s/', $value) === 0 && $value == ('digits:9')) {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
