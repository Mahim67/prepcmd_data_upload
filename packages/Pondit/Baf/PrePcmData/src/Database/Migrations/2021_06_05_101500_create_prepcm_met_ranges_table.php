<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrepcmMetRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepcm_met_ranges', function (Blueprint $table) {
            $table->id();
            $table->string('ser_no')->nullable();
            $table->string('part_no')->nullable();
            $table->string('name_of_eqpt')->nullable();
            $table->bigInteger('range_id')->nullable();
            $table->string('range_name')->nullable();
            $table->bigInteger('office_id')->nullable();
            $table->string('name_of_user')->nullable();
            $table->string('office_name_from_user')->nullable();
            $table->string('ph_baseunit_all_ranges')->nullable();
            $table->string('ph_baseunit_met_range')->nullable();
            $table->string('ph_user_all_ranges')->nullable();
            $table->string('ph_user_met_range')->nullable();
            $table->bigInteger('qty')->default(0)->nullable();
            $table->string('unit')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('estimated_unit_price', 12,2)->default(0)->nullable();
            $table->decimal('estimated_total_cost', 14,2)->default(0)->nullable();
            $table->bigInteger('recom_qty_by_opi_dte')->default(0)->nullable();
            $table->decimal('recom_unit_price_by_opi_dte', 12,2)->default(0)->nullable();
            $table->decimal('recom_total_cost_by_opi_dte', 14,2)->default(0)->nullable();
            $table->text('justification')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepcm_met_ranges');
    }
}
