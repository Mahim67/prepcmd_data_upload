<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddThirdSupplyRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prepcm_supply_ranges', function (Blueprint $table) {
            $table->string('item_group_name', 500)->nullable();
            $table->unsignedInteger('budgetcode_id')->nullable();
            $table->string('budgetcode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prepcm_supply_ranges', function (Blueprint $table) {
            //
        });
    }
}
