<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSecondColumnInPrepcmTrgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prepcm_trg_ranges', function (Blueprint $table) {
            $table->bigInteger('recom_qty_by_prepcm')->default(0)->nullable();
            $table->decimal('recom_unit_price_by_prepcm', 12,2)->default(0)->nullable();
            $table->decimal('recom_total_cost_by_prepcm', 14,2)->default(0)->nullable();
            $table->bigInteger('approved_qty_by_pcm')->default(0)->nullable();
            $table->decimal('approved_unit_price_by_pcm', 12,2)->default(0)->nullable();
            $table->decimal('approved_total_cost_by_pcm', 14,2)->default(0)->nullable();
            $table->string('item_group_name', 500)->nullable();
            $table->unsignedInteger('budgetcode_id')->nullable();
            $table->string('budgetcode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prepcm_trg_ranges', function (Blueprint $table) {
            $table->dropColumn('recom_qty_by_prepcm');
            $table->dropColumn('recom_unit_price_by_prepcm');
            $table->dropColumn('recom_total_cost_by_prepcm');
            $table->dropColumn('approved_qty_by_pcm');
            $table->dropColumn('approved_unit_price_by_pcm');
            $table->dropColumn('approved_total_cost_by_pcm');
            $table->dropColumn('item_group_name');
            $table->dropColumn('budgetcode_id');
            $table->dropColumn('budgetcode');
        });
    }
}
