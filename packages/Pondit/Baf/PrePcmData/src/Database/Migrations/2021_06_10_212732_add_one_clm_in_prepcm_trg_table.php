<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOneClmInPrepcmTrgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('prepcm-data')->table('prepcm_trg_ranges', function (Blueprint $table) {
            $table->integer('item_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('prepcm-data')->table('prepcm_trg_ranges', function (Blueprint $table) {
            $table->dropColumn('item_id');
        });
    }
}
