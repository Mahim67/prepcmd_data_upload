<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrepcmCndRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepcm_cnd_ranges', function (Blueprint $table) {
            $table->id();
            $table->string('ser_no')->nullable();
            $table->string('part_no')->nullable();
            $table->bigInteger('item_id')->nullable();
            $table->string('name_of_eqpt', 1000)->nullable();
            $table->bigInteger('office_id')->nullable();
            $table->string('name_of_user')->nullable();
            $table->string('office_name_from_user')->nullable();

            $table->string('base_name')->nullable();
            $table->bigInteger('range_id')->nullable();
            $table->string('range_name')->nullable();
            $table->bigInteger('subrange_id')->nullable();
            $table->string('subrange_name')->nullable();
            $table->bigInteger('class_id')->nullable();
            $table->string('class_name')->nullable();
            $table->bigInteger('eqpt_type_id')->nullable();
            $table->string('eqpt_type_name')->nullable();
            $table->bigInteger('trade_id')->nullable();
            $table->string('trade_name')->nullable();
            $table->bigInteger('aircraft_id')->nullable();
            $table->string('aircraft_name')->nullable();

            $table->string('main_eqpt_in_system')->nullable();
            $table->string('qty_used_in_sys')->nullable();

            $table->string('ph_baseunit_cnd_range')->nullable();
            $table->string('ph_baseunit_svc_fitted')->nullable();
            $table->string('ph_baseunit_svc_spares')->nullable();
            $table->string('ph_baseunit_svc')->nullable();
            $table->string('ph_baseunit_svc_bd')->nullable();
            $table->string('ph_baseunit_svc_un')->nullable();
            $table->string('ph_baseunit_us')->nullable();
            $table->string('ph_baseunit_rs')->nullable();
            $table->string('ph_baseunit_rd')->nullable();
            $table->string('ph_baseunit_ur')->nullable();
            $table->string('ph_baseunit_sal')->nullable();
            $table->string('ph_baseunit_ttl')->nullable();
            $table->string('life_exp')->nullable();
            $table->string('last_year_consumption')->nullable();
            $table->string('last_5year_consumption')->nullable();
            $table->string('spt_contact_rcvd')->nullable();
            $table->string('last_year_qty_dmd')->nullable();
            $table->bigInteger('qty')->default(0)->nullable();
            $table->string('unit')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('estimated_unit_price', 12,2)->default(0)->nullable();
            $table->decimal('estimated_total_cost', 14,2)->default(0)->nullable();
            $table->bigInteger('recom_qty_by_opi_dte')->default(0)->nullable();
            $table->decimal('recom_unit_price_by_opi_dte', 12,2)->default(0)->nullable();
            $table->decimal('recom_total_cost_by_opi_dte', 14,2)->default(0)->nullable();
            $table->bigInteger('recom_qty_by_prepcm')->default(0)->nullable();
            $table->decimal('recom_unit_price_by_prepcm', 12,2)->default(0)->nullable();
            $table->decimal('recom_total_cost_by_prepcm', 14,2)->default(0)->nullable();
            $table->bigInteger('approved_qty_by_pcm')->default(0)->nullable();
            $table->decimal('approved_unit_price_by_pcm', 12,2)->default(0)->nullable();
            $table->decimal('approved_total_cost_by_pcm', 14,2)->default(0)->nullable();
            $table->string('item_group_name', 500)->nullable();
            $table->unsignedInteger('budgetcode_id')->nullable();
            $table->string('budgetcode')->nullable();

            $table->text('justification')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepcm_cnd_ranges');
    }
}
