<?php

namespace Pondit\Baf\PrePcmData\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetCodeToItem;
use Pondit\Baf\PrePcmData\Models\PrePcmArmtRange;

class PrePcmArmtRangeController extends Controller
{
    const PAGINATION_ROW_PER_PAGE = 20;
    public function createArmtRange ()
    {
        $data = PrePcmArmtRange::all();
        return view('prePcmData::armt-range.create', compact('data'));
    }

    public function index ()
    {
        $rowsPerPage = self::PAGINATION_ROW_PER_PAGE;
        return view('prePcmData::armt-range.index', compact('rowsPerPage'));
    }


    /**
     * Index JQGrid & JExcel (Supply Range)
     */
    public function jqgrid()
    {
        return view('prePcmData::armt-range.jqgrid');
    }
    public function jexcel()
    {
        $data = PrePcmArmtRange::select('part_no','name_of_eqpt','base_name','name_of_user','justification')->get();
//        return response()->json($data);
       return view('prePcmData::armt-range.jexcel', compact('data'));
    }




    /**
     * API Request Data (Supply Range)
     */
    public function retrieveData()
    {
        $data = PrePcmArmtRange::orderBy('id', 'asc')->get();
        return response()->json($data);
    }

    public function getArmtQtyDmdInfo($id)
    {
        $office = PrePcmArmtRange::where('id', $id)->first();
        $data = PrePcmArmtRange::where('base_name', $office->base_name)->where('name_of_eqpt', $office->name_of_eqpt)->get();
        $estimateTotalCost          = PrePcmArmtRange::where('base_name', $office->base_name)->sum('estimated_total_cost');
        $opiTotalCost               = PrePcmArmtRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_opi_dte');
        $prePcmTotalCost            = PrePcmArmtRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_prepcm');
        $budgetcode                 = BudgetCodeToItem::where('item_name', $office->name_of_eqpt)->first();

        return response()->json([
            'status'    => 'ok',
            'data'      => $data,
            'estimateTotalCost' => $estimateTotalCost,
            'opiTotalCost'      => $opiTotalCost,
            'prePcmTotalCost'   => $prePcmTotalCost,
            'budgetcode'        => $budgetcode->budgetcode ?? ''
        ]);
    }
    public function armtStoreQtyDmd(Request $request)
    {
        try {
            $office = PrePcmArmtRange::where('id', $request->id)->first();
            /** Value Update From Pre PCM Activity **/
            if (array_key_exists('qty_prepcm', $request->all())) {
                $sumvalue = (float)$request->qty_prepcm * $office->recom_unit_price_by_prepcm;
                $data = [
                    'recom_qty_by_prepcm'           => $request->qty_prepcm,
                    'recom_total_cost_by_prepcm'    => $sumvalue ?? 0
                ];
            }
            /** Value Update From Opi Dte Activity **/
            elseif (array_key_exists('qty', $request->all())) {
                $sumvalue = (float)$request->qty * $office->recom_unit_price_by_opi_dte;
                $data = [
                    'recom_qty_by_opi_dte'           => $request->qty,
                    'recom_total_cost_by_opi_dte'    => $sumvalue ?? 0
                ];
            }

            $update = PrePcmArmtRange::where('id', $request->id)->update($data);
            if (isset($update)) {
                // $estimateTotalCost       = PrePcmArmtRange::where('base_name', $office->base_name)->sum('estimated_total_cost');
                $opiTotalCost       = PrePcmArmtRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_opi_dte');
                $prePcmTotalCost    = PrePcmArmtRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_prepcm');
                $data = [
                    'opiTotalCost' => $opiTotalCost,
                    'prePcmTotalCost'   => $prePcmTotalCost,
                ];
                return response()->json([
                    'status' => 'success',
                    'total_amount' => $data
                ]);
            }
        } catch (\Exception $th) {
            throw new Exception("Error Processing Request", 1);
        }
    }


    public function getArmtData(){

        $paginatePerPage = !is_null(\request('rows_per_page')) ? \request('rows_per_page') : self::PAGINATION_ROW_PER_PAGE;
        $filterableColumns = \request('filterable_columns');

        $query  = PrePcmArmtRange::orderBy('name_of_eqpt', 'asc')->orderBy('base_name', 'asc')->orderBy('name_of_user', 'asc')->get()->groupBy('name_of_eqpt');
       
        /*Filtering by column*/
        if (!is_null($filterableColumns)) {
            $columns = explode(';', $filterableColumns);
            foreach ($columns as $column) {
                $columnName = explode('|', $column);
                if (isset($columnName[0]) && isset($columnName[1])) {
                    if ($columnName[1]) {
                        $query  = PrePcmArmtRange::orderBy('name_of_eqpt', 'asc')->orderBy('base_name', 'asc')->orderBy('name_of_user', 'asc')->whereRaw('LOWER('.$columnName[0].') like ?', [ "%" . strtolower($columnName[1]) . "%"])->get()->groupBy('name_of_eqpt');
                    }
                }
            }
        }
        //////////////////////////////////////////////////////////
        // $demands = $query->paginate($paginatePerPage);
        // $demandArray = $demands;

     
            return response()->json([
                'status'        => 'ok',
                'data'          => $query,
                // 'pages'         => $this->getPages($demandArray->currentPage(), $demandArray->lastPage(), $demandArray->total()),
                'sl'            => !is_null(\request()->page) ? (\request()->page - 1) * $paginatePerPage : 0
    
            ]);
            
        
    }


    private function getPages($currentPage, $lastPage, $totalPages)
    {
        $startPage = ($currentPage < 5) ? 1 : $currentPage - 4;
        $endPage = 8 + $startPage;
        $endPage = ($totalPages < $endPage) ? $totalPages : $endPage;
        $diff = $startPage - $endPage + 8;
        $startPage -= ($startPage - $diff > 0) ? $diff : 0;
        $pages = [];

        if ($startPage > 1) {
            $pages[] = '...';
        }

        for ($i = $startPage; $i <= $endPage && $i <= $lastPage; $i++) {
            $pages[] = $i;
        }

        if ($currentPage < $lastPage) {
            $pages[] = '...';
        }

        return $pages;
    }
}
