<?php

namespace Pondit\Baf\PrePcmData\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pondit\Baf\PrePcmData\Models\PrePcmCndRange;

class PrePcmCndRangeController extends Controller
{
    public function createCndRange ()
    {
        $data = PrePcmCndRange::all();
        return view('prePcmData::cnd-range.create', compact('data'));
    }
}
