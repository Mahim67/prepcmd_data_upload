<?php

namespace Pondit\Baf\PrePcmData\Http\Controllers;

use Exception;
use App\Models\PItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Pondit\Baf\Budget\Models\BudgetAllotment;
use Pondit\Baf\Budget\Models\BudgetCodeToItem;
use Pondit\Baf\PrePcmData\Models\PrePcmSupplyRange;

class PrePcmSupplyRangeController extends Controller
{
    public function createSupplyRange ()
    {
        $data = PrePcmSupplyRange::all();
        return view('prePcmData::supply-range.create', compact('data'));
    }

    public function index ()
    {
        $data = PrePcmSupplyRange::orderBy('name_of_eqpt', 'asc')->orderBy('base_name', 'asc')->orderBy('name_of_user', 'asc')->get()->groupBy('name_of_eqpt');
        return view('prePcmData::supply-range.index', compact('data'));
    }




    /**
     * Index JQGrid & JExcel (Supply Range)
     */
    public function jqgrid()
    {
        return view('prePcmData::supply-range.jqgrid');
    }
    public function jexcel()
    {
        $data = PrePcmSupplyRange::select('part_no','name_of_eqpt','base_name','name_of_user','justification')->get();
//        return response()->json($data);
       return view('prePcmData::supply-range.jexcel', compact('data'));
    }




    /**
     * API Request Data (Supply Range)
     */
    public function retrieveData()
    {
        $data = PrePcmSupplyRange::orderBy('id', 'asc')->get();
        return response()->json($data);
    }

    public function getQtyDmdInfo($id)
    {
        $office = PrePcmSupplyRange::where('id', $id)->first();
        $data = PrePcmSupplyRange::where('base_name', $office->base_name)->where('name_of_eqpt', $office->name_of_eqpt)->get();
        $budgetcode         = BudgetCodeToItem::where('item_name', $office->name_of_eqpt)->first();
        /** Ask to MZR sir for total amount ki Base Wise Show Hobe kina...???? */
        $estimateTotalCost  = PrePcmSupplyRange::where('base_name', $office->base_name)->where('budgetcode_id', $budgetcode->budgetcode_id)->sum('estimated_total_cost');
        $opiTotalCost       = PrePcmSupplyRange::where('base_name', $office->base_name)->where('budgetcode_id', $budgetcode->budgetcode_id)->sum('recom_total_cost_by_opi_dte');
        $prePcmTotalCost    = PrePcmSupplyRange::where('base_name', $office->base_name)->where('budgetcode_id', $budgetcode->budgetcode_id)->sum('recom_total_cost_by_prepcm');
        $budgetAlloted      = BudgetAllotment::where('budgetcode_id', $budgetcode->budgetcode_id)->sum('total_budgetcode_amount');
        // dd($budgetcode->budgetcode);
        return response()->json([
            'status'    => 'ok',
            'data'      => $data,
            'estimateTotalCost' => $estimateTotalCost,
            'opiTotalCost'      => $opiTotalCost,
            'prePcmTotalCost'   => $prePcmTotalCost,
            'budgetcode'        => $budgetcode->budgetcode,
            'budgetAlloted'     => $budgetAlloted
        ]);
    }
    public function storeQtyDmd(Request $request)
    {
        try {
            $office = PrePcmSupplyRange::where('id', $request->id)->first();
            /** Value Update From Pre PCM Activity **/
            if (array_key_exists('qty_prepcm', $request->all())) {
                $sumvalue = (float)$request->qty_prepcm * $office->recom_unit_price_by_prepcm;
                $data = [
                    'recom_qty_by_prepcm'           => $request->qty_prepcm,
                    'recom_total_cost_by_prepcm'    => $sumvalue ?? 0
                ];
            }
            /** Value Update From Opi Dte Activity **/
            elseif (array_key_exists('qty', $request->all())) {
                $sumvalue = (float)$request->qty * $office->recom_unit_price_by_opi_dte;
                $data = [
                    'recom_qty_by_opi_dte'           => $request->qty,
                    'recom_total_cost_by_opi_dte'    => $sumvalue ?? 0
                ];
            }

            $update = PrePcmSupplyRange::where('id', $request->id)->update($data);
            if (isset($update)) {
                // $estimateTotalCost       = PrePcmSupplyRange::where('base_name', $office->base_name)->sum('estimated_total_cost');
                $opiTotalCost       = PrePcmSupplyRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_opi_dte');
                $prePcmTotalCost    = PrePcmSupplyRange::where('base_name', $office->base_name)->sum('recom_total_cost_by_prepcm');
                $data = [
                    'opiTotalCost' => $opiTotalCost,
                    'prePcmTotalCost'   => $prePcmTotalCost,
                ];
                return response()->json([
                    'status' => 'success',
                    'total_amount' => $data
                ]);
            }
        } catch (\Exception $th) {
            throw new Exception("Error Processing Request", 1);
        }
    }

    public function itemFilter($param)
    {
        $search = strtolower(trim($param));
        $data  = PrePcmSupplyRange::where(DB::raw('LOWER(name_of_eqpt)') , 'LIKE', '%'.$search.'%')
            ->orWhere(DB::raw('LOWER(base_name)') , 'LIKE', '%'.$search.'%')
            ->orderBy('name_of_eqpt', 'asc')
            ->orderBy('base_name', 'asc')
            ->orderBy('name_of_user', 'asc')
            ->get();
        return response()->json([
            'status'    => 'ok',
            'data'      => $data
        ]);
    }


    /**
     * For SCRIPTS
     */
    public function updateSupplyData(){
        $suplyDatas = PrePcmSupplyRange::all();

        foreach ($suplyDatas as $suplyData) {
            if ( isset($suplyData->name_of_eqpt) && is_null($suplyData->item_id)) {

                $product = PItem::where('title',$suplyData->name_of_eqpt)->first();
                if (!is_null($product)) {
                    PrePcmSupplyRange::where('id',$suplyData->id)->update(['item_id' => $product->id]);
                }
                else{
                    $itemData = [
                        'title'                          => $suplyData->name_of_eqpt,
                        'w_part_no'                      => $suplyData->part_no ?? null,
                        'is_suggested_item'              => 2,
                    ];

                    $insertProduct = PItem::create($itemData);
                    PrePcmSupplyRange::where('id',$suplyData->id)->update(['item_id' => $insertProduct->id]);
                }
            }

        }

        return response()->json([
            'status'    => 'ok'
        ]);
    }

    public function updateBudgetCode(){

        $suplyDatas = PrePcmSupplyRange::all();
        $UpdateCount = 0 ;
        $notUpdateCount = 0 ;
        foreach ($suplyDatas as $suplyData) {
            $budgetcodeItems = BudgetCodeToItem::where('item_id',$suplyData->item_id)->first();
            
            if (!is_null($budgetcodeItems)) {
                PrePcmSupplyRange::where('id',$suplyData->id)->update([

                        'budgetcode_id' => $budgetcodeItems->budgetcode_id,
                        'budgetcode'    => $budgetcodeItems->budgetcode->newcode

                    ]);
                $UpdateCount++ ;
            }else{
                $notUpdateCount++ ;
            }
        }

        $nullBudgetCode = PrePcmSupplyRange::whereNull('budgetcode_id')->pluck('name_of_eqpt')->toArray();
        
        return response()->json([
            'status'                => 'ok',
            'No of Update Row'      => $UpdateCount,
            'No of Not Update Row'  => $notUpdateCount,
            'Did Not Update'        => $nullBudgetCode, 
        ]);
    }
}