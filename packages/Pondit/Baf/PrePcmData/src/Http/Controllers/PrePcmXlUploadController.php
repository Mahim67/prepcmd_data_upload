<?php

namespace Pondit\Baf\PrePcmData\Http\Controllers;

use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Pondit\Baf\MasterData\Models\Range;
use Pondit\Baf\PrePcmData\Models\PrePcmCndRange;
use Pondit\Baf\PrePcmData\Models\PrePcmMedRange;
use Pondit\Baf\PrePcmData\Models\PrePcmMetRange;
use Pondit\Baf\PrePcmData\Models\PrePcmTrgRange;
use Pondit\Baf\PrePcmData\Models\PrePcmArmtRange;
use Pondit\Baf\PrePcmData\Models\PrePcmSupplyRange;
use Pondit\GeneralizeXlUpload\Imports\GeneralizedExcelImport;

class PrePcmXlUploadController extends Controller
{
    public function create()
    {
        $ranges = Range::orderBy('title','asc')->pluck('title', 'id')->toArray();
        return view('prePcmData::create', compact('ranges'));
    }
    public function createArmtRange ()
    {
        $data = PrePcmArmtRange::all();
        return view('prePcmData::armt-range.create', compact('data'));
    }
    public function createTrgRange ()
    {
        $data = PrePcmTrgRange::all();
        return view('prePcmData::trg-range.create', compact('data'));
    }
    public function createMedRange ()
    {
        $data = PrePcmMedRange::all();
        return view('prePcmData::med-range.create', compact('data'));
    }
    public function createMetRange ()
    {
        $data = PrePcmMetRange::all();
        return view('prePcmData::met-range.create', compact('data'));
    }


    public function store(Request $request)
    {
        try {
            // dd($request->all());
            $filePathData = null;
            if (($request->has('excel_file_path'))) {
                $file_path = $request->file('excel_file_path')->store('/public/excel_file/');
                $filePathData = storage_path("app/" . $file_path);
                
                $originalFileName = $request->file('excel_file_path')->getClientOriginalName();
                $colName = explode(',',$originalFileName);
                
                $range = self::rangeName($request->range);

                if (isset($range)) {
                    $columnsInfo = self::rangeWiseColumnInfo($range['range_name']);
                    $whiteListedExcel = self::rangeWiseXlFormat($range['range_name']);
                    $modelName = self::modelName($range['range_name']);
                }
                // dd($modelName,$columnsInfo);
                
                $validationInfo = [
                    "*.qty"                     => ['nullable'],
                    "*.estimated_unit_price"    => ['nullable','min:0','max:99999999999','regex:/^\d+(\.\d{1,2})?$/'],
                    "*.estimated_total_cost"    => ['nullable','min:0','max:99999999999','regex:/^\d+(\.\d{1,2})?$/'],
                    
                ];
                
                $hiddenParseData = [ 
                    'range_id'      => $range['range_id'] ?? null,
                    'range_name'    => $range['range_name'] ?? null
                ];
                $itemsInport = new GeneralizedExcelImport($modelName, $columnsInfo, $validationInfo, $whiteListedExcel, $hiddenParseData);
                $itemsInport->import($filePathData);
               
                return redirect()->back()->withSuccess('Successfully Uploaded!');
            }
        } 
        catch (QueryException $th) {
           
            return redirect()->back()->withErrors($th->getMessage());
        }
    }




    protected static function modelName ($rangeName)
    {
        if ($rangeName == "Supply Range") {
            return PrePcmSupplyRange::class;
        }
        elseif ($rangeName == "Armt Range") {
            return PrePcmArmtRange::class;
        }
        elseif ($rangeName == "Trg Range") {
            return PrePcmTrgRange::class;
        } 
        elseif ($rangeName == "Med Range") {
            return PrePcmMedRange::class;
        }
        elseif ($rangeName == "Met Range") {
            return PrePcmMetRange::class;
        }  
        elseif ($rangeName == "C&E Range") {
            return PrePcmCndRange::class;
        }        
    }
    protected static function rangeName ($param)
    {
        $range = Range::where('title', $param)->first();
        $data = [
            'range_id'      => $range->id,
            'range_name'    => $range->title  
        ];
        if (is_null($data)) {
            throw new Exception("Range not found!", 404);
        }
        return $data;
    }


    protected static function rangeWiseXlFormat($rangeName)
    {
        if ($rangeName == "Supply Range") {
            return PrePcmSupplyRange::xlFormat();
        }
        elseif ($rangeName == "Armt Range") {
            return PrePcmArmtRange::xlFormat();
        }
        elseif ($rangeName == "Trg Range") {
            return PrePcmTrgRange::xlFormat();
        }
        elseif ($rangeName == "Med Range") {
            return PrePcmMedRange::xlFormat();
        }
        elseif ($rangeName == "Met Range") {
            return PrePcmMetRange::xlFormat();
        }
        elseif ($rangeName == "C&E Range") {
            return PrePcmCndRange::xlFormat();
        }
        // dd($data);
    }
    /**
     * For Column Info
     */
    protected static function rangeWiseColumnInfo($rangeName)
    {
        if ($rangeName == "Supply Range") {
            return PrePcmSupplyRange::column();
        }
        elseif ($rangeName == "Armt Range") {
            return PrePcmArmtRange::column();
        }
        elseif ($rangeName == "Trg Range") {
            return PrePcmTrgRange::column();
        }
        elseif ($rangeName == "Med Range") {
            return PrePcmMedRange::column();
        }
        elseif ($rangeName == "Met Range") {
            return PrePcmMetRange::column();
        }
        elseif ($rangeName == "C&E Range") {
            return PrePcmCndRange::column();
        }
        // dd($data);
    }




    
}
