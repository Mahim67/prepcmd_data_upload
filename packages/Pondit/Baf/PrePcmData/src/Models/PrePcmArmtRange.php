<?php

namespace Pondit\Baf\PrePcmData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrePcmArmtRange extends Model
{
    use HasFactory;
    protected $table = "prepcm_armt_ranges";
    protected $fillable = [
        'id',
        'ser_no',
        'item_group',
        'part_no',
        'name_of_eqpt',
        'name_of_user',
        'office_id',
        'office_name_from_user',
        'ph_baseunit_armt_range',
        'ph_user_armt_range',
        'qty',
        'unit',
        'currency',
        'estimated_unit_price',
        'estimated_total_cost',
        'recom_qty_by_opi_dte',
        'recom_unit_price_by_opi_dte',
        'recom_total_cost_by_opi_dte',
        'justification',

        'range_id',
        'range_name',
        'base_name',
        'recom_qty_by_prepcm',
        'recom_unit_price_by_prepcm',
        'recom_total_cost_by_prepcm',
        'approved_qty_by_pcm',
        'approved_unit_price_by_pcm',
        'approved_total_cost_by_pcm'
    ];
    protected static function column () 
    {
        return $list = [
            'ser_no',
            'item_group',
            'part_no',
            'name_of_eqpt',
            'name_of_user',
            'office_id',
            'office_name_from_user',
            'ph_baseunit_armt_range',
            'ph_user_armt_range',
            'qty',
            'unit',
            'currency',
            'estimated_unit_price',
            'estimated_total_cost',
            'recom_qty_by_opi_dte',
            'recom_unit_price_by_opi_dte',
            'recom_total_cost_by_opi_dte',
            'justification',
            'range_id',
            'range_name',
            'base_name',
            'recom_qty_by_prepcm',
            'recom_unit_price_by_prepcm',
            'recom_total_cost_by_prepcm',
            'approved_qty_by_pcm',
            'approved_unit_price_by_pcm',
            'approved_total_cost_by_pcm'


        ];
    }

    protected static function xlFormat () 
    {
        return $list = [
            //excel column   => 'db column'
            'ser_no'                                        => 'ser_no',
            'item_group'                                    => 'item_group',
            'part_no'                                       => 'part_no',
            'name_of_eqpt'                                  => 'name_of_eqpt',
            'name_of_user_unitsqnsecflt'                    => 'name_of_user',
            'base_name'                                     => 'base_name',
            'office_id'                                     => 'office_id',
            'office_name_from_user'                         => 'office_name_from_user',
            'present_holding_in_baseunit_armt_range'        => 'ph_baseunit_armt_range',
            'present_holding_at_user_sqnfltsec_armt_range'  => 'ph_user_armt_range',
            'dmd_qty'                                       => 'qty',
            'unit'                                          => 'unit',
            'currency'                                      => 'currency',
            'estimated_unit_price'                          => 'estimated_unit_price',
            'total_cost'                                    => 'estimated_total_cost',
            'justification'                                 => 'justification'
        ];
    }
}
