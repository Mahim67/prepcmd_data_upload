<?php

namespace Pondit\Baf\PrePcmData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrePcmCndRange extends Model
{
    use HasFactory;
    protected $table = "prepcm_cnd_ranges";
    protected $guarded = [];

    protected static function column () 
    {
        return $list = [
            'ser_no',
            'part_no',
            'name_of_eqpt',
            'name_of_user',
            'office_id',
            'office_name_from_user',
            'ph_baseunit_cnd_range',
            'qty',
            'unit',
            'currency',
            'estimated_unit_price',
            'estimated_total_cost',
            'justification',
            'range_id',
            'range_name',
            'base_name',

            'recom_qty_by_opi_dte',
            'recom_unit_price_by_opi_dte',
            'recom_total_cost_by_opi_dte',

            'recom_qty_by_prepcm',
            'recom_unit_price_by_prepcm',
            'recom_total_cost_by_prepcm',
            
            'approved_qty_by_pcm',
            'approved_unit_price_by_pcm',
            'approved_total_cost_by_pcm',

            'ph_baseunit_svc',
            'ph_baseunit_svc_fitted',
            'ph_baseunit_svc_spares',
            'ph_baseunit_svc_bd',
            'ph_baseunit_svc_un',
            'ph_baseunit_us',
            'ph_baseunit_rs',
            'ph_baseunit_rd',
            'ph_baseunit_ur',
            'ph_baseunit_sal',
            'ph_baseunit_ttl',
            'life_exp',
            'last_year_consumption',
            'last_5year_consumption',
            'spt_contact_rcvd',
            'last_year_qty_dmd',
            'subrange_name',
            'class_name',
            'eqpt_type_name',
            'trade_name',
            'aircraft_name',
            'main_eqpt_in_system',
            'qty_used_in_sys'
        ];
    }

    protected static function xlFormat () 
    {
        return $list = [
            //excel column   => 'db column'
            'ser_no'                                        => 'ser_no',
            'part_no'                                       => 'part_no',
            'name_of_eqpt'                                  => 'name_of_eqpt',
            'name_of_user_sqnunit'                          => 'name_of_user',
            'base_name'                                     => 'base_name',
            'office_id'                                     => 'office_id',
            'office_name_from_user'                         => 'office_name_from_user',
            'present_holding_in_baseunit'                   => 'ph_baseunit_cnd_range',
            'dmd_qty'                                       => 'qty',
            'unit'                                          => 'unit',
            'currency'                                      => 'currency',
            'estimated_unit_price'                          => 'estimated_unit_price',
            'total_cost'                                    => 'estimated_total_cost',
            'justification'                                 => 'justification',

            'sub_range'                                     => 'subrange_name',
            'class'                                         => 'class_name',
            'eqpt_type'                                     => 'eqpt_type_name',
            'trade'                                         => 'trade',
            'aircraft'                                      => 'aircraft_name',
            'main_equipmentsystem'                          => 'main_eqpt_in_system',
            'qty_used_in_the_system_at_a_time'              => 'qty_used_in_sys',
            "present_holding_base_unit_svc_fitted"          => 'ph_baseunit_svc_fitted',
            "present_holding_base_unit_svc_spares"          => 'ph_baseunit_svc_spares',
            "present_holding_base_unit_svc"                 => 'ph_baseunit_svc',
            "present_holding_base_unit_svcbd"               => 'ph_baseunit_svc_bd',
            "present_holding_base_unit_svcun"               => 'ph_baseunit_svc_un',
            "present_holding_base_unit_us"                  => 'ph_baseunit_us',
            "present_holding_base_unit_rs"                  => 'ph_baseunit_rs',
            "present_holding_base_unit_rd"                  => 'ph_baseunit_rd',
            "present_holding_base_unit_ur"                  => 'ph_baseunit_ur',
            "sal"                                           => 'ph_baseunit_sal',
            "ttl"                                           => 'ph_baseunit_ttl',
            "life_exp_on"                                   => 'life_exp',
            "last_5year_consumption"                        => 'last_5year_consumption',
            "last_year_consumption"                         => 'last_year_consumption',
            "spt_contactyet_to_rcvd"                        => 'spt_contact_rcvd',
            "fy_2020_21"                                    => 'last_year_qty_dmd'
        ];
    }
}
