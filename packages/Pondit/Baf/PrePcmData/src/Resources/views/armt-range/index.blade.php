@extends('pondit-limitless::layouts.master')
@section('content')
    @include('pondit-limitless::elements.success')
    @include('pondit-limitless::elements.error')
    <x-pondit-card title="Armt Range">
        <a href="{{ route('armt-ranges.create') }}" class="btn btn-sm bg-primary">Create</a>
        <div class="table-responsive mt-3">
            <div class="row">
                <div class="col-sm-12  col-md-3  ">
                    <div class="form-group form-inline  float-right">
                        <label for="numberOfRowsPerPage"> {{ __('Item per page') }}</label>&nbsp;
                        <select class="form-control" id="numberOfRowsPerPage">
                            <option {{ $rowsPerPage == '5' ? 'selected' : '' }}>5</option>
                            <option {{ $rowsPerPage == '10' ? 'selected' : '' }}>10</option>
                            <option {{ $rowsPerPage == '20' ? 'selected' : '' }}>20</option>
                            <option {{ $rowsPerPage == '30' ? 'selected' : '' }}>30</option>
                            <option {{ $rowsPerPage == '40' ? 'selected' : '' }}>40</option>
                            <option {{ $rowsPerPage == '50' ? 'selected' : '' }}>50</option>
                            <option {{ $rowsPerPage == '100' ? 'selected' : '' }}>100</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">
                    {{-- {!! Form::text('item_part_no', null, [
                'class' => 'form-control',
                'id' => 'item_part_no',
                'placeholder' => 'Part No',
                'onkeyup' => 'index()',
                ]) !!} --}}
                    <input type="text" class="form-control" onkeyup="index()" placeholder="Name Of Eqpt" id="name_of_eqpt">
                </div>
            </div>

            <table class="table table-bordered">
                <thead class="bg-success text-center">
                    <tr>
                        <th rowspan="2">Ser No</th>
                        <th rowspan="2">Name Of Eqpt</th>
                        <th colspan="4">Demand</th>
                        <th rowspan="2">Recom by <br>OPI Dte</th>
                        <th rowspan="2">Approve by <br> Pre PCM</th>
                        <th rowspan="2">Estimated Unit Price <br> (TK)</th>
                        <th rowspan="2">Total Cost (TK)</th>
                        <th rowspan="2">Total Recom Cost <br> (TK)</th>
                        <th rowspan="2">Justification/Remarks</th>
                    </tr>
                    <tr>
                        <th>Base Name</th>
                        <th>Name Of User <br> (Base/Unit)</th>
                        <th>Present holding <br> in Base/Unit</th>
                        <th>Dmd Qty</th>
                    </tr>
                </thead>
                <tbody id="myTable"></tbody>
                {{-- <tbody>
                @php $sl = 1; @endphp
                @foreach ($data as $item => $datam)
                <tr>
                    <td rowspan="{{ count($datam) }}">{{ $sl++ }}</td>
                    <td rowspan="{{ count($datam) }}">{{ $item }}</td>
                    @foreach ($datam as $item)

                    {!! $loop->first ? '' : "
                </tr>
                <tr>" !!}
                    <td>{{ $item->base_name }}</td>
                    <td>{{ $item->name_of_user }}</td>
                    <td>{{ $item->ph_baseunit_supply_range }}</td>
                    <td class="text-center">
                        <a href="" class="" data-toggle="modal" data-target="#qtyDmdModal" data-target-id="{{ $item->id }}"><b>{{ $item->qty }}</b></a>
                    </td>
                    <td class="text-center">{{ $item->recom_qty_by_opi_dte }}</td>
                    <td class="text-center">{{ $item->recom_qty_by_prepcm }}</td>
                    <td class="text-right">{{ number_format($item->estimated_unit_price, 2) }}</td>
                    <td class="text-right">{{ number_format($item->estimated_total_cost, 2) }}</td>
                    <td class="text-right">{{ number_format($item->recom_total_cost_by_opi_dte, 2) }}</td>
                    <td>
                        <span data-popup="tooltip" title="{{ $item->justification }}" data-placement="top">
                            {{ Illuminate\Support\Str::limit($item->justification, '40', '...') }}
                        </span>
                    </td>
                </tr>
                @endforeach
                @endforeach
            </tbody> --}}
            </table>
            {{-- <nav aria-label="Page navigation" class="text-uppercase" id="ponditPagination">
                <ul class="pagination float-right" id="pagesList"></ul>
                <div>Page: <span class="badge badge-primary text-white current-page-number">0</span> of
                    <span class="badge badge-primary text-white total-page-number">0</span> Records: <span
                        class="badge badge-primary text-white total-record-number">0</span>
                </div>
            </nav> --}}
        </div>

    </x-pondit-card>


    <!-- Qty Dmd Modal Start -->
    <div class="modal fade" id="qtyDmdModal" tabindex="-1" role="dialog" aria-labelledby="qtyDmdModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="qtyDmdModalLabel"></h5>
                    <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="text-success">Budget Code : <b id="budgetLabel"></b></h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Budget Alloted : </h5>
                        </div>
                        <div class="col-sm-3">
                            <h5 class="text-success"><b id="budgetAlloted"></b></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Total Demanded Amount : </h5>
                        </div>
                        <div class="col-sm-3">
                            <h5><b id="estimateTotalCost"></b></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Total Amount Recom by OPI Dte : </h5>
                        </div>
                        <div class="col-sm-3">
                            <h5><b id="opiTotalCost" class="text-danger"></b></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Total Amount Recom at Pre-PCM : </h5>
                        </div>
                        <div class="col-sm-3">
                            <h5><b id="prePcmTotalCost" class="text-danger"></b></h5>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered">
                        <thead class="bg-success text-center">
                            <tr>
                                <th>Name Of User <br> (Base/Unit)</th>
                                <th>Qty Dmd</th>
                                <th>Recom Qty Dmd <br> by OPI Dte</th>
                                <th>Recom Qty Dmd <br> by Pre-Pcm</th>
                            </tr>
                        </thead>
                        <tbody id="qtyDmdModalTbody">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm bg-success save-modal" data-dismiss="modal">Save</button>
                    <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- Qty Dmd Modal End -->

@endsection
@push('js')

    <script>
        $(document).ready(function() {
            $("#qtyDmdModal").on("show.bs.modal", function(e) {
                var id = $(e.relatedTarget).data('target-id');
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: `${window.origin}/api/get-armt-qty-demand/${id}`,
                    success: function(res) {
                        if (res.status == 'ok') {
                            var data = res.data;
                            var htmlList = '';
                            var title;
                            $.each(data, function(key, datam) {
                                htmlList += `
                                    <tr>
                                        <td>${datam.name_of_user}</td>
                                        <td class="text-center">${datam.qty}</td>
                                        <td>
                                            <form id="form">@csrf
                                                <input type="text" class="form-control text-center" value=${datam.recom_qty_by_opi_dte} name="qty" id="qty_dmd_${datam.id}" onchange="qtyDmdOpi(${datam.id})" />
                                            </form>
                                        </td>
                                        <td>
                                            <form id="form">@csrf
                                                <input type="text" class="form-control text-center" value=${datam.recom_qty_by_prepcm} name="qty_dmd_prepcm_" id="qty_dmd_prepcm_${datam.id}" onchange="qtyDmdPrePcm(${datam.id})" />
                                            </form>
                                        </td>
                                    </tr>   
                                `;
                                title = `<b>Base/Unit Name : ${datam.base_name}<br>
                                        Name Of Eqpt : ${datam.name_of_eqpt}</b>
                                        `;
                            });
                            let banglaBDT = Intl.NumberFormat("en-US", {
                                style: "currency",
                                currency: "BDT",
                                useGrouping: true,
                            });
                            console.log(res)
                            $("#qtyDmdModalLabel").html(title);
                            $("#qtyDmdModalTbody").html(htmlList);
                            $("#estimateTotalCost").text(banglaBDT.format(res
                                .estimateTotalCost));
                            $("#opiTotalCost").text(banglaBDT.format(res.opiTotalCost));
                            $("#prePcmTotalCost").text(banglaBDT.format(res.prePcmTotalCost));
                            $("#budgetLabel").text("(" + res.budgetcode.oldcode + ") " + res
                                .budgetcode.newcode + " (" + res.budgetcode.description +
                                ")");
                            $("#budgetAlloted").text("BDT  3.130 Crore");

                        }
                    },
                    error: function() {
                        console.log(res);
                    }
                });
            });

            $(".save-modal").click(function() {
                location.reload(true);
            });
        });

        function qtyDmdOpi(id) {
            let _token = $("input[name=_token]").val();
            var qty = $("#qty_dmd_" + id).val();

            $.ajax({
                url: `${window.origin}/api/armt-store-qty-demand`,
                type: "POST",
                data: {
                    id: id,
                    qty: qty,
                    _token: _token
                },
                success: function(res) {
                    if (res.status == "success") {
                        let banglaBDT = Intl.NumberFormat("en-US", {
                            style: "currency",
                            currency: "BDT",
                            useGrouping: true,
                        });
                        $('#opiTotalCost').text(banglaBDT.format(res.total_amount.opiTotalCost));
                    }
                },
            });
        }

        function qtyDmdPrePcm(id) {
            let _token = $("input[name=_token]").val();
            var qty = $("#qty_dmd_prepcm_" + id).val();

            $.ajax({
                url: `${window.origin}/api/armt-store-qty-demand`,
                type: "POST",
                data: {
                    id: id,
                    qty_prepcm: qty,
                    _token: _token
                },
                success: function(res) {
                    if (res.status == "success") {
                        let banglaBDT = Intl.NumberFormat("en-US", {
                            style: "currency",
                            currency: "BDT",
                            useGrouping: true,
                        });
                        $('#prePcmTotalCost').text(banglaBDT.format(res.total_amount.prePcmTotalCost));
                    }
                },
            });
        }

    </script>
    
    <script src="{{ asset('js') }}/axios.min.js"></script>
    <script src="{{ asset('js') }}/paginator.js"></script>
    <script>
        const filterableColumns = ['name_of_eqpt'];

        /*Clear filter options*/
        function clearAllFilters() {
            filterableColumns.forEach(function(column) {
                document.querySelector('#' + column).value = '';
            });
            index()
        }

        window.onload = function() {

            /*Number of rows onchange*/
            let numberOfRowsPerPage = document.querySelector('#numberOfRowsPerPage');
            numberOfRowsPerPage.addEventListener('change', function() {
                index();
            });

            /*Filterable columns onchange*/
            filterableColumns.forEach(function(column) {
                document.getElementById(column).addEventListener('change', function() {
                    index();
                })
            })
            index();
        }

        function index(page_url = null) {

            let numberOfRowsPerPageInput = document.querySelector('#numberOfRowsPerPage');
            let rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value;
            // let keywordInput = document.getElementById('keyword');
            // let searchKeyWord = keywordInput.value;
            let queryString = '';
            // queryString += searchKeyWord != ''? 'keyword='+searchKeyWord : '';
            queryString += '&rows_per_page=' + rowsPerPage;


            let filterColumns = '';
            filterableColumns.forEach(function(column) {
                let val = document.querySelector('#' + column).value;
                filterColumns += column + '|' + val + ';';
            });
            queryString += filterColumns.length > 0 ? '&filterable_columns=' + filterColumns : '';
            page_url = page_url || `/get-armt-data?${queryString}`;
            axios.get(page_url)
                .then(res => {

                    let numberOfRowsPerPageInput = document.querySelector('#numberOfRowsPerPage');
                    let rowsPerPage = numberOfRowsPerPageInput.options[numberOfRowsPerPageInput.selectedIndex].value
                    let parentElement = document.querySelector('#myTable');
                    parentElement.innerHTML = '';
                    let items = res.data.data;
                    let sl = 1;

                    let pageData = Object.values(items);

                    if (items.length == 0) {
                        parentElement.innerHTML = `<tr>
                                                                <td colspan="12" class="text-center"> No Data Found </td>
                                                            </tr>`;
                    }
                    
                    pageData.forEach(item => {
                        createRowElement(item, parentElement, sl++)
                    });



                    // // /*Required arguments for pagination*/
                    let searchKeyWord = '';
                    return {
                        parent_element: parentElement,
                        url: `/get-armt-data`,
                        current_page: items.current_page,
                        last_page: items.last_page,
                        total_rows: items.total,
                        keyword: searchKeyWord,
                        row_per_page: rowsPerPage,
                        // pages: res.data.pages,
                        query_string: queryString
                    };
                })
                .then(data => {
                    makePagination(data);
                })
                .catch(err => console.log(err))
        }


        function createRowElement(item, parentElement, sl) {
            let tbody = document.getElementById('myTable');
            const firstRow = document.createElement('tr');
            firstRow.innerHTML = ` 
                                <td rowspan="${item.length + 1}"><b>${sl}</b></td>
                                <td rowspan="${item.length + 1}"><b>${item[0].name_of_eqpt}</b></td>
                                `;
            tbody.appendChild(firstRow);


            const TR = document.createElement('tr');
            item.forEach(data => {
                const TR = document.createElement('tr');

                const baseName = document.createElement('td');
                baseName.innerHTML = data.base_name;
                TR.appendChild(baseName);

                const nameOfUser = document.createElement('td');
                nameOfUser.innerHTML = data.name_of_user;
                TR.appendChild(nameOfUser);

                const phBaseunitArmtRange = document.createElement('td');
                phBaseunitArmtRange.innerHTML = data.ph_baseunit_armt_range;
                TR.appendChild(phBaseunitArmtRange);

                const Qty = document.createElement('td');
                Qty.className = 'text-center';
                Qty.innerHTML = `
                                <a href="" class="" data-toggle="modal" data-target="#qtyDmdModal" data-target-id="${data.id}"><b>${ data.qty }</b></a>
                                `
                TR.appendChild(Qty);

                const recomQtyByOpiDte = document.createElement('td');
                recomQtyByOpiDte.className = 'text-center';
                recomQtyByOpiDte.innerHTML = data.recom_qty_by_opi_dte;
                TR.appendChild(recomQtyByOpiDte);

                const recomQtyByPrepcm = document.createElement('td');
                recomQtyByPrepcm.className = 'text-center';
                recomQtyByPrepcm.innerHTML = data.recom_qty_by_prepcm;
                TR.appendChild(recomQtyByPrepcm);

                const estimatedUnitPrice = document.createElement('td');
                estimatedUnitPrice.className = 'text-center';
                estimatedUnitPrice.innerHTML = data.estimated_unit_price;
                TR.appendChild(estimatedUnitPrice);

                const estimatedTotalCost = document.createElement('td');
                estimatedTotalCost.className = 'text-center';
                estimatedTotalCost.innerHTML = data.estimated_total_cost;
                TR.appendChild(estimatedTotalCost);

                const recomTotalCostByOpiDte = document.createElement('td');
                recomTotalCostByOpiDte.className = 'text-center';
                recomTotalCostByOpiDte.innerHTML = data.recom_total_cost_by_opi_dte;
                TR.appendChild(recomTotalCostByOpiDte);

                const Justification = document.createElement('td');
                if (data.justification) {
                    if (data.justification.length < 50) {
                        Justification.innerHTML = `
                <span data-popup="tooltip" title="${data.justification}" data-placement="top">
                    ${data.justification}
                </span>
                                `;
                    } else {
                        let justificationText = data.justification.substr(0, 50) + "...";
                        Justification.innerHTML = `
                                        <span data-popup="tooltip" title="${data.justification}" data-placement="top">
                                            ${justificationText}
                                        </span>
                                `;
                    }
                }
                TR.appendChild(Justification);

                parentElement.appendChild(TR);
            });


        }

    </script>

@endpush
