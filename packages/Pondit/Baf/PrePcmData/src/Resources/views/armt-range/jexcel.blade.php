@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="JExcel">

    <div id="spreadsheet" class="table-responsive"></div>
    <!-- <ol class='example'>
        <a onclick="myTable.getCell()">click</a>
    </ol> -->
</x-pondit-card>
@endsection

@push('js')
<script src="https://bossanova.uk/jspreadsheet/v4/jexcel.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jspreadsheet/v4/jexcel.css" type="text/css" />
<script src="https://jsuites.net/v4/jsuites.js"></script>
<link rel="stylesheet" href="https://jsuites.net/v4/jsuites.css" type="text/css" />

<script>
    var data = @json($data);
    
    var myTable = $('#spreadsheet').jexcel({
        csvHeaders: true,
        search: true,
        pagination: 10,
        filters: true,
        data: data,
        columns: [{
                type: 'text',
                title: 'Part No',
                width: 100
            },
            {
                type: 'text',
                title: 'Name of Eqpt',
                width: 150
            },
            {
                type: 'text',
                title: 'Base Name',
                width: 150
            },
            {
                type: 'text',
                title: 'Name of User',
                width: 250
            },
            {
                type: 'text',
                title: 'Justification',
                width: 500
            },
        ],

    });

</script>
@endpush