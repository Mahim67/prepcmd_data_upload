@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="XL Upload">
    <div class="col-sm-6">
        <h5><a href="{{ route('armt-ranges.create') }}">Upload XL - Armt Range</a></h5>
        <h5><a href="{{ route('supply-ranges.create') }}">Upload XL - Supply Range</a></h5>
        <h5><a href="{{ route('trg-ranges.create') }}">Upload XL - Trg Range</a></h5>
        <h5><a href="{{ route('med-ranges.create') }}">Upload XL - Med Range</a></h5>
        <h5><a href="{{ route('met-ranges.create') }}">Upload XL - Met Range</a></h5>
        <h5><a href="{{ route('cnd-ranges.create') }}">Upload XL - C&E Range</a></h5>
        <h5><a href="#" onclick="return confirm('Under Developing....');">Upload XL - Engg Range</a></h5>

    </div>
</x-pondit-card>
@endsection