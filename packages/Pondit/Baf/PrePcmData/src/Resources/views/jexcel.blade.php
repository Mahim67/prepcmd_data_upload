@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="JExcel">

    <div id="spreadsheet" class="table-responsive"></div>




</x-pondit-card>
@endsection

@push('js')

    <script src="https://jspreadsheet.com/v7/jspreadsheet.js"></script>
    <script src="https://jsuites.net/v4/jsuites.js"></script>
    <link rel="stylesheet" href="https://jspreadsheet.com/v7/jspreadsheet.css" type="text/css" />
    <link rel="stylesheet" href="https://jsuites.net/v4/jsuites.css" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    <script>






        var changed = function() {
            alert(this)
        }


        var data = @json($data);
        let table =  jspreadsheet(document.getElementById('spreadsheet'), {
            csvHeaders:true,
            search:true,
            pagination:10,
            filters: true,
            data: data,

            onchange: changed,

            columns: [

                {type: 'text', title: 'id', width: 100},
                {type: 'text', title: 'Part No', width: 100},
                {type: 'text', title: 'Name_of Eqpt', width: 150},
                {type: 'text', title: 'Base Name', width: 150},
                {type: 'text', title: 'Name of User', width: 250},
                {type: 'text', title: 'Justification', width: 500},
            ],



        })

    </script>
@endpush