@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Supply Range">
    <a href="{{ route('supply-ranges.create') }}" class="btn btn-sm bg-primary">Create</a>

    <div class="form-group mt-2">
        <label for="filter">Filter : </label>
        <div class="col-sm-5 d-flex">
            <input type="text" class="form-control w-50 mr-2" name="filter" onkeyup="itemFilter();" id="filter" placeholder="Name of Eqpt/Base Name">
            <button type="button" class="btn btn-sm bg-danger reset">Reset</button>
        </div>
    </div>
    <div class="table-responsive mt-3">
        <table class="table table-bordered">
            <thead class="bg-success text-center">
                <tr>
                    <th rowspan="2">Ser No</th>
                    <th rowspan="2">Name Of Eqpt</th>
                    <th colspan="4">Demand</th>
                    <th rowspan="2">Recom by <br>OPI Dte</th>
                    <th rowspan="2">Approve by <br> Pre PCM</th>
                    <th rowspan="2">Estimated Unit Price <br> (TK)</th>
                    <th rowspan="2">Total Cost (TK)</th>
                    <th rowspan="2">Total Recom Cost <br> (TK)</th>
                    <th rowspan="2">Justification/Remarks</th>
                </tr>
                <tr>
                    <th>Base Name</th>
                    <th>Name Of User <br> (Base/Unit)</th>
                    <th>Present holding <br> in Base/Unit</th>
                    <th>Dmd Qty</th>
                </tr>
            </thead>
            <tbody id="tbody">
                @php $sl = 1; @endphp
                @foreach ($data as $item => $datam)
                <tr>
                    <td rowspan="{{ count($datam) }}">{{ $sl++ }}</td>
                    <td rowspan="{{ count($datam) }}">{{ $item }}</td>
                    @foreach ($datam as $item)

                    {!! $loop->first ? '' : "
                </tr>
                <tr>" !!}
                    <td>{{ $item->base_name }}</td>
                    <td>{{ $item->name_of_user }}</td>
                    <td>{{ $item->ph_baseunit_supply_range }}</td>
                    <td class="text-center">
                        <a href="" class="" data-toggle="modal" data-target="#qtyDmdModal" data-target-id="{{ $item->id }}"><b>{{ $item->qty }}</b></a>
                    </td>
                    <td class="text-center">{{ $item->recom_qty_by_opi_dte }}</td>
                    <td class="text-center">{{ $item->recom_qty_by_prepcm }}</td>
                    <td class="text-right">{{ number_format($item->estimated_unit_price, 2) }}</td>
                    <td class="text-right">{{ number_format($item->estimated_total_cost, 2) }}</td>
                    <td class="text-right">{{ number_format($item->recom_total_cost_by_opi_dte, 2) }}</td>
                    <td>
                        <span data-popup="tooltip" title="{{ $item->justification }}" data-placement="top">
                            {{ Illuminate\Support\Str::limit($item->justification, '40', '...') }}
                        </span>
                    </td>
                </tr>
                @endforeach
                @endforeach
            </tbody>
        </table>
    </div>

</x-pondit-card>


<!-- Qty Dmd Modal Start -->
<div class="modal fade" id="qtyDmdModal" tabindex="-1" role="dialog" aria-labelledby="qtyDmdModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="qtyDmdModalLabel"></h5>
                <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-success">Budget Code : <b id="budgetLabel"></b></h5>
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Budget Alloted : </h5>
                    </div>
                    <div class="col-sm-3">
                        <h5 class="text-success"><b id="budgetAlloted"></b></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Total Demanded Amount : </h5>
                    </div>
                    <div class="col-sm-3">
                        <h5><b id="estimateTotalCost"></b></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Total Amount Recom by OPI Dte : </h5>
                    </div>
                    <div class="col-sm-3">
                        <h5><b id="opiTotalCost"></b></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h5>Total Amount Recom at Pre-PCM : </h5>
                    </div>
                    <div class="col-sm-3">
                        <h5><b id="prePcmTotalCost" class="text-danger"></b></h5>
                    </div>
                </div>
                <table class="table table-hover table-bordered">
                    <thead class="bg-success text-center">
                        <tr>
                            <th>Name Of User <br> (Base/Unit)</th>
                            <th>Qty Dmd</th>
                            <th>Recom Qty Dmd <br> by OPI Dte</th>
                            <th>Recom Qty Dmd <br> by Pre-Pcm</th>
                        </tr>
                    </thead>
                    <tbody id="qtyDmdModalTbody">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm bg-success reset" data-dismiss="modal">Save</button>
                <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<!-- Qty Dmd Modal End -->

@endsection
@push('js')
<script src="{{ asset('') }}vendor/assets/plugins/select2/js/select2.min.js"></script>
<script>
    var allotmentBudget;
    var crore = 10000000;
    $(document).ready(function() {
        $("#qtyDmdModal").on("show.bs.modal", function(e) {
            var id = $(e.relatedTarget).data('target-id');
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: `${window.origin}/api/get-qty-demand/${id}`,
                success: function(res) {
                    if (res.status == 'ok') {
                        var data = res.data;
                        var htmlList = '';
                        var title;
                        $.each(data, function(key, datam) {
                            htmlList += `
                                <tr>
                                    <td>${datam.name_of_user}</td>
                                    <td class="text-center">${datam.qty}</td>
                                    <td>
                                        <form id="form">@csrf
                                            <input type="text" class="form-control text-center" value=${datam.recom_qty_by_opi_dte} name="qty" id="qty_dmd_${datam.id}" onchange="qtyDmdOpi(${datam.id})" />
                                        </form>
                                    </td>
                                    <td>
                                        <form id="form">@csrf
                                            <input type="text" class="form-control text-center" value=${datam.recom_qty_by_prepcm} name="qty_dmd_prepcm_" id="qty_dmd_prepcm_${datam.id}" onchange="qtyDmdPrePcm(${datam.id})" />
                                        </form>
                                    </td>
                                </tr>   
                            `;
                            title = `<b>Base/Unit Name : ${datam.base_name}<br>
                                    Name Of Eqpt : ${datam.name_of_eqpt}</b>
                                    `;
                        });
                        let banglaBDT = Intl.NumberFormat("en-US", {
                            style: "currency",
                            currency: "BDT",
                            useGrouping: true,
                        });

                        $("#qtyDmdModalLabel").html(title);
                        $("#qtyDmdModalTbody").html(htmlList);
                        $("#estimateTotalCost").text(res.estimateTotalCost / crore + " Cr");
                        allotmentBudget = res.budgetAlloted / crore;
                        let amount = (res.opiTotalCost / crore);
                        if (allotmentBudget >= amount) {
                            $("#opiTotalCost").text(amount + " Cr");
                            $("#opiTotalCost").addClass("text-success");
                        } else {
                            $("#opiTotalCost").text("- " + amount + " Cr");
                            $("#opiTotalCost").addClass("text-danger");
                        }
                        $("#prePcmTotalCost").text(res.prePcmTotalCost / crore + " Cr");
                        $("#budgetLabel").text("(" + res.budgetcode.oldcode + ") " + res.budgetcode.newcode + " (" + res.budgetcode.description + ")");
                        $("#budgetAlloted").text(allotmentBudget + " Cr");
                        qtyDmdOpi();
                    }
                },
                error: function() {
                    console.log(res);
                }
            });
        });

        $(".reset").click(function() {
            location.reload(true);
        });
    });

    function qtyDmdOpi(id) {
        let _token = $("input[name=_token]").val();
        var qty = $("#qty_dmd_" + id).val();
        if (id != null) {
            $.ajax({
                url: `${window.origin}/api/store-qty-demand`,
                type: "POST",
                data: {
                    id: id,
                    qty: qty,
                    _token: _token
                },
                success: function(res) {
                    if (res.status == "success") {
                        let banglaBDT = Intl.NumberFormat("en-US", {
                            style: "currency",
                            currency: "BDT",
                            useGrouping: true,
                        });
                        let amount = (res.total_amount.opiTotalCost / crore);
                        if (allotmentBudget >= amount) {
                            $("#opiTotalCost").text(amount + " Cr");
                            $("#opiTotalCost").addClass("text-success");
                        } else {
                            $("#opiTotalCost").text("- " + amount + " Cr");
                            $("#opiTotalCost").removeClass("text-success");
                            $("#opiTotalCost").addClass("text-danger");
                        }
                    }
                },
            });
        }
    }

    function qtyDmdPrePcm(id) {
        let _token = $("input[name=_token]").val();
        var qty = $("#qty_dmd_prepcm_" + id).val();

        $.ajax({
            url: `${window.origin}/api/store-qty-demand`,
            type: "POST",
            data: {
                id: id,
                qty_prepcm: qty,
                _token: _token
            },
            success: function(res) {
                if (res.status == "success") {
                    let banglaBDT = Intl.NumberFormat("en-US", {
                        style: "currency",
                        currency: "BDT",
                        useGrouping: true,
                    });
                    $('#prePcmTotalCost').text(banglaBDT.format(res.total_amount.prePcmTotalCost));
                }
            },
        });
    }

    function itemFilter() {
        let params = $("#filter").val();
        if (params.length >= 3) {
            $.ajax({
                url: `${window.origin}/api/item-filter/${params}`,
                type: "GET",
                success: function(res) {
                    let sl = 1;
                    var html = '';
                    $.each(res.data, function(key, value) {
                        html += `
                            <tr>
                                <td>${sl++}</td>
                                <td>${value.name_of_eqpt}</td>
                                <td>${value.base_name}</td>
                                <td>${value.name_of_user}</td>
                                <td>${value.ph_baseunit_supply_range ?? ''}</td>
                                <td class="text-center">
                                    <a href="" data-toggle="modal" data-target="#qtyDmdModal" data-target-id="${value.id}"><b>${value.qty}</b></a>
                                </td>  
                                <td class="text-center">${value.recom_qty_by_opi_dte}</td>  
                                <td class="text-center">${value.recom_qty_by_prepcm}</td>  
                                <td class="text-right">${value.estimated_unit_price}</td>  
                                <td class="text-right">${value.estimated_total_cost}</td>  
                                <td class="text-right">${value.recom_total_cost_by_opi_dte}</td>
                                <td>${value.justification}</td>
                            </tr>
                        `;

                    });
                    $("#tbody").html(html);

                },
                error: function() {
                    console.log(res);
                }

            });
        }

    }
</script>

@endpush