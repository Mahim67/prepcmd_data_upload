@extends('pondit-limitless::layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/ui.jqgrid-bootstrap4.css">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/fontawesome/css/fontawesome-all.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="{{ asset('vendor/pondit/assets/jqgrid/css/datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/css/main.css">
<!-- <link rel="stylesheet" href="{{ asset('') }}vendor/pondit/assets/jqgrid/main.css"> -->

@endpush
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Budgets">
<a href="{{ route('supply-ranges.create') }}" class="btn btn-sm bg-primary">Upload</a>


    <div class="row" style="position: relative;">
        <div class="col-12 mt-1 main-col">
            <div class="p-2">
                <div class="row">
                    <p class="no-print">
                        <button id="export_as_pdf" type="button" class="btn btn-sm btn-danger">Export as PDF</button>
                    </p>
                    <table id="jqGrid" style="position: relative;"></table>
                    <div id="jqGridPager"></div>
                </div>
            </div>
        </div>
    </div>

    
    <span class="oi oi-person"></span>
    <div id="userForm" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-primary">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <form id="form">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="fin_year">Fin Year</label>
                            <select name="fin_year" id="fin_year" placeholder="Select Fin Year"
                                class="form-control select">
                                <option value="2019-2020">2019-2020</option>
                                <option value="2020-2021">2020-2021</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="total_amount">Total Amount</label>
                            <input name="total_amount" type="number" placeholder="Total Amount"
                                id="total_amount" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <button type="button" id="form-reset" class="btn btn-info btn-sm float-left"><i
                                    class="fa fa-sync"></i> CLEAR</button>
                            <button type="button" id="form-submit" class="btn btn-success btn-sm float-right"><i
                                    class="fa fa-save"></i> SAVE</button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="userDetails" class="modal fade show" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h5 class="modal-title text-white">Budget Codes</h5>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <table class="table table-striped table-bordered" id="user-details-table"></table>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>



    <div id="cell_modal" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h3 class="modal-title text-white">My Modal</h3>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Website</th>
                            </tr>
                        </thead>
                        <tbody id="cell_content"></tbody>
                    </table>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


    <div id="confirmation-modal" class="modal fade" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header bg-success">
                    <h3 class="modal-title text-white">Are you sure to save this row data ?</h3>
                    <span aria-hidden="true" data-dismiss="modal"><i class="fa fa-times text-light"
                            style="cursor: pointer;"></i></span>
                </div>

                <div class="modal-body">
                    <div class="float-right">
                        <button class="btn bg-info confirm">Confirm</button>
                        <button class="btn btn-danger deny">Deny</button>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


</x-pondit-card>
<input type="hidden" value="{{ csrf_token() }}" name="_token" id="csrf_token">
@endsection
@push('js')

<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/i18n/grid.locale-en.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/context-menu.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/trirand/jquery.jqGrid.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="{{ asset('vendor/pondit/assets/jqgrid/js/datetimepicker.min.js') }}"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="{{ asset('vendor/pondit/assets/jqgrid/js/trirand/jszip.min.js') }}"></script>
<script src="{{ asset('vendor/pondit/assets/jqgrid/js/trirand/pdf_maker.js') }}"></script>
<script src="{{ asset('vendor/pondit/assets/jqgrid/js/trirand/vfs_fonts.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>
<script src="https://unpkg.com/jspdf-autotable@3.5.14/dist/jspdf.plugin.autotable.js"></script>
<script src="{{ asset('') }}vendor/pondit/assets/jqgrid/js/budget1.js"></script>

@endpush
