@extends('pondit-limitless::layouts.master')
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<x-pondit-card title="Trg Range XL Upload">
    <form action="{{ route('prepcm-xl-upload.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" name="range" value="Trg Range" class="form-control" readonly>
            </div>
            <div class="form-group">
                <label>Excel File</label>
                <input type="file" class="form-control" name="excel_file_path">
            </div>
            <button type="submit" class="btn btn-sm bg-success">Upload</button>
        </div>
    </form>
    @if (isset($data) && count($data) > 0)
        <div class="text-center mt-3">
            <h5>Total Uploaded Trg Range : <b> {{ count($data) }} </b></h5>
        </div>
    @endif
</x-pondit-card>
@endsection