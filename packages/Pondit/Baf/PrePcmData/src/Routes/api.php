<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmArmtRangeController;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmSupplyRangeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/api/prepcm-supply-ranges', [PrePcmSupplyRangeController::class, 'retrieveData']);
Route::get('/api/get-qty-demand/{id}', [PrePcmSupplyRangeController::class, 'getQtyDmdInfo']);
Route::post('/api/store-qty-demand', [PrePcmSupplyRangeController::class, 'storeQtyDmd']);


Route::get('/api/get-armt-qty-demand/{id}', [PrePcmArmtRangeController::class, 'getArmtQtyDmdInfo']);
Route::post('/api/armt-store-qty-demand', [PrePcmArmtRangeController::class, 'armtStoreQtyDmd']);
Route::get('/api/item-filter/{id}', [PrePcmSupplyRangeController::class, 'itemFilter']);
