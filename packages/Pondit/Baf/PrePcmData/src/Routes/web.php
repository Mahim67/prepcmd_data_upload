<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmCndRangeController;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmXlUploadController;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmArmtRangeController;
use Pondit\Baf\PrePcmData\Http\Controllers\PrePcmSupplyRangeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [PrePcmXlUploadController::class, 'create']);





// Route::get('/armt-ranges', [PrePcmXlUploadController::class, 'createArmtRange'])->name('armt-ranges.create');
Route::get('/trg-ranges', [PrePcmXlUploadController::class, 'createTrgRange'])->name('trg-ranges.create');
Route::get('/med-ranges', [PrePcmXlUploadController::class, 'createMedRange'])->name('med-ranges.create');
Route::get('/met-ranges', [PrePcmXlUploadController::class, 'createMetRange'])->name('met-ranges.create');


Route::post('/store', [PrePcmXlUploadController::class, 'store'])->name('prepcm-xl-upload.store');

/**
 * Supply Range
 */
Route::get('/supply-ranges/create', [PrePcmSupplyRangeController::class, 'createSupplyRange'])->name('supply-ranges.create');
Route::get('/jexcel', [PrePcmSupplyRangeController::class, 'jexcel'])->name('supply-ranges.jexcel');
Route::get('/retreive-supply-ranges', [PrePcmSupplyRangeController::class, 'jqgrid'])->name('supply-ranges.jqgrid');
Route::get('/supply-range', [PrePcmSupplyRangeController::class, 'index'])->name('supply-ranges.index');

// Route::get('/get-supply-data', [PrePcmSupplyRangeController::class, 'getSupplyData']);
Route::get('/update-supply-data-item-id', [PrePcmSupplyRangeController::class, 'updateSupplyData']);
Route::get('/update-supply-data-budgetcode-id', [PrePcmSupplyRangeController::class, 'updateBudgetCode']);

/**
 * Armt Range
 */
Route::get('/armt-ranges/create', [PrePcmArmtRangeController::class, 'createArmtRange'])->name('armt-ranges.create');
Route::get('/armt-jexcel', [PrePcmArmtRangeController::class, 'jexcel'])->name('armt-ranges.jexcel');
Route::get('/retreive-armt-ranges', [PrePcmArmtRangeController::class, 'jqgrid'])->name('armt-ranges.jqgrid');
Route::get('/armt-range', [PrePcmArmtRangeController::class, 'index'])->name('armt-ranges.index');

Route::get('/get-armt-data', [PrePcmArmtRangeController::class, 'getArmtData']);

// Route::post('/store-qty-demand', [PrePcmSupplyRangeController::class, 'storeQtyDmd'])->name('supply-range.qtyStore');

/**
 * Armt Range
 */
Route::get('/cnd-ranges/create', [PrePcmCndRangeController::class, 'createCndRange'])->name('cnd-ranges.create');



Route::get('/sync', function () {
    return view('dashboard');
});
