<?php 
namespace Pondit\GeneralizeXlUpload;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Pondit\PonditComponent\Components\Baf\AirCrftComponent;
use Pondit\PonditComponent\Components\Baf\EqptTypeComponent;

class GeneralizeXlUploadServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
    }

    public function register()
    {
        // $this->loadViewsFrom(__DIR__.'/Resources/views', 'generalizeXlUpload');
    }

}