<?php

namespace Pondit\GeneralizeXlUpload\Imports;

use Exception;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class GeneralizedExcelImport implements ToModel, WithHeadingRow, WithValidation {
    use Importable;
    public $columns, $model, $validationInfo;
    public $whiteListedExcel;
    public $hiddenColumn;

    public function __construct($model, array $columns, array $validationInfo =[], array $whiteListedExcel = [], array $hiddenColumn = []) {
        $this->model = $model;
        $this->whiteListedExcel = $whiteListedExcel;
        $this->columns = $columns;
        $this->validationInfo = $validationInfo;
        $this->hiddenColumn = $hiddenColumn;
    }
    public function model(array $row) {
        // dd($row);
        $parseRow = [];
        $newRows = [];
        if (is_array($row)) {
            if (isset($this->whiteListedExcel) && count($this->whiteListedExcel)>0) {
                foreach ($row as $key => $value) {
                    if (isset($this->whiteListedExcel[$key])) {
                        $parseRow[$this->whiteListedExcel[$key]] = $value;
                    }
                }

                if (isset($row['estimated_unit_price'])) {
                    $totalPrice = (float) $row['estimated_unit_price'] * (float) $row['dmd_qty'];

                    $parseRow['estimated_total_cost']   =  $totalPrice ?? $row['total_cost'];

                    $parseRow['recom_qty_by_opi_dte']          = $row['dmd_qty'];
                    $parseRow['recom_unit_price_by_opi_dte']   = $row['estimated_unit_price'];
                    $parseRow['recom_total_cost_by_opi_dte']   = $totalPrice ?? $row['total_cost'];

                    $parseRow['recom_qty_by_prepcm']          = $row['dmd_qty'];
                    $parseRow['recom_unit_price_by_prepcm']   = $row['estimated_unit_price'];
                    $parseRow['recom_total_cost_by_prepcm']   = $totalPrice ?? $row['total_cost'];

                    $parseRow['approved_qty_by_pcm']          = $row['dmd_qty'];
                    $parseRow['approved_unit_price_by_pcm']   = $row['estimated_unit_price'];
                    $parseRow['approved_total_cost_by_pcm']   = $totalPrice ?? $row['total_cost'];



                    if ($parseRow['estimated_total_cost'] > 99999999999.99) {
                        throw new Exception('The ' . $this->count . " -Total Price Amount Length Must be Less Than 99999999999.99", 1);
                    }
                } else {
                    $parseRow['estimated_unit_cost']  = 0;
                    $parseRow['estimated_total_cost'] = 0;
                }

                $newRows = array_merge($parseRow, $row);
            }
            if (count($newRows) > 0) {
                if (isset($this->hiddenColumn) && count($this->hiddenColumn)>0) {
                    $newRows = array_merge($newRows, $this->hiddenColumn);
                }
                $this->storeData($this->model, $this->columns, $newRows);
            } else {
                if (isset($this->hiddenColumn)) {
                    $row = array_merge($row, $this->hiddenColumn);
                }
                $this->storeData($this->model, $this->columns, $row);
            }
        }
    }
    public function rules(): array
    {
        return $this->validationInfo;
    }

    public function storeData($model, $columns, $arrayOfRow) {
        if (is_array($columns)) {
            $matchCol = array_intersect($columns, array_keys($arrayOfRow));

            $dataArray = [];
            foreach ($matchCol as $eachColumn) {
                array_push($dataArray, [
                    $eachColumn => isset($arrayOfRow[$eachColumn]) ? $arrayOfRow[$eachColumn] : null,
                ]);
            }
            
            $makeSingleArray = (collect($dataArray)->collapse())->toArray();
            $data = $this->prepareData($makeSingleArray);
            $model::create($data);
        }
    }

    /**
     * For Cleaning value
     */
    public function prepareData ($param)
    {
        $setOfArray = [];
        foreach ($param as $key => $value) {
            if ($value != null) {
                $data = $this->cleanData($value);
                array_push($setOfArray , [
                    $key => isset($data) ? $data : null,
                ]);
            }
        }
        $setOfSingleArray = (collect($setOfArray)->collapse())->toArray();
        return $setOfSingleArray;
    }
    protected function cleanData ($value)
    {
        $cleanData = trim($value);
        return $cleanData;
    }

}
