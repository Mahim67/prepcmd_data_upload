<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ATag extends Component
{
    public $url
           ,$id
           ,$color
           ,$btnType
           ,$btnSize
           ,$icon
           ,$iconSize
           ,$tooltip
           ,$title
           ,$class;

    public function __construct(
        $url = '#',
        $icon = 'pen',
        $tooltip = " __('edit') ", 
        $color = "primary"
        )
    {
        $this->url = $url;
        $this->icon = $icon;
        $this->tooltip = $tooltip;
        $this->color = $color;
    }
    
    public function render()
    {
        return view('widgets::baf.a_tag');
    }
}
