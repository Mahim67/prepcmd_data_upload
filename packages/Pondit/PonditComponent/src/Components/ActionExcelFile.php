<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionExcelFile extends Component
{
    public $url
           ,$id
           ,$class
           ,$icon
           ,$color
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = 'file-excel',
        $tooltip = "widgets::lang.excel download",
        $class = false, $id = false,
        $title = false, $color = 'primary'
    )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->color = $color;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::baf.actions_excel_file');
    }
}
