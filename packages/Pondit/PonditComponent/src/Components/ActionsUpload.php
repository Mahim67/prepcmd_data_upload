<?php

namespace Pondit\PonditComponent\Components;

use Illuminate\View\Component;

class ActionsUpload extends Component
{
    public $url
           ,$id
           ,$class
           ,$icon
           ,$tooltip
           ,$title;

    public function __construct(
        $url = '#', $icon = 'upload',
        $tooltip = "widgets::lang.upload",
        $class = false, $id = false,
        $title = false
    )
    {
        $this->id = $id;
        $this->url = $url;
        $this->icon = $icon;
        $this->class = $class;
        $this->title = $title;
        $this->tooltip = $tooltip;
    }
    
    public function render()
    {
        return view('widgets::baf.actions_upload');
    }
}
