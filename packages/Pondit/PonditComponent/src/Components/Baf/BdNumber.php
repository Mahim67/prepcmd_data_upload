<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class BdNumber extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$placeholder
           ,$otherAttr;

    public function __construct(
        $id           =  false,
        $placeholder  =  false,
        $label        =  "widgets::lang.Service Number",
        $name         =  false,
        $class        =  false,
        $otherAttr    =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->otherAttr    = $otherAttr;
        $this->placeholder  = $placeholder;
    }
    
    public function render()
    {
        return view('widgets::baf.bdnumber');
    }
}
