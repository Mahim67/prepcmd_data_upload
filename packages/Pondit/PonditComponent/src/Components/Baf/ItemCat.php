<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class ItemCat extends Component
{
    public  $class
           ,$label
           ,$name
           ,$id
           ,$selected
           ,$otherAttr;

    public $catLists = ['0'=>'TV', '1'=>'Freeze', '3'=>'Vehicle'];

    public function __construct(
        $id         =  false,
        $label      =  "widgets::lang.itemCat",
        $name       =  false,
        $class      =  false,
        $selected   =  false,
        $otherAttr  =  false
    )
    {
        $this->id           = $id;
        $this->class        = $class;
        $this->label        = $label;
        $this->name         = $name;
        $this->selected     = $selected;
        $this->otherAttr    = $otherAttr;
    }
    
    public function render()
    {
        $lists = $this->catLists;
        return view('widgets::baf.itemcat', compact('lists'));
    }
}
