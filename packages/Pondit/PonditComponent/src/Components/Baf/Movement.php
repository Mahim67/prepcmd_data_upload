<?php

namespace Pondit\PonditComponent\Components\Baf;

use Illuminate\View\Component;

class Movement extends Component
{
    public $buttonName, $title, $dataList, $url;

    public function __construct(
        $buttonName = false,
        $title      = false,
        $dataList       = false,
        $url        = false
    )
    {
        $this->buttonName = $buttonName;
        $this->title      = $title;
        $this->dataList       = $dataList;
        $this->url        = $url;
    }
    
    public function render()
    {
        return view('widgets::baf.movement');
    }
}
