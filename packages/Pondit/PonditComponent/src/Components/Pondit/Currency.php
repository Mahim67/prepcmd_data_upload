<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class Currency extends Component
{
    public  $currencyClass
           ,$currencyName
           ,$currencyId
           ,$otherAttr;

    public function __construct(
        $currencyId = false,
        $currencyClass = false,
        $currencyName = false,
        $otherAttr = false)
    {
        $this->currencyId             = $currencyId;
        $this->currencyName           = $currencyName;
        $this->currencyClass          = $currencyClass;
        $this->otherAttr              = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::pondit.currency');
    }
}
