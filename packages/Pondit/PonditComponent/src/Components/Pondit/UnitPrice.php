<?php

namespace Pondit\PonditComponent\Components\Pondit;

use Illuminate\View\Component;

class UnitPrice extends Component
{
    public  $class
           ,$name
           ,$value
           ,$label
           ,$id
           ,$currencyName
           ,$unitPricePlaceHolder
           ,$otherAttr;

    public function __construct(
        $id = "estimated_price",
        $class = false,
        $label = false,
        $name = false,
        $currencyName = false,
        $value = false,
        $unitPricePlaceHolder = false,
        $otherAttr = false)
    {
        $this->id             = $id;
        $this->name           = $name;
        $this->label          = $label;
        $this->class          = $class;
        $this->value          = $value;
        $this->currencyName   = $currencyName;
        $this->unitPricePlaceHolder   = $unitPricePlaceHolder;
        $this->otherAttr      = $otherAttr;
    }
    
    public function render()
    {
        return view('widgets::pondit.unit_price');
    }
}
