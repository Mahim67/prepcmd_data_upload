<?php 
namespace Pondit\PonditComponent;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Pondit\PonditComponent\Components\Baf\AirCrftComponent;
use Pondit\PonditComponent\Components\Baf\EqptTypeComponent;

class PonditComponentServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadComponents();

        $this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'widgets');
    }

    public function register()
    {
        $this->publishers();
        $this->loadViewsFrom(__DIR__.'/Resources/components', 'widgets');
    }

    private function publishers()
    {

        $this->publishes([
            __DIR__.'/Resources/lang' => resource_path('lang/vendor/widgets'),
        ]);

        $this->publishes([
            __DIR__.'/Resources/components' => resource_path('views/vendor/pondit-components'), 
        ], 'pondit-components');

        $this->publishes([
            __DIR__.'/assets' => public_path('vendor/pondit-component/assets'),
        ], 'assets');
        
    }

    private function loadComponents()
    {
        /***
         * BAF WIDGETS
        ***/
        Blade::component('pondit-baf-prepcm-sub-report', Components\Baf\PcmSubmissionReport::class);
        Blade::component('pondit-baf-movement', Components\Baf\Movement::class);
        Blade::component('pondit-baf-auth-user-info', Components\Baf\AuthUserInfo::class);
        Blade::component('pondit-baf-office', Components\Baf\Office::class);
        Blade::component('pondit-baf-bdnumber', Components\Baf\BdNumber::class);
        Blade::component('pondit-baf-budgetcodes', Components\Baf\BudgetcodeComponent::class);
        Blade::component('pondit-baf-ranges', Components\Baf\RangeComponent::class);
        Blade::component('pondit-baf-itemcat', Components\Baf\ItemCat::class);
        Blade::component('pondit-baf-itemlist', Components\Baf\ItemList::class);
        Blade::component('pondit-qty', Components\Pondit\Quantity::class);
        Blade::component('pondit-qty-unit', Components\Pondit\QuantityUnit::class);
        Blade::component('pondit-currency', Components\Pondit\Currency::class);
        Blade::component('pondit-unit-price', Components\Pondit\UnitPrice::class);
        Blade::component('pondit-total-cost', Components\Pondit\TotalCost::class);
        Blade::component('pondit-baf-trades', Components\Baf\TradeComponent::class);
        Blade::component('pondit-baf-sub-ranges', Components\Baf\SubRangeComponent::class);
        Blade::component('pondit-baf-classes', Components\Baf\EqptClassComponent::class);
        Blade::component('pondit-baf-eqpt-type', Components\Baf\EqptTypeComponent::class);
        Blade::component('pondit-baf-air-crft', Components\Baf\AircraftComponent::class);
        Blade::component('pondit-baf-store', Components\Baf\StoreComponent::class);



         /**
         * PONDIT WIDGETS
         */
        Blade::component('pondit-excel-importer', Components\Pondit\ExcelImporter::class);
        Blade::component('pondit-attach-up', Components\Pondit\AttachmentUpload::class);


        Blade::component('pondit-fin-year', Components\Pondit\FinancialYear::class);
        Blade::component('pondit-card', Components\Card::class);
        Blade::component('pondit-form', Components\Form::class);
        Blade::component('pondit-table', Components\Table::class);
        Blade::component('pondit-datatable', Components\DataTable::class);
        Blade::component('pondit-multiselect', Components\Multiselect::class);
        Blade::component('pondit-act-btn', Components\ActionButton::class);
        Blade::component('pondit-act-link', Components\ActionLink::class);
        Blade::component('pondit-act-link-d', Components\ActionLinkDelete::class);
        Blade::component('pondit-btn', Components\Button::class);

        Blade::component('pondit-act-c', Components\ActionsCreate::class);
        Blade::component('pondit-act-i', Components\ActionsIndex::class);
        Blade::component('pondit-act-e', Components\ActionsEdit::class);
        Blade::component('pondit-act-v', Components\ActionsShow::class);
        Blade::component('pondit-act-d', Components\ActionsDelete::class);
        Blade::component('pondit-act-t', Components\ActionsTrash::class);
        Blade::component('pondit-act-u', Components\ActionsUpload::class);
        Blade::component('pondit-act-word', Components\ActionWordFile::class);
        Blade::component('pondit-act-excel', Components\ActionExcelFile::class);
        Blade::component('pondit-act-pdf', Components\ActionPdfFile::class);
        Blade::component('pondit-act-preview', Components\ActionPreviewFile::class);

        Blade::component('pondit-a', Components\ATag::class);

    }
}