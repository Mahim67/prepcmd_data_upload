@extends('pondit-limitless::layouts.master')
@push('css')
<style>
    .left-card .top-info a {
        font-size: 16px;
    }

    .left-card .card-body {
        padding: 2rem 1.25rem;

    }

    .left-card .card-footer {
        padding: 1.10rem 1.25rem;
    }
</style>
@endpush
@section('content')
@include('pondit-limitless::elements.success')
@include('pondit-limitless::elements.error')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 col-lg-4 col-sm-12">
                <div class="card text-white text-center left-card">
                    <div class="card-body bg-success">
                        <div class="top-info my-3">
                            <a class="d-block text-white" href="#">Procurement Management</a>
                        </div>
                        <a href="#" class="text-white"><i class="fas fa-pencil-alt"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-8 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-success d-flex">
                                <a href="#" class="text-white align-self-center mr-3"><i class="fas fa-coins"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="#">Budget</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-blue  d-flex ">
                                <a href="{{ route('pcm.dashboard') }}" class="text-white align-self-center mr-3"><i
                                        class="fas fa-file-contract"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="{{ route('pcm.dashboard') }}">Pre PCM</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-brown d-flex ">
                                <a href="#" class="text-white align-self-center mr-3"><i
                                        class="fas fa-id-badge"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="#">G-2-G Contract</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-indigo d-flex ">
                                <a href="#" class="text-white align-self-center mr-3"><i
                                        class="fas fa-align-justify"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="#">Indent Management</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-brown d-flex ">
                                <a href="#" class="text-white align-self-center mr-3"><i class="fas fa-tasks"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="#">Priority Management</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="card text-white text-center">
                            <div class="card-body bg-success d-flex ">
                                <a href="#" class="text-white align-self-center mr-3"><i
                                        class="fas fa-store-alt"></i></a>
                                <div class="top-info">
                                    <a class="d-block text-white" href="#">Local Purchase</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection