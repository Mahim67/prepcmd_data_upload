<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Bangladesh Air Force</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />


    <!-- App css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

    <link href="{{ asset('') }}vendor/pondit/themes/limitless/assets/login/css/style.css" rel="stylesheet"
        type="text/css">


</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 left-panel px-0">
                <div class="left-panel-top bg-success text-center text-white py-5">
                    <h2 class="login-head">
                        <sup><i class="fas fa-quote-left"></i></sup>
                        বাংলার আকাশ রাখিব মুক্ত
                        <sup> <i class="fas fa-quote-right"></i></sup>
                    </h2>
                </div>
                <div class="left-panel-img">
                    <img src="{{ asset('vendor/pondit/themes/limitless/assets/login/img/AIMS.jpg') }}" alt="">
                </div>
                <div class="bg-success text-center text-white py-3 px-2">
                    <h1>
                        Air Force Inventory Management Software (AIMS)
                    </h1>
                </div>
            </div>
            <div class="left-panel-bottom col-4 align-items-center d-flex">

                <div class="w-75 m-auto">
                    <div class="">
                        <a href="index.html">
                            <span><img src="{{ asset('vendor/pondit/themes/limitless/assets/login/img/logo.svg') }}"
                                    alt="" height="50"></span>
                        </a>
                        <p class="text-muted mb-4 mt-3">Enter your service number and password to access work space.</p>
                    </div>


                    @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ session('status') }}
                    </div>
                    @endif
                    {{-- @if (Route::has('login'))
                    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                        @auth
                        <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
                        @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                        @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                        @endauth
                    </div>
                    @endif
                </div> --}}
                
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group mb-3">
                        <label for="serviceNumber"></label>
                        <input class="form-control" type="text" id="serviceNumber" required=""
                            placeholder="Enter Service Number" required autofocus>
                        <input id="password" class="block mt-1 form-control" type="password" name="password" required
                            autocomplete="current-password" />
                    </div>
                    <div class="form-group mb-2 text-center">
                        <a href="{{url('/dashboard')}}" class="btn btn-primary btn-block" type="submit">Sign in</a>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>


</body>

</html>