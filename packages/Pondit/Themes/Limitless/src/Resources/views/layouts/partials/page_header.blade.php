
<div class="page-header page-header-light">

	<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a href="{{ url('/dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Dashboard</span>
			</div>

			<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
		</div>

		<div class="header-elements d-none">
			<div class="breadcrumb justify-content-center">
				<div class="form-group-feedback form-group-feedback-right mr-4">
					{{-- <input type="text" class="form-control form-control-lg" value="" placeholder="Search">
					<div class="form-control-feedback form-control-feedback-lg">
						<i class="icon-search4 text-muted"></i>
					</div> --}}
				</div>
				<div class="breadcrumb-elements-item dropdown p-0">
					<a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">
						<i class="fas fa-bell"></i>
						<span class="badge badge-primary badge-pill" id="procurement_unread_message"></span>
						
					</a>
				
					<div class="dropdown-menu dropdown-menu-right" id="procurement_message_details">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>