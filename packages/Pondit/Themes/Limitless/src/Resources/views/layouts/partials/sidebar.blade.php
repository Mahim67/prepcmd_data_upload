<!-- Main sidebar -->
<div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <span class="font-weight-semibold">Navigation</span>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="sidebar-user-material-body">
                <div class="card-body text-center">
                </div>

                <div class="sidebar-user-material-footer">
                    <a href="#user-profile-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle legitRipple" data-toggle="collapse" aria-expanded="true"><span>My account</span></a>
                </div>
            </div>
            @push('js')
            <script>
                $(document).ready(function() {
                    $("#my_account").click();
                });
            </script>
            @endpush



            <div class="collapse hide" id="user-profile-nav" style="">

                <ul class="nav nav-sidebar">
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="icon-user-plus"></i>
                            <span>{{ __('Profile') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="fa fa-pen"></i>
                            <span>{{ __('Update Profile') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link legitRipple">
                            <i class="icon-comment-discussion"></i>
                            <span>Pending Documents</span>
                            <span class="badge bg-teal-400 badge-pill align-self-center ml-auto">58</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <form method="POST" action="#">
                            @csrf
                            <a href="#" onclick="event.preventDefault();
        this.closest('form').submit();" class="nav-link legitRipple">
                                <i class="icon-switch2"></i>
                                <span>{{ __('Log Out') }}</span>
                            </a>
                        </form>
                    </li>
                    <li class="nav-item">

                    </li>
                </ul>
            </div>

            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">

                </ul>
                <ul class="nav nav-sidebar" data-nav-type="accordion">
                    {{-- <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li> --}}
                    <div class="check_zero">
                        <div class="card  mx-1 my-2 border-primary progressbar-card check_zero">
                            <div class="card-header header-elements-inline justify-content-end">
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                        <a class="list-icons-item" data-action="reload"></a>
                                        <a class="list-icons-item" data-action="remove"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <span class="mx-1 text-dark"> You have <a href="#" class="text-danger pending_task_count"> </a> pending
                                    documents.</span>
                            </div>
                        </div>
                    </div>
                </ul>
            </div>


        </div>
        <!-- /user menu -->

        <!-- Main navigation -->

        <div class="card card-sidebar-mobile">

            <ul class="nav nav-sidebar">
                <li class="nav-item bg-success">
                    <a href="{{ url('/') }}" class="nav-link text-light">
                        <i class="icon-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="#" class="nav-link text-light">
                        <i class="icon-upload"></i>
                        <span>Excel Upload</span>
                    </a>
                    <ul class="nav nav-group-sub bg-primary" data-submenu-title="Menu levels">
                        <li class="nav-item">
                            <a href="{{ route('armt-ranges.create') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Armt Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('supply-ranges.create') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Supply Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('trg-ranges.create') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Trg Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('med-ranges.create') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Med Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('met-ranges.create') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Met Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link text-white" onclick="return confirm('Under Development...');">
                                <i class="fas fa-arrows-alt"></i>
                                <span>C&E Range</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link text-white" onclick="return confirm('Under Development...');">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Engg Range</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu bg-success">
                    <a href="#" class="nav-link text-light">
                        <i class="icon-stack"></i>
                        <span>Budget</span>
                    </a>
                    <ul class="nav nav-group-sub bg-primary" data-submenu-title="Menu levels">
                        <li class="nav-item">
                            <a href="{{ url('budgetcodes') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Budgetcode</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('budgetcode-to-item') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Budgetcode To Item</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('budget-allotments') }}" class="nav-link text-white">
                                <i class="fas fa-arrows-alt"></i>
                                <span>Budget Allotment</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar content -->
</div>
<!-- /main sidebar -->