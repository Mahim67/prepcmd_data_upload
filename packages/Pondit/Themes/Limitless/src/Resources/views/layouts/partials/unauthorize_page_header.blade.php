<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-dark bg-success navbar-static">
	<div class="navbar-brand">
		<a href="{{ route('procurement.dashboard') }}" class="d-inline-block">
			<img src="{{ asset('') }}{{$themepath}}/global_assets/images/logo-light.svg" height="40px!important"
				{{-- Morning, Victoria! --}} alt="">
		</a>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
					<i class="icon-paragraph-justify3"></i>
				</a>
			</li>
		</ul>

		{{-- <span class="navbar-text ml-md-3">
					<span class="badge badge-mark border-orange-300 mr-2"></span>
					Wellcome  <span id="userName"></span>
				</span> --}}

		<ul class="navbar-nav ml-md-auto">
			{{-- <li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-make-group mr-2"></i>
						Connect
					</a>

					<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-body p-2">
							<div class="row no-gutters">
								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-github4 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Item</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-dropbox text-blue-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Procurement</div>
									</a>
								</div>
								
								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-dribbble3 text-pink-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">In Bound</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-google-drive text-success-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Out Bound</div>
									</a>
								</div>

								<div class="col-12 col-sm-4">
									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-twitter text-info-400 icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Ware House</div>
									</a>

									<a href="#" class="d-block text-default text-center ripple-dark rounded p-3">
										<i class="icon-youtube text-danger icon-2x"></i>
										<div class="font-size-sm font-weight-semibold text-uppercase mt-2">Demand</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</li> --}}

			{{-- <li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<i class="icon-pulse2 mr-2"></i>
						Activity
					</a>
					
					<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-header">
							<span class="font-size-sm line-height-sm text-uppercase font-weight-semibold">Latest activity</span>
							<a href="#" class="text-default"><i class="icon-search4 font-size-base"></i></a>
						</div> --}}

			{{-- <div class="dropdown-content-body dropdown-scrollable">
							<ul class="media-list">
								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-success-400 rounded-round btn-icon"><i class="icon-mention"></i></a>
									</div>

									<div class="media-body">
										<a href="#"></a>08 X line Avionices spares of K-8W ac proc from CATIC, China
										<div class="font-size-sm text-muted mt-1">4 minutes ago</div>
									</div>
								</li> --}}

			{{-- <li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-pink-400 rounded-round btn-icon"><i class="icon-paperplane"></i></a>
									</div>
									
									<div class="media-body">
										Special offers have been sent to subscribed users by <a href="#">Donna Gordon</a>
										<div class="font-size-sm text-muted mt-1">36 minutes ago</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-blue rounded-round btn-icon"><i class="icon-plus3"></i></a>
									</div>
									
									<div class="media-body">
										<a href="#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch in <span class="font-weight-semibold">Limitless</span> repository
										<div class="font-size-sm text-muted mt-1">2 hours ago</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-purple-300 rounded-round btn-icon"><i class="icon-truck"></i></a>
									</div>
									
									<div class="media-body">
										Shipping cost to the Netherlands has been reduced, database updated
										<div class="font-size-sm text-muted mt-1">Feb 8, 11:30</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-warning-400 rounded-round btn-icon"><i class="icon-comment"></i></a>
									</div>
									
									<div class="media-body">
										New review received on <a href="#">Server side integration</a> services
										<div class="font-size-sm text-muted mt-1">Feb 2, 10:20</div>
									</div>
								</li>

								<li class="media">
									<div class="mr-3">
										<a href="#" class="btn bg-teal-400 rounded-round btn-icon"><i class="icon-spinner11"></i></a>
									</div>
									
									<div class="media-body">
										<strong>January, 2018</strong> - 1320 new users, 3284 orders, $49,390 revenue
										<div class="font-size-sm text-muted mt-1">Feb 1, 05:46</div>
									</div>
								</li> --}}
			{{-- </ul>
						</div>

						<div class="dropdown-content-footer bg-light">
							<a href="#" class="font-size-sm line-height-sm text-uppercase font-weight-semibold text-grey mr-auto">All activity</a>
							<div>
								<a href="#" class="text-grey" data-popup="tooltip" title="Clear list"><i class="icon-checkmark3"></i></a>
								<a href="#" class="text-grey ml-2" data-popup="tooltip" title="Settings"><i class="icon-gear"></i></a>
							</div>
						</div>
					</div>
				</li> --}}

			<li class="nav-item">
				<a href="#" onclick="logout()" class="navbar-nav-link">
					<i class="icon-switch2"></i>
					<span class="d-md-none ml-2">Logout</span>
				</a>
			</li>
		</ul>
	</div>
</div>

@push('js')
<script>
	if(!auth.getAuthenticatedUser()){
            getUserByToken();
        }else{
			const user = auth.getAuthenticatedUser();
			// console.log(user);
		if (typeof  user.active_appointment != "undefined" ||  user.active_appointment != null){
					if( (user.active_appointment =="NCO") || (user.active_appointment =="JCO") ){
						check_active_appointment = "Concern Group"
					}
					else{
						check_active_appointment = user.active_appointment
					}
				}
            document.querySelector('#userName').textContent = user.name
            document.querySelector('#userRank').textContent = user.short_rank ? (`[${user.short_rank}]`) : '';
            document.querySelector('#userAppointment').textContent = user.active_appointment ? (`[${check_active_appointment}]`) : '';
            document.querySelector('#userOffice').textContent = user.office_name ? (`[${user.office_name}]`) : '';
            document.querySelector('#profile').src = user.profile_photo_url ? user.profile_photo_url  : `{{ asset('') }}vendor/pondit/themes/limitless/global_assets/images/user.png`;
        }
        function logout() {
			auth.destroyToken();
			destroySessionToken();
            if (!auth.isAuthenticated()) {
                window.location.href = BASE_URL;
            }
		}
		
		    function destroySessionToken() {
        		axios.post(BASE_URL+'oauth/destroy-token')
					.then(response => {
						console.log(response.data)
						// if(response.data.access_token){
						//     auth.setToken(response.data.access_token, response.data.expires_in + Date.now())
						// }
					})
					// .then(()=> {
					//     if(auth.isAuthenticated()){
					//         window.location.href = BASE_URL+'procurement/dashboard';
					//     }
					// });
    			}


        function getUserByToken(){
            axios.post(`user`)
                .then(function (response, data) {
                    // console.log(response);
                    if(response.data == 'Unauthenticated'){
                        auth.destroyToken();
                        if(!auth.isAuthenticated()){
                            window.location.href = BASE_URL;
                        }
                    }else{
						let user = response.data.data;
						console.log(user);
                        auth.setAuthenticatedUser(user);
						let check_active_appointment;
						if (typeof  user.active_appointment != "undefined" ||  user.active_appointment != null){
							if( (user.active_appointment =="NCO") || (user.active_appointment =="JCO") ){
								check_active_appointment = "Concern Group"
							}
							else{
								check_active_appointment = user.active_appointment
							}
						}
						document.querySelector('#userName').textContent = user.name
						document.querySelector('#userRank').textContent = user.short_rank ? (`[${user.short_rank}]`) : '';
						document.querySelector('#userAppointment').textContent = user.active_appointment ? (`[${check_active_appointment}]`) : '';
						document.querySelector('#userOffice').textContent = user.office_name ? (`[${user.office_name}]`) : '';
						document.querySelector('#profile').src = user.profile_photo_url ? user.profile_photo_url  : `{{ asset('') }}vendor/pondit/themes/limitless/global_assets/images/user.png`;
						
                    }
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
</script>
@endpush
<!-- /main navbar -->