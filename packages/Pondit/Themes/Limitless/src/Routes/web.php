<?php

use Illuminate\Support\Facades\Route;
use Pondit\Baf\PrePCM\Models\PrePcmDemand;

// Route::group(['middleware' => ['web']], function () {
//     Route::get('dashboard', function () {
//         return view('pondit-limitless::layouts.aims-dashboard');
//     })->name('aims.dashboard');
// });
Route::group(['middleware' => ['web','auth:sanctum', 'verified']], function () {
    Route::get('fwd-link-refresh', function () {
        PrePcmDemand::where('parent_office_base_id', 209)->update([
            'is_fwd_frm_raisor' => 0,
            'ON_OFFICE_AUTHORIZOR_DESK' => 0,
            'IS_FWD_FRM_OFFICE_AUTHORIZOR' => 0,
            'ON_BASE_CONSOLIDATOR_DESK' => 0,
            'IS_FWD_FRM_BASE_CONSOLIDATOR' => 0,
            'ON_BASE_AUTHORIZOR_DESK' => 0,
            'IS_FWD_FRM_BASE_AUTHORIZOR' => 0,
            'ON_HQ_CONSOLIDATOR_DESK' => 0,
            'IS_FWD_FRM_HQ_CONSOLIDATOR' => 0,
            'ON_OPI_DTE_DESK' => 0
        ]);
        dd('success');
    });
});