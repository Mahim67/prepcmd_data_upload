(function($) {
    $(document).ready(function() {

        let aa = $('meta[name="csrf-token"]').attr('content')
        // load data 
        load_data();

        // eventlistener
        $(document).on('click', '.fa-pen', editData);

        $(document).on('click', '#form-submit', updateExistingItem);

        $(document).on('click', '.add-item', showAddItemForm);

        $(document).on('click', '#form-submit-add', sendNewItemToDatabase);

        $(document).on('click', '.fa-trash', deleteData);

        $(document).on('hidden.bs.modal', function() { resetForm(); });

        $(document).on('click', '.fa-eye', singleItemDetails);

        $(document).on('click', '.ui-menu-item', function(){
            $myGrid     = $("#jqGrid");
            let cells   = $myGrid.find('tr:nth-child(2)').find('td:visible');
            if(cells?.length<=11){ $myGrid.setGridWidth($('.main-col').width());}
            $myGrid.trigger('reloadGrid');
        });


    });


})(jQuery)




function editData() {
    $('#form-submit-add').attr('id', 'form-submit');

    let
        rowId   = $(this).parent().parent().attr('id'),
        data    = $("#jqGrid").jqGrid("getRowData", rowId);

    $('#fin_year').val(data.fin_year);
    $('#total_amount').val(data.total_amount);
    $('#no_of_supplimentary_budget').val(data.no_of_supplimentary_budget);
    $('#id').val(data.id);

    $('#userForm').modal('show');
}




function updateExistingItem() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/update/${getFormData().id}`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'PUT',
        success : function(res) {
            console.log(res)
            $('#jqGrid').trigger('reloadGrid');
            $('#userForm').modal('hide');
            // $(document).find(`#${getFormData().id}`).replaceWith(renderSingleRow(getFormData()));
            location.reload();
        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function deleteData() {
    let
    rowId   = $(this).parent().parent().attr('id');
    data    = $("#jqGrid").jqGrid("getRowData", rowId);

    if (confirm('Are You want To delete It ?')) {
        $.ajax({
            url     : `${window.origin}/masterdata/api/budgets/delete/${rowId}`,
            datatype: 'json',
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : { 'id': rowId },
            type    : 'POST',
            success : function(res) {
                console.log(res)
                $('#jqGrid').jqGrid('delRowData', rowId);
                $('#jqGrid').trigger('reloadGrid');
            },
            error   : function(xhr, status, error) {
                $('#jqGrid').restoreRow(rowId);
                $('#jqGrid').trigger('reloadGrid');
                console.log(xhr.responseText);
            }
        });
    }
}


function showAddItemForm() {
    $('#userForm').modal('show');
    $('#form-submit').attr('id', 'form-submit-add');
}



function sendNewItemToDatabase() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/store`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'POST',
        success : function(res) {
            console.log(res)
            location.reload();
            $('#userForm').modal('hide');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });

}


function singleItemDetails() {
    let rowId = $(this).parent().parent().attr('id');
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/show/${rowId}`,
        datatype: 'json',
        data    : { "id": rowId },
        type    : 'GET',
        success : function(res) {
            console.log(res)

            var row = `
                <tr>
                    <th>Fin Year : </th>
                    <td>${(res.fin_year == null)?'':res.fin_year}</td>
                </tr>
                <tr>
                    <th>Total Amount : </th>
                    <td>${(res.total_amount == null)?'':res.total_amount}</td>
                </tr>
                <tr>
                    <th>No Of Supplimentary Budget : </th>
                    <td>${(res.no_of_supplimentary_budget == null)?'':res.no_of_supplimentary_budget}</td>
                </tr>
                `;

            $('#user-details-table').html(row);
            $('#userDetails').modal('show');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}





function resetForm() {
    $('#form')[0].reset();
}




function renderSingleRow(v = null) {
    let single_row = `<tr id="${v.id}" class="jqgrow ui-row-ltr" role="row" tabindex="-1">
                        <td role="gridcell">${v.fin_year}</td>
                        <td role="gridcell">${v.total_amount}</td>
                        <td role="gridcell">0</td>
                        <td role="gridcell" >
                        <i class="fa fa-eye  btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>
                        </td>
                    </tr>`;

    return single_row;
}


function getFormData() {
    let obj = {};
    obj.id = $('#id').val();
    obj._token = $('meta[name="csrf-token"]').attr('content');
    obj.fin_year = $('#fin_year').val();
    obj.total_amount = $('#total_amount').val();
    obj.no_of_supplimentary_budget = $('#no_of_supplimentary_budget').val();
    // console.log(obj)
    return obj;
}



function load_data() {

    $.ajax({
        url         : `${window.origin}/masterdata/api/budgets`,
        method      : 'get',
        dataType    : 'json',
        beforeSend  : function(data) {
            // console.log('loading ...');
        },
        success     : function(res) {
            // console.log(res)
            render_data(res);
        },
        error       : function(error) {
            console.log("ERROR :" + error);
        },
        complete    : function() {

            $("#jqGrid").jqGrid('filterToolbar', {
                stringResult: true,
                searchOnEnter: false,
                defaultSearch: "cn"
            });
            
        }
    });

    // render_data(null);

}




function render_data(res) {
    $('#jqGrid').jqGrid({
        datatype    : "local",
        // url         : `${window.origin}/masterdata/api/budgets`,
        // mtype       : 'GET',
        data        : res,
        editurl     : 'clientArray',
        colModel    : [
            { label: 'Fin Year', name: 'fin_year', align: 'left', firstsortorder: 'asc',formatter: 'string', editable: true, editrules: { required: true } },
            { label: 'Total Amount', name: 'total_amount', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true } },
            { label: 'No Of Supplimentary Budget', name: 'no_of_supplimentary_budget', formatter: 'string', firstsortorder: 'asc', editable: true ,editrules: { required: true }},
            { label: 'Created By', name: 'created_by', align: 'center', formatter: 'string', firstsortorder: 'asc', editable: true },
            { label: 'Updated By', name: 'updated_by', align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true },
            { label: 'Created At', name: 'created_at', align: 'center', formatter: 'date',formatoptions: {srcformat: 'U', newformat:'d/m/Y'}, firstsortorder: 'asc', editable: true },
            { label: 'Updated At', name: 'updated_at', align: 'center', formatter: 'date', formatoptions: {srcformat: 'U', newformat:'d/m/Y'}, firstsortorder: 'asc', editable: true },
            { label: 'Id', name: 'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: true ,hidedlg: true},
            { label: 'Action', name: 'action', firstsortorder: 'asc' ,search:false,},
        ],
        rowNum      : 5000,
        rownumbers  : true,
        // jsonReader  : { 
        //     root: "rows",
        //     page: "page",
        //     total: "total",
        //     records: "records",
        //     repeatitems: false,
        //     id:0
        // },
        // rowList     :[30,100,1000],
        pager       : '#jqGridPager',
        pgbuttons   : true,
        toppager    : false,
        // caption 			: `Budget Codes`,
        height      : '500',
        // width: '1020',
        // responsive: true,
        emptyRecords: "No records to display!",
        headertitles: false,
        gridview    : true,
        footerrow   : true,
        userDataOnFooter: true,
        viewrecords : true,
        sortname    : 'sequence_number',
        sortable    :true,       
        sortorder   : 'desc', 
        subGrid     : false,
        subGridModel: [],
        multiselect : false,
        grouping    : true,
        shrinkToFit : true,
        autowidth   : true,
        gridComplete: function() {

            // total Users count
            var td, users, countUsers,
                $grid = $('#jqGrid'),
                actions = $('td[aria-describedby="jqGrid_action"]');

            for (var i = 0; i < actions.length; i++) {
                td = actions[i];
                $(td).html(`<i class="fa fa-eye btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i><i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i><i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>`);
                $(td).attr('class', 'text-center');
            }
            $('#jqGrid_action').addClass('text-center');

            $grid.jqGrid('footerData', 'set', { 'action': `<span type="button" class="btn btn-sm bg-light add-item fa fa-plus my-0"> ADD</span>`, });

            users = $grid.jqGrid('getCol', 'id'),

            total_users = [];
            for (var i = 0; i < users.length; i++) total_users[users[i]] = (users[i]);
            countUsers = Object.keys(users).length;
            $grid.jqGrid('footerData', 'set', {
                'id': `Users : ${countUsers}`,
            });


            $('.ui-jqgrid').css('overflow-x', 'auto');

    
            let $groupingHeaders = $(this).find(">tbody>tr.jqgroup");
            if($groupingHeaders?.length>0){
                let rowsEl = $(this).find(">tbody>tr.jqgrow"); 
                if(rowsEl.length>0)
                {
                    let cellLen = $(rowsEl[0]).find('td').length;
                    if(cellLen<=11){
                        $(document).find('table').removeClass('custom');
                        $(document).find('table').addClass('custom');
                    }else{
                        $(document).find('table').removeClass('custom');
                    }
                }
            } 
            
        },
        colMenu     : true,
        styleUI     : 'Bootstrap4',
        iconSet     : "fontAwesome",
        beforeRequest: function() {
            console.log('sending...')
        },
        onSelectRow: function(id) {
            if (id != null) {
                if (id == typeof Number) console.log(id);

            }
        },
        // onPaging: function() {

        //     console.log($("#jqGrid").getGridParam('url'))
        //     $("#jqGrid").setGridParam({datatype:'json'});
        //     $("#jqGrid").trigger("reloadGrid");
        // }, 
        // loadComplete: function () {       	
        // 	$("#jqGrid").setGridParam({datatype:'local'});
        // 	// $("#jqGrid").trigger("reloadGrid");
        // }, 	 
        

    });

}
