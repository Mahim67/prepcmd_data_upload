
(function($) {
    $(document).ready(function() {

        // load data 
        load_data();

        // eventlistener
        // $(document).on('click', '.fa-pen', editData);

        $(document).on('click', '#form-submit', updateExistingItem);

        $(document).on('click', '.add-item', showAddItemForm);

        $(document).on('click', '#form-submit-add', sendNewItemToDatabase);

        $(document).on('click', '.fa-trash', deleteData);

        $(document).on('hidden.bs.modal', function() { resetForm(); });

        $(document).on('click', '.details', singleItemDetails);

        $(document).on('click', '.ui-menu-item', function(){
            $myGrid     = $("#jqGrid");
            let cells   = $myGrid.find('tr:nth-child(2)').find('td:visible');
            if(cells?.length<=11){ $myGrid.setGridWidth($('.main-col').width());}
            $myGrid.trigger('reloadGrid');
        });

        // $("#jqGrid").jqGrid().on("editRow", function() {  
        //     console.log('ok')
        // });
        $(document).on('click','#confirmation-modal .confirm',function(){
            $('#confirmation-modal').modal('hide');
        });

        $(document).on('click','#confirmation-modal .deny',function(){
            $('#confirmation-modal').modal('hide');
        });

    });


})(jQuery)




function editData() {
    $('#form-submit-add').attr('id', 'form-submit');

    let
        rowId   = $(this).parent().parent().attr('id'),
        data    = $("#jqGrid").jqGrid("getRowData", rowId);

    $('#fin_year').val(data.fin_year);
    $('#total_amount').val(data.total_amount);
    $('#no_of_supplimentary_budget').val(data.no_of_supplimentary_budget);
    $('#id').val(data.id);

    $('#userForm').modal('show');
}




function updateExistingItem() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/update/${getFormData().id}`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'PUT',
        success : function(res) {
            console.log(res)
            $('#jqGrid').trigger('reloadGrid');
            $('#userForm').modal('hide');
            location.reload();
        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function deleteData() {
    let
    rowId   = $(this).parent().parent().attr('id');
    data    = $("#jqGrid").jqGrid("getRowData", rowId);

    if (confirm('Are You want To delete It ?')) {
        $.ajax({
            url     : `${window.origin}/masterdata/api/budgets/delete/${data?.id}`,
            datatype: 'json',
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : { 'id': data?.id },
            type    : 'POST',
            success : function(res) {
                console.log(res)
                $('#jqGrid').jqGrid('delRowData', rowId);
                $('#jqGrid').trigger('reloadGrid');
            },
            error   : function(xhr, status, error) {
                $('#jqGrid').restoreRow(rowId);
                $('#jqGrid').trigger('reloadGrid');
                console.log(xhr.responseText);
            }
        });
    }
}


function showAddItemForm() {
    $('#userForm').modal('show');
    $('#form-submit').attr('id', 'form-submit-add');
}



function sendNewItemToDatabase() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/store`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'POST',
        success : function(res) {
            console.log(res)
            location.reload();
            $('#userForm').modal('hide');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });

}


function singleItemDetails() {

    let rowId = $(this).parent().parent().attr('id');
        data  = $("#jqGrid").jqGrid("getRowData", rowId)
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/show/${data?.id}`,
        datatype: 'json',
        data    : { "id": data?.id },
        type    : 'GET',
        success : function(res) {
            console.log(res)

            var row = `
                <tr>
                    <th>Fin Year : </th>
                    <td>${(res.fin_year == null)?'':res.fin_year}</td>
                </tr>
                <tr>
                    <th>Total Amount : </th>
                    <td>${(res.total_amount == null)?'':res.total_amount}</td>
                </tr>
                <tr>
                    <th>No Of Supplimentary Budget : </th>
                    <td>${(res.no_of_supplimentary_budget == null)?'':res.no_of_supplimentary_budget}</td>
                </tr>
                `;

            $('#user-details-table').html(row);
            $('#userDetails').modal('show');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}





function resetForm() {
    $('#form')[0].reset();
}




function renderSingleRow(v = null) {
    let single_row = `<tr id="${v.id}" class="jqgrow ui-row-ltr" role="row" tabindex="-1">
                        <td role="gridcell">${v.fin_year}</td>
                        <td role="gridcell">${v.total_amount}</td>
                        <td role="gridcell">0</td>
                        <td role="gridcell" >
                        <i class="fa fa-eye btn bg-success btn-circle btn-circle-sm pt-2 details" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>
                        </td>
                    </tr>`;

    return single_row;
}


function getFormData() {
    let obj = {};
    obj.id = $('#id').val();
    obj._token = $('meta[name="csrf-token"]').attr('content');
    obj.fin_year = $('#fin_year').val();
    obj.total_amount = $('#total_amount').val();
    obj.no_of_supplimentary_budget = $('#no_of_supplimentary_budget').val();
    // console.log(obj)
    return obj;
}

function load_data()
{
    localStorage.removeItem('prevId');
    localStorage.removeItem('_token');
    let token = $(document).find('input[name="_token"]').val();
   
    let lastSelection;
    $("#jqGrid").jqGrid({
        url         : `${window.origin}/masterdata/api/budgets`,
        editurl     : `${window.origin}/masterdata/api/budgets/update/`,
        datatype    : "json",
        colModel    : [
            { label: 'Fin Year', name: 'fin_year', index:'fin_year', align: 'left', firstsortorder: 'asc',formatter: 'string', editable: true, edittype: 'select',  key: false,
                editoptions:
                {
                size: 1,
                dataUrl: `${window.origin}/masterdata/api/budgets/fin-years`,
                buildSelect : function (options) {
                        let selectoptions = JSON.parse(options);
                        let s = '<select class="select2">';
                        if( $.isPlainObject(selectoptions)) {
                            for( var key in selectoptions) {
                                s += '<option value="'+key+'" >'+selectoptions[key]+'</option>';
                            }
                        }else{
                            for( var key in selectoptions) {
                                s += '<option value="'+selectoptions[key]+'" >'+selectoptions[key]+'</option>';
                            }
                        }
                        s += '</select>';
                        return s;
                    }
                }, 
                editrules: { required: true }, 
            },
            { label: 'Total Amount', name: 'total_amount', index:'total_amount', align: 'left', firstsortorder: 'asc', formatter: 'integer', summaryTpl : "{0}", summaryType: "sum", editable: true, editrules: { required: true }, },
            { label: 'No Of Supplimentary Budget', name: 'no_of_supplimentary_budget', index:'no_of_supplimentary_budget', formatter: 'integer', summaryTpl : "{0}", summaryType: "sum", firstsortorder: 'asc', editable: true ,editrules: { required: true },},
            { label: 'Created By', name: 'created_by', index:'created_by', align: 'center', formatter: 'string', firstsortorder: 'asc', editable: true, },
            { label: 'Updated By', name: 'updated_by', index:'updated_by',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 1', name: 'updated_by1', index:'updated_by1',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 2', name: 'updated_by2', index:'updated_by2',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 3', name: 'updated_by3', index:'updated_by3',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 4', name: 'updated_by4', index:'updated_by4',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 5', name: 'updated_by5', index:'updated_by5',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 6', name: 'updated_by6', index:'updated_by6',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 7', name: 'updated_by7', index:'updated_by7',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 8', name: 'updated_by8', index:'updated_by8',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Created At', name: 'created_at',index:'created_at', align: 'center', formatter: 'string', firstsortorder: 'asc', editable: true,
                editoptions: {
                dataInit: function (element) {
                        $(element).datetimepicker({
                            todayHighlight  : true,
                            format          : 'yyyy-mm-dd hh:ii:00',
                            startDate       : new Date('2020-01-01'),
                            endDate         : new Date(),
                            autoclose       : true,
                            todayBtn        : true,
                            showOn      : 'focus'
                        });
                    }
                }
            },
            { label: 'Updated At', name: 'updated_at',index:'updated_at', align: 'center', formatter: 'string',firstsortorder: 'asc', editable: true,
                editoptions: {
                dataInit: function (element) {
                        $(element).datetimepicker({
                            todayHighlight  : true,
                            format          : 'yyyy-mm-dd hh:ii:00',
                            startDate       : new Date('2020-01-01'),
                            endDate         : new Date(),
                            autoclose       : true,
                            todayBtn        : true,
                            showOn      : 'focus'
                        });
                    }
                }
            },
            { label: 'Id', name: 'id', index:'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: false ,hidedlg: true},
            { label: 'Action', name: 'action', align: 'center', search:false, formatter:statusFormatter},
        ],
        // width               : '1020',
        height              : '500',
        pager               : true,
        pager               : '#jqGridPager',
        pageinput           : true,
        rowNum              : 1000,
        rownumbers          : false,
        rowList             : [100, 500,1000, 1500, 5000],
        sortname            : 'id',
        sortorder           : "asc",
        viewrecords         : true,
        autowidth           : false,
        shrinkToFit         : true,
        emptyrecords        : "No records to view",
        loadonce            : false,
        loadtext            : "Loading...",
        colMenu             : true,
        styleUI             : 'Bootstrap4',
        iconSet             : "fontAwesome",
        multiselect         : false,
        autoencode          : true,
        ignoreCase          : true,
        gridview            : true,
        footerrow			: true,
		userDataOnFooter 	: true,
        grouping            : false, 
        groupingView 		: {
			groupField 	   : ['fin_year'],	
			groupColumnShow: [true],
			groupText	   : [
				"Finace Year : <b>{0}</b>", 
			],
			groupOrder	  : ["asc", "asc","asc"],
			groupSummary  : [false,false,false,true],
			groupCollapse : false,	   	
		},
        onSelectRow         : function(id,state,event) {   
            if(event.target.classList.contains('fa-pen'))       
            {   
                let 
                edituri = $("#jqGrid").getGridParam('editurl'),
                rowdata = $("#jqGrid").jqGrid("getRowData", id),
                _token  = $(document).find('input[name="_token"]').val(),
                finalEditUri='',
                prevToken   ='',
                prevId      = 0;

                if(id && id!==lastSelection){
                    if(prevId = localStorage.getItem('prevId')){
                        prevToken    = localStorage.getItem('_token');
                        finalEditUri = edituri.replace(prevId,rowdata.id).replace(prevToken,_token);
                        localStorage.setItem('prevId',rowdata.id);
                        localStorage.setItem('_token',_token);
                    }else{
                        finalEditUri = `${edituri}${rowdata.id}?_token=${_token}`;
                        localStorage.setItem('prevId',rowdata.id);
                        localStorage.setItem('_token',_token);
                    }

                    $("#jqGrid").setGridParam({editurl: finalEditUri});
                    // console.log(finalEditUri)
                    setTimeout(() => {
                        $(document).find("select[role='select']").select2();
                    }, 1000);

                    let $modal = $(document).find('#confirmation-modal');
                    // console.log('js grid')

                    $('#jqGrid').jqGrid('restoreRow',lastSelection);
                    $('#jqGrid').jqGrid('editRow',id,{
						keys    : true,
						onEnter : function(rowid, options, event) {
                            // $modal.modal('show');                         
							if (confirm("Are you sure to save this row data ?")) {
								$(this).jqGrid("saveRow", rowid, options );
                                $('#jqGrid').trigger('reloadGrid');
							}

                            // $modal.modal('hide');
						}
					});

                    lastSelection=id;
                }


            }
        },
        onPaging        : function() {
            $("#jqGrid").setGridParam({loadonce:false});
        },
        loadComplete    : function (res) {  
            // $("#jqGrid").setGridParam({loadonce:true});
        },
        gridComplete    : function(){
            let $grid = $('#jqGrid');
            let no_of_supplimentary_budget = $grid.jqGrid('getCol', 'no_of_supplimentary_budget');
            let total_amount    = $grid.jqGrid('getCol', 'total_amount');
            let total_budgets   = 0;
            let total_amounts   = 0;

            $.each(no_of_supplimentary_budget,function (i,v) {
				total_budgets += parseFloat(v);
			});

            $.each(total_amount,function (i,v) {
				total_amounts += parseFloat(v);
			});

            $grid.jqGrid('footerData', 'set', { 
				'no_of_supplimentary_budget'	: total_budgets,
			});

            $grid.jqGrid('footerData', 'set', { 
				'total_amount'	: total_amounts,
			});

            $('.ui-jqgrid').css('overflow-x', 'auto');
            // $('#jqGrid').jqGrid('setGridWidth', '1020');
        },
        
    }).navGrid('#jqGridPager', { search: true,edit: false, add: true, del: false, refresh: true,pdf: true,
        beforeRefresh: function(){
            console.log("beforeRefresh");
        },
        afterRefresh: function(){
            $("#jqGrid").setGridParam({
                datatype:"json",
                loadonce:false
            }).trigger('reloadGrid');
        }
    },
    {},
    {
        url:`${window.origin}/masterdata/api/budgets/store?_token=${token}`,
    },
    {},
    {multipleSearch:true, multipleGroup:true, showQuery: true});	

    $("#jqGrid").jqGrid('inlineNav',"#jqGrid");


    function statusFormatter(cellvalue, options, rowObject){
        return `<i class="fa fa-eye details btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer;"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>`; 
    }

    $('#jqGrid').jqGrid('filterToolbar',{ stringResult: true, searchOnEnter: false, enableClear: false, defaultSearch: "cn", });
}



let 
currentDate = new Date(),
date 	= currentDate.getDate(),
month 	= currentDate.getMonth(),
year 	= currentDate.getFullYear(),
hours	= currentDate.getHours(),
minutes = currentDate.getMinutes(),
seconds	= currentDate.getSeconds(),
ampm 	= (hours >= 12) ? "pm" : "am";

let dateString = date + "-" +(month + 1) + "-" + year;


// $(document).on("click", "#export_as_excel", function(){
//     if(confirm("Are You Want To Export It into Excel Sheet ?"))
//     {
//         $("#jqGrid").jqGrid("exportToExcel",{
//             includeLabels 		: true,
//             includeGroupHeader  : true,
//             includeFooter		: true,
//             fileName 			: 'BUDGET_REPORT_'+dateString+'.xlsx',
//             maxlength 			: 40  
//         });
//     }
// });

$(document).on("click","#export_as_pdf",function(){

    reloadToExport(true);

    if(confirm("Are You Want To Export It as a PDF ?"))
    {
        $("#jqGrid").jqGrid("exportToPdf",{
            title 				: 'BUDGET_REPORT',
            orientation			: 'landscape',
            pageSize			: 'A4',
            onBeforeExport 		: function( doc ) {
                //you can set custom pdf styles here like this
                // alignment: 'center'
                doc.styles.tableHeader.fillColor = '#dfeffc';
                doc.styles.tableHeader.color = '#2e6e9e';
                doc.styles.tableBody.fontSize = 6.3;
                doc.styles.tableHeader.fontSize = 9;
                doc.styles.tableFooter.fontSize = 10;
                doc.styles.tableFooter.bold = true;
                doc.header = function(currentPage, pageCount) { 
                    return { text: `Date Time: ${dateString} ${hours}:${minutes}:${seconds} ${ampm}`, alignment: 'right',fontSize:10, margin: [ 0, 5, 10,5 ]};
                };
                doc.footer = function(currentPage, pageCount,pageSize) { 
                    return [
                        { text: `${currentPage.toString() + ' of ' + pageCount}`, alignment: 'center',fontSize:10 }
                      ];
                };
                doc.pageMargins	=[ 10, 30, 10, 30 ];
                doc.defaultStyle= {
                    columnGap: 20
                };
            },
            description			: '',
            customSettings		: null,
            download			: 'download',
            includeLabels 		: true,
            includeGroupHeader 	: true,
            includeFooter		: true,
            fileName 			: 'BUDGET_REPORT_'+dateString+".pdf",
        });

    }
});

function reloadToExport(loadonce=false)
{
    $("#jqGrid").setGridParam({loadonce}).trigger('reloadGrid');
}





// function load_data() {

//     $.ajax({
//         url         : `${window.origin}/masterdata/api/budgets`,
//         method      : 'get',
//         dataType    : 'json',
//         beforeSend  : function(data) {
//             // console.log('loading ...');
//         },
//         success     : function(res) {
//             // console.log(res)
//             render_data(res);
//         },
//         error       : function(error) {
//             console.log("ERROR :" + error);
//         },
//         complete    : function() {

//             $("#jqGrid").jqGrid('filterToolbar', {
//                 stringResult: true,
//                 searchOnEnter: false,
//                 defaultSearch: "cn",
//                 beforeSearch: function(r){
//                     console.log(r)
//                     console.log($("#jqGrid").getGridParam('url'))
//                 },
//                 afterSearch: function(){
//                 }
//             });
            
//         }
//     });

// }

 


// function render_data(res) {
//     // $('#jqGrid').jqGrid({
//     //     datatype    : "json",
//     //     url         : `${window.origin}/masterdata/api/budgets`,
//     //     mtype       : 'GET',
//     //     editurl     : 'clientArray',
//         // colModel    : [
//         //     { label: 'Fin Year', name: 'fin_year', align: 'left', firstsortorder: 'asc',formatter: 'string', editable: true, editrules: { required: true } },
//         //     { label: 'Total Amount', name: 'total_amount', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true } },
//         //     { label: 'No Of Supplimentary Budget', name: 'no_of_supplimentary_budget', formatter: 'string', firstsortorder: 'asc', editable: true ,editrules: { required: true }},
//         //     { label: 'Created By', name: 'created_by', align: 'center', formatter: 'string', firstsortorder: 'asc', editable: true },
//         //     { label: 'Updated By', name: 'updated_by', align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true },
//         //     { label: 'Created At', name: 'created_at', align: 'center', formatter: 'date',formatoptions: {srcformat: 'U', newformat:'d/m/Y'}, firstsortorder: 'asc', editable: true },
//         //     { label: 'Updated At', name: 'updated_at', align: 'center', formatter: 'date', formatoptions: {srcformat: 'U', newformat:'d/m/Y'}, firstsortorder: 'asc', editable: true },
//         //     { label: 'Id', name: 'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: true ,hidedlg: true},
//         // ],
//     //     // jsonReader: { root: "d.rows", page: "d.page", total: "d.total",
//     //     //       records: "d.records" },
//     //     loadonce    :false,
//     //     autoencode  : true,
//     //     // rowNum      :1000,
//     //     rownumbers  : true,
//    	//     rowList     :[100,500,1000,5000,10000],
//     //     pager       : '#jqGridPager',
//     //     height      : '500',
//     //     width       : '1020',
//     //     viewrecords : true,
//     //     sortname    : 'id',    
//     //     sortorder   : 'desc', 
//     //     gridComplete: function() {

           
            
//     //     },
//     //     colMenu     : true,
//     //     styleUI     : 'Bootstrap4',
//     //     iconSet     : "fontAwesome",
//     //     beforeRequest: function() {
//     //         console.log('sending...')
//     //     },
//         // onSelectRow: function(id) {
//         //     if (id != null) {
//         //         if (id == typeof Number) console.log(id);

//         //     }
//         // },
//         // onPaging: function() {

//         //     // console.log($("#jqGrid").getGridParam('editurl'))
//         //     // $("#jqGrid").setGridParam({datatype:'json'});
//         //     // $("#jqGrid").trigger("reloadGrid");
//         // }, 
//         // loadComplete: function (res) {  
//         //     console.log(res)     	
//         // 	// $("#jqGrid").setGridParam({datatype:'local'});
//         // 	// $("#jqGrid").trigger("reloadGrid");
//         // }, 	 
        

//     // });




// }
