
(function($) {
    $(document).ready(function() {

        // load data 
        load_data();

        // eventlistener
        // $(document).on('click', '.fa-pen', editData);

        $(document).on('click', '#form-submit', updateExistingItem);

        $(document).on('click', '.add-item', showAddItemForm);

        $(document).on('click', '#form-submit-add', sendNewItemToDatabase);

        $(document).on('click', '.fa-trash', deleteData);

        $(document).on('hidden.bs.modal', function() { resetForm(); });

        $(document).on('click', '.details', singleItemDetails);

        $(document).on('click', '.ui-menu-item', function(){
            $myGrid     = $("#jqGrid");
            let cells   = $myGrid.find('tr:nth-child(2)').find('td:visible');
            if(cells?.length<=11){ $myGrid.setGridWidth($('.main-col').width());}
            $myGrid.trigger('reloadGrid');
        });

        // $("#jqGrid").jqGrid().on("editRow", function() {  
        //     console.log('ok')
        // });
        $(document).on('click','#confirmation-modal .confirm',function(){
            $('#confirmation-modal').modal('hide');
        });

        $(document).on('click','#confirmation-modal .deny',function(){
            $('#confirmation-modal').modal('hide');
        });

    });


})(jQuery)




function editData() {
    $('#form-submit-add').attr('id', 'form-submit');

    let
        rowId   = $(this).parent().parent().attr('id'),
        data    = $("#jqGrid").jqGrid("getRowData", rowId);

    $('#fin_year').val(data.fin_year);
    $('#total_amount').val(data.total_amount);
    $('#no_of_supplimentary_budget').val(data.no_of_supplimentary_budget);
    $('#id').val(data.id);

    $('#userForm').modal('show');
}




function updateExistingItem() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/update/${getFormData().id}`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'PUT',
        success : function(res) {
            console.log(res)
            $('#jqGrid').trigger('reloadGrid');
            $('#userForm').modal('hide');
            location.reload();
        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function deleteData() {
    let
    rowId   = $(this).parent().parent().attr('id');
    data    = $("#jqGrid").jqGrid("getRowData", rowId);

    if (confirm('Are You want To delete It ?')) {
        $.ajax({
            url     : `${window.origin}/masterdata/api/budgets/delete/${data?.id}`,
            datatype: 'json',
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data    : { 'id': data?.id },
            type    : 'POST',
            success : function(res) {
                console.log(res)
                $('#jqGrid').jqGrid('delRowData', rowId);
                $('#jqGrid').trigger('reloadGrid');
            },
            error   : function(xhr, status, error) {
                $('#jqGrid').restoreRow(rowId);
                $('#jqGrid').trigger('reloadGrid');
                console.log(xhr.responseText);
            }
        });
    }
}


function showAddItemForm() {
    $('#userForm').modal('show');
    $('#form-submit').attr('id', 'form-submit-add');
}



function sendNewItemToDatabase() {
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/store`,
        datatype: 'json',
        headers : {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data    : getFormData(),
        type    : 'POST',
        success : function(res) {
            console.log(res)
            location.reload();
            $('#userForm').modal('hide');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });

}


function singleItemDetails() {

    let rowId = $(this).parent().parent().attr('id');
        data  = $("#jqGrid").jqGrid("getRowData", rowId)
    $.ajax({
        url     : `${window.origin}/masterdata/api/budgets/show/${data?.id}`,
        datatype: 'json',
        data    : { "id": data?.id },
        type    : 'GET',
        success : function(res) {
            console.log(res)

            var row = `
                <tr>
                    <th>Fin Year : </th>
                    <td>${(res.fin_year == null)?'':res.fin_year}</td>
                </tr>
                <tr>
                    <th>Total Amount : </th>
                    <td>${(res.total_amount == null)?'':res.total_amount}</td>
                </tr>
                <tr>
                    <th>No Of Supplimentary Budget : </th>
                    <td>${(res.no_of_supplimentary_budget == null)?'':res.no_of_supplimentary_budget}</td>
                </tr>
                `;

            $('#user-details-table').html(row);
            $('#userDetails').modal('show');

        },
        error   : function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}





function resetForm() {
    $('#form')[0].reset();
}




function renderSingleRow(v = null) {
    let single_row = `<tr id="${v.id}" class="jqgrow ui-row-ltr" role="row" tabindex="-1">
                        <td role="gridcell">${v.fin_year}</td>
                        <td role="gridcell">${v.total_amount}</td>
                        <td role="gridcell">0</td>
                        <td role="gridcell" >
                        <i class="fa fa-eye btn bg-success btn-circle btn-circle-sm pt-2 details" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>
                        </td>
                    </tr>`;

    return single_row;
}


function getFormData() {
    let obj = {};
    obj.id = $('#id').val();
    obj._token = $('meta[name="csrf-token"]').attr('content');
    obj.fin_year = $('#fin_year').val();
    obj.total_amount = $('#total_amount').val();
    obj.no_of_supplimentary_budget = $('#no_of_supplimentary_budget').val();
    // console.log(obj)
    return obj;
}

function load_data()
{
    
    localStorage.removeItem('prevId');
    localStorage.removeItem('_token');
    let token = $(document).find('input[name="_token"]').val();
   
    let prevCellVal = { cellId: undefined, value: undefined };
    let lastSelection;
    $("#jqGrid").jqGrid({
        // url         : `${window.origin}/masterdata/api/budgets`,
        url         : `http://127.0.0.1:8000/api/prepcm-supply-ranges`,
        editurl     : `${window.origin}/masterdata/api/budgets/update/`,
        datatype    : "json",
        colModel    : [
            
            { label: 'Part No', name: 'part_no', index:'part_no', formatter: 'string', firstsortorder: 'asc', editable: true ,editrules: { required: true } },
            { label: 'Name Of Eqpt', name: 'name_of_eqpt', index:'name_of_eqpt', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true }, },
            { label: 'name_of_user', name: 'name_of_user', index:'name_of_user',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            
            { label: 'ph_baseunit_supply_range', name: 'ph_baseunit_supply_range', index:'ph_baseunit_supply_range',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'ph_user_supply_range', name: 'ph_user_supply_range', index:'ph_user_supply_range',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            { label: 'Demo 8', name: 'updated_by8', index:'updated_by8',align: 'center',formatter: 'string', firstsortorder: 'asc', editable: true ,},
            // { label: 'Created At', name: 'created_at',index:'created_at', align: 'center', formatter: 'string', firstsortorder: 'asc', editable: true,
            //     editoptions: {
            //         dataInit: function (element) {
            //             $(element).datetimepicker({
            //                 todayHighlight  : true,
            //                 format          : 'yyyy-mm-dd hh:ii:00',
            //                 startDate       : new Date('2020-01-01'),
            //                 endDate         : new Date(),
            //                 autoclose       : true,
            //                 todayBtn        : true,
            //                 showOn      : 'focus'
            //             });
            //         }
            //     },
            // },
            // { label: 'Updated At', name: 'updated_at',index:'updated_at', align: 'center', formatter: 'string',firstsortorder: 'asc', editable: true,
            //     editoptions: {
            //         dataInit: function (element) {
            //             $(element).datetimepicker({
            //                 todayHighlight  : true,
            //                 format          : 'yyyy-mm-dd hh:ii:00',
            //                 startDate       : new Date('2020-01-01'),
            //                 endDate         : new Date(),
            //                 autoclose       : true,
            //                 todayBtn        : true,
            //                 showOn      : 'focus'
            //             });
            //         }
            //     },
            // },
            { label: 'Id', name: 'id', index:'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: false ,hidedlg: true},
            { label: 'Action', name: 'action', align: 'center', search:false, formatter:statusFormatter},
        ],
        // width               : '1020',
        height              : '500',
        pager               : true,
        pager               : '#jqGridPager',
        pageinput           : true,
        rowNum              : 1000,
        rownumbers          : false,
        rowList             : [100, 500,1000, 1500, 5000],
        sortname            : 'id',
        sortorder           : "asc",
        viewrecords         : true,
        autowidth           : false,
        shrinkToFit         : true,
        emptyrecords        : "No records to view",
        loadonce            : false,
        loadtext            : "Loading...",
        colMenu             : true,
        styleUI             : 'Bootstrap4',
        iconSet             : "fontAwesome",
        multiselect         : false,
        autoencode          : true,
        ignoreCase          : true,
        gridview            : true,
        footerrow			: true,
		userDataOnFooter 	: true,
        grouping            : false, 
        groupingView 		: {
			groupField 	   : ['name_of_eqpt'],	
			groupColumnShow: [true],
			groupText	   : [
				"Name of Equipment : <b>{0}</b>", 
			],
			groupOrder	  : ["asc", "asc","asc"],
			groupSummary  : [false,false,false,true],
			groupCollapse : false,	   	
		},
        rowattr: function (rd) {
            return {"data-id": rd?.id};
        },
        onSelectRow         : function(id,state,event) {   
            if(event.target.classList.contains('fa-pen'))       
            {   
                let 
                edituri = $("#jqGrid").getGridParam('editurl'),
                rowdata = $("#jqGrid").jqGrid("getRowData", id),
                _token  = $(document).find('input[name="_token"]').val(),
                finalEditUri='',
                prevToken   ='',
                prevId      = 0;

                if(id && id!==lastSelection){
                    if(prevId = localStorage.getItem('prevId')){
                        prevToken    = localStorage.getItem('_token');
                        finalEditUri = edituri.replace(prevId,rowdata.id).replace(prevToken,_token);
                        localStorage.setItem('prevId',rowdata.id);
                        localStorage.setItem('_token',_token);
                    }else{
                        finalEditUri = `${edituri}${rowdata.id}?_token=${_token}`;
                        localStorage.setItem('prevId',rowdata.id);
                        localStorage.setItem('_token',_token);
                    }

                    $("#jqGrid").setGridParam({editurl: finalEditUri});
                    // console.log(finalEditUri)
                    setTimeout(() => {
                        $(document).find("select[role='select']").select2();
                    }, 1000);

                    let $modal = $(document).find('#confirmation-modal');
                    // console.log('js grid')

                    $('#jqGrid').jqGrid('restoreRow',lastSelection);
                    $('#jqGrid').jqGrid('editRow',id,{
						keys    : true,
						onEnter : function(rowid, options, event) {
                            // $modal.modal('show');                         
							if (confirm("Are you sure to save this row data ?")) {
								$(this).jqGrid("saveRow", rowid, options );
                                $('#jqGrid').trigger('reloadGrid');
							}

                            // $modal.modal('hide');
						}
					});

                    lastSelection=id;
                }


            }
        },
        onCellSelect        : function(rowId, iCol, content, event){

            let 
                replaceContent  = content.replace(/,/, ''),
                dataID          = $(event.target).parent().attr('data-id');

            
            let selector1 = $(event.target);
            let selector2 = $(event.target).parent().parent().find('td:last-child');

            if(selector1.attr('aria-describedby')!="jqGrid_action" && selector2.attr('aria-describedby')!="jqGrid_action"){
                fetch(`https://jsonplaceholder.typicode.com/users`)
                .then(res => res.json())
                .then(resText => {
                    let content = '';
                    resText.forEach(el => {
                        content +=`<tr><td>${el?.name}</td><td>${el?.phone}</td><td>${el?.website}</td></tr>`;
                    });
                    
                    $('#cell_content').html(content);
                    $('#cell_modal').modal('show');
                })
                .catch(err => console.error(err));
            }
            
            
            // $('#cell_content').text(`cell value: ${replaceContent} and ID : ${dataID}`);
        },
        onPaging        : function() {
            $("#jqGrid").setGridParam({loadonce:false});
        },
        loadComplete    : function (res) {  
            console.log(res)
        },
        gridComplete    : function(){
            let $grid = $('#jqGrid');
            let no_of_supplimentary_budget = $grid.jqGrid('getCol', 'estimated_unit_price');
            let total_amount    = $grid.jqGrid('getCol', 'total_amount');
            let total_budgets   = 0;
            let total_amounts   = 0;

            $.each(no_of_supplimentary_budget,function (i,v) {
				total_budgets += parseFloat(v);
			});

            $.each(total_amount,function (i,v) {
				total_amounts += parseFloat(v);
			});

            $grid.jqGrid('footerData', 'set', { 
				'no_of_supplimentary_budget'	: total_budgets,
			});

            $grid.jqGrid('footerData', 'set', { 
				'total_amount'	: total_amounts,
			});

            $('.ui-jqgrid').css('overflow-x', 'auto');
            // $('#jqGrid').jqGrid('setGridWidth', '1020');

            let myGrid = this;
            $('td[rowspan="1"]', myGrid).each(function () {
                let spans = $('td[rowspanid="' + this.id + '"]', myGrid).length + 1;
                if (spans > 1) {
                    $(this).attr('rowspan', spans);
                }
            });

        },
        
    }).navGrid('#jqGridPager', { search: true,edit: false, add: true, del: false, refresh: true,pdf: true,
        beforeRefresh: function(){
            console.log("beforeRefresh");
        },
        afterRefresh: function(){
            $("#jqGrid").setGridParam({
                datatype:"json",
                loadonce:false
            }).trigger('reloadGrid');
        }
    },
    {},
    {
        url:`${window.origin}/masterdata/api/budgets/store?_token=${token}`,
    },
    {},
    {multipleSearch:true, multipleGroup:true, showQuery: true});	

    $("#jqGrid").jqGrid('inlineNav',"#jqGrid");


    function statusFormatter(cellvalue, options, rowObject){
        return `<i class="fa fa-eye details btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer;"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>`; 
    }

    function mergeById(rowId, val, rawObject, cm, rdata) {
        return "id='fin_year" +rowId + "'";
    }


    function mergeCell(rowId, val, rawObject, cm, rdata) {
        let result;
        // if (prevCellVal.value == val) {
        //     result = ' style="display: none" rowspanid="' + prevCellVal.cellId + '"';
        // }
        // else {
        //     let cellId  = this.id + '_row_' + rowId + '_' + cm.name;
        //     result      = ' rowspan="1" id="' + cellId + '"';
        //     prevCellVal = { cellId: cellId, value: val };
        // }

        // return result;
    }

    $("#jqGrid").jqGrid('setGroupHeaders', {
        useColSpanStyle: true, 
        groupHeaders:[
          {startColumnName: '', numberOfColumns: 3, titleText: '<em>Group 1</em>'},
        ]
    });

    $('#jqGrid').jqGrid('filterToolbar',{ stringResult: true, searchOnEnter: false, enableClear: false, defaultSearch: "cn", });
}



let 
currentDate = new Date(),
date 	= currentDate.getDate(),
month 	= currentDate.getMonth(),
year 	= currentDate.getFullYear(),
hours	= currentDate.getHours(),
minutes = currentDate.getMinutes(),
seconds	= currentDate.getSeconds(),
ampm 	= (hours >= 12) ? "pm" : "am";

let dateString = date + "-" +(month + 1) + "-" + year;


$(document).on("click","#export_as_pdf",function(){

    reloadToExport(true);

    if(confirm("Are You Want To Export It as a PDF ?"))
    {
        $("#jqGrid").jqGrid("exportToPdf",{
            title 				: 'BUDGET_REPORT',
            orientation			: 'landscape',
            pageSize			: 'A4',
            onBeforeExport 		: function( doc ) {
                //you can set custom pdf styles here like this
                // alignment: 'center'
                doc.styles.tableHeader.fillColor = '#dfeffc';
                doc.styles.tableHeader.color = '#2e6e9e';
                doc.styles.tableBody.fontSize = 6.3;
                doc.styles.tableHeader.fontSize = 9;
                doc.styles.tableFooter.fontSize = 10;
                doc.styles.tableFooter.bold = true;
                doc.header = function(currentPage, pageCount) { 
                    return { text: `Date Time: ${dateString} ${hours}:${minutes}:${seconds} ${ampm}`, alignment: 'right',fontSize:10, margin: [ 0, 5, 10,5 ]};
                };
                doc.footer = function(currentPage, pageCount,pageSize) { 
                    return [
                        { text: `${currentPage.toString() + ' of ' + pageCount}`, alignment: 'center',fontSize:10 }
                      ];
                };
                doc.pageMargins	=[ 10, 30, 10, 30 ];
                doc.defaultStyle= {
                    columnGap: 20
                };
            },
            description			: '',
            customSettings		: null,
            download			: 'download',
            includeLabels 		: true,
            includeGroupHeader 	: true,
            includeFooter		: true,
            fileName 			: 'BUDGET_REPORT_'+dateString+".pdf",
        });

    }
});

function reloadToExport(loadonce=false)
{
    $("#jqGrid").setGridParam({loadonce}).trigger('reloadGrid');
}


