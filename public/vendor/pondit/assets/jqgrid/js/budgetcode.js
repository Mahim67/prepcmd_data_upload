(function($) {
    $(document).ready(function() {

        let aa = $('meta[name="csrf-token"]').attr('content')
        console.log(aa)
            // load data 
        load_data();


        // eventlistener
        $(document).on('click', '.fa-pen', editData);

        $(document).on('click', '#form-submit', updateExistingItem);

        $(document).on('click', '.add-item', showAddItemForm);

        $(document).on('click', '#form-submit-add', sendNewItemToDatabase);

        $(document).on('click', '.fa-trash', deleteData);

        $(document).on('hidden.bs.modal', function() { resetForm(); });

        $(document).on('click', '.fa-eye', singleItemDetails);


    });


})(jQuery)




function editData() {
    $('#form-submit-add').attr('id', 'form-submit');

    let
        rowId = $(this).parent().parent().attr('id'),
        data = $("#jqGrid").jqGrid("getRowData", rowId);
    console.log(data)

    $('#range').val(data.range);
    $('#fin_years').val(data.introduced_fin_year);
    $('#sequence_number').val(data.sequence_number);
    $('#new_code').val(data.new_code);
    $('#old_code').val(data.old_code);
    $('#activity_type').val(data.activity_type);
    $('#budget_head').val(data.budget_head);
    $('#description').val(data.description);
    $('#id').val(data.id);

    $('#userForm').modal('show');
}




function updateExistingItem() {
    $.ajax({
        url: `http://aims-budget.test//masterdata/api/budgetcodes/update/${getFormData().id}`,
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: getFormData(),
        type: 'PUT',
        success: function(res) {
            console.log(res)
            $('#jqGrid').trigger('reloadGrid');
            $('#userForm').modal('hide');
            $(document).find(`#${getFormData().id}`).replaceWith(renderSingleRow(getFormData()));
        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function deleteData() {
    let
        rowId = $(this).parent().parent().attr('id');
    data = $("#jqGrid").jqGrid("getRowData", rowId);
    // console.log(data, rowId)

    if (confirm('Are You want To delete It ?')) {
        $.ajax({
            url: `http://aims-budget.test/masterdata/api/budgetcodes/delete/${rowId}`,
            datatype: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: { 'id': rowId },
            type: 'POST',
            success: function(res) {
                console.log(res)
                $('#jqGrid').jqGrid('delRowData', rowId);
                $('#jqGrid').trigger('reloadGrid');
            },
            error: function(xhr, status, error) {
                $('#jqGrid').restoreRow(rowId);
                $('#jqGrid').trigger('reloadGrid');
                console.log(xhr.responseText);
            }
        });
    }
}


function showAddItemForm() {
    $('#userForm').modal('show');
    $('#form-submit').attr('id', 'form-submit-add');
}



function sendNewItemToDatabase() {
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgetcodes/store`,
        datatype: 'json',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: getFormData(),
        type: 'POST',
        success: function(res) {
            console.log(res)
            location.reload();
            $('#userForm').modal('hide');
        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');

            // $('#userForm').modal('hide');
            var error = JSON.parse(xhr.responseText);
            alert(error.errors.new_code[0]);

            console.log(error.errors.new_code[0]);
        }
    });
}


function singleItemDetails() {
    let rowId = $(this).parent().parent().attr('id');
    console.log(rowId)
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgetcodes/show/${rowId}`,
        datatype: 'json',
        data: { "id": rowId },
        type: 'GET',
        success: function(res) {
            console.log(res)

            var row = `
                <tr>
                    <th>Range : </th>
                    <td>${getRange(res)}</td>
                </tr>
                <tr>
                    <th>Financial Year : </th>
                    <td>${(res.introduced_fin_year == null)?'':res.introduced_fin_year}</td>
                </tr>
                <tr>
                    <th>Sequence Number : </th>
                    <td>${(res.sequence_number == null)?'':res.sequence_number}</td>
                </tr>
                <tr>
                    <th>New Code : </th>
                    <td>${(res.new_code == null)?'':res.new_code}</td>
                </tr>
                <tr>
                    <th>Old Code : </th>
                    <td>${(res.old_code == null)?'':res.old_code}</td>
                </tr> 
                <tr>
                    <th>Budget Head : </th>
                    <td>${(res.budget_head == null)?'':res.budget_head}</td>
                </tr>               
                <tr>
                    <th>Description : </th>
                    <td>${(res.description == null)?'':res.description}</td>
                </tr>
                `;

            $('#user-details-table').html(row);
            $('#userDetails').modal('show');

        },
        error: function(xhr, status, error) {
            $('#jqGrid').restoreRow(getFormData().id);
            $('#jqGrid').trigger('reloadGrid');
            console.log(xhr.responseText);
        }
    });
}



function getRange(res) {
    let data = '';
    $.each(res.range, function(index, range) {
        data += `<p class="m-0 p-0">${index+1 +'.' + range.title}</p>`;
    })
    return data;
}

function resetForm() {
    $('#form')[0].reset();
}




function renderSingleRow(v = null) {
    let single_row = `<tr id="${v.id}" class="jqgrow ui-row-ltr" role="row" tabindex="-1">
                        <td role="gridcell">${v.new_code}</td>
                        <td role="gridcell">${v.old_code}</td>
                        <td role="gridcell">${v.sequence_number}</td>
                        <td role="gridcell">${v.activity_type}</td>
                        <td role="gridcell" >
                        <i class="fa fa-eye  btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>
                        </td>
                    </tr>`;

    return single_row;
}


function getFormData() {
    let obj = {};
    obj.id = $('#id').val();
    obj._token = $('meta[name="csrf-token"]').attr('content');
    obj.range = $('.range').val();
    obj.introduced_fin_year = $('#fin_years').val();
    obj.sequence_number = $('#sequence_number').val();
    obj.new_code = $('#new_code').val();
    obj.old_code = $('#old_code').val();
    obj.activity_type = $('#activity_type').val();
    obj.budget_head = $('#budget_head').val();
    obj.description = $('#description').val();

    console.log($("input[type='checkbox']").val());
    return obj;
}



function load_data() {
    $.ajax({
        url: `http://aims-budget.test/masterdata/api/budgetcodes`,
        method: 'get',
        dataType: 'json',
        beforeSend: function(data) {
            // console.log('loading ...');
        },
        success: function(res) {
            // console.log(res)
            render_data(res);
        },
        error: function(error) {
            console.log("ERROR :" + error);
        },
        complete: function() {

            $("#jqGrid").jqGrid('filterToolbar', {
                stringResult: true,
                searchOnEnter: false,
                defaultSearch: "cn"
            });

        }
    });
}




function render_data(res) {
    console.log(res)
    $('#jqGrid').jqGrid({
        datatype: "local",
        data: res,
        editurl: 'clientArray',
        colModel: [
            { label: 'New Code', name: 'new_code', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true } },
            { label: 'Old Code', name: 'old_code', align: 'left', firstsortorder: 'asc', formatter: 'string', editable: true, editrules: { required: true } },
            { label: 'Sequence Number', name: 'sequence_number', formatter: 'string', firstsortorder: 'asc', editable: true },
            { label: 'Activity Type', name: 'activity_type', formatter: 'string', align: 'left', firstsortorder: 'asc', editable: true },
            { label: 'Id', name: 'id', key: true, hidden: true, formatter: 'string', align: 'left', firstsortorder: 'asc', editable: true },
            { label: 'Action', name: 'action', class: "text-center", firstsortorder: 'asc' },
        ],
        rowNum: 5000,
        rownumbers: true,
        pager: '#jqGridPager',
        pgbuttons: true,
        toppager: false,
        // caption: `Budget Codes`,
        height: '500',
        width: '1000',
        headertitles: false,
        gridview: true,
        footerrow: true,
        userDataOnFooter: true,
        viewrecords: true,
        sortname: 'sequence_number',
        subGrid: false,
        subGridModel: [],
        responsive: true,
        multiselect: false,
        grouping: true,
        gridComplete: function() {

            // total Users count
            var td, users, countUsers,
                $grid = $('#jqGrid'),
                actions = $('td[aria-describedby="jqGrid_action"]');

            for (var i = 0; i < actions.length; i++) {
                td = actions[i];
                $(td).html(`<i class="fa fa-eye  btn bg-success btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-pen btn bg-primary btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i> <i class="fa fa-trash btn bg-danger btn-circle btn-circle-sm pt-2" style="cursor: pointer"></i>`);
                $(td).attr('class', 'text-center');
            }

            $grid.jqGrid('footerData', 'set', { 'action': `<span class="btn btn-sm btn-light add-item"> + ADD</span>`, });

            users = $grid.jqGrid('getCol', 'id'),

                total_users = [];

            for (var i = 0; i < users.length; i++) {
                total_users[users[i]] = (users[i]);
            }

            countUsers = Object.keys(users).length;
            $grid.jqGrid('footerData', 'set', {
                'id': `Users : ${countUsers}`,
            });


            $('.ui-jqgrid').css('overflow-x', 'auto');


        },
        colMenu: true,
        styleUI: 'Bootstrap4',
        iconSet: "fontAwesome",
        beforeRequest: function() {
            // console.log('sending...')
        },
        onSelectRow: function(id) {
            if (id != null) {
                if (id == typeof Number) console.log(id);

            }
        },

    });

}