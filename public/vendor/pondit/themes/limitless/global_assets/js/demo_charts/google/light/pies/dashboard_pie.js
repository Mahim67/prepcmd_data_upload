var _googleDonutBasic = function(block,pcm_sum = {}) {
        if (typeof google == 'undefined') {
            console.warn('Warning - Google Charts library is not loaded.');
            return;
        }
		// console.log(block);
        // Initialize chart
        google.charts.load('current', {
            callback: function () {

                // Draw chart
                drawDonut(block);

                // Resize on sidebar width change
                var sidebarToggle = document.querySelector('.sidebar-control');
                sidebarToggle && sidebarToggle.addEventListener('click', drawDonut);

                // Resize on window resize
                var resizeDonutBasic;
                window.addEventListener('resize', function() {
                    clearTimeout(resizeDonutBasic);
                    resizeDonutBasic = setTimeout(function () {
                        drawDonut(block);
                    }, 200);
                });
            },
            packages: ['corechart']
        });

        // Chart settings
        function drawDonut() {

            // Define charts element
            var donut_chart_element = document.getElementById('google-donut');

			// Data
			let new_array = [];
			let color_array = [];
			new_array.push(['Task', 'Hours per Day'])
            if (block.includes("g_2_g")) {
                if (pcm_sum.g_2_g) { 
                    new_array.push(['G-2-G Contract', pcm_sum.g_2_g]);
                }
                else {
                    new_array.push(['G-2-G Contract',     0]);
                }
				color_array.push("#2ec7c9")
			}
            if (block.includes("indent")) {
                if (pcm_sum.indent) { 
                    new_array.push(['Indent Management', pcm_sum.indent]);
                }
                else {
                    new_array.push(['Indent Management',     0]);
                }
				color_array.push("#006a4e")
			}
            if (block.includes("priority")) {
                if (pcm_sum.priority) { 
                    new_array.push(['Priority Procurement', pcm_sum.priority]);
                } else {
                    new_array.push(['Priority Procurement',     0]);
                }
				color_array.push("#805f4c")
			}
            if (block.includes("local")) {
                if (pcm_sum.local) { 
                    new_array.push(['Local Purchase', pcm_sum.local]);
                } else {
                    new_array.push(['Local Purchase',     0]);
                }
				color_array.push("#071c75")
			}


            var data = google.visualization.arrayToDataTable(new_array);

            // Options
            var options_donut = {
                fontName: 'Roboto',
                pieHole: 0.55,
                height: 300,
                width: 500,
                backgroundColor: 'transparent',
                colors:color_array,
                chartArea: {
                    left: 50,
                    width: '90%',
                    height: '90%'
                }
            };
            
            // Instantiate and draw our chart, passing in some options.
            var donut = new google.visualization.PieChart(donut_chart_element);
            donut.draw(data, options_donut);
        }
	};