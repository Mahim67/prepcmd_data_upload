<?php

return [

    // translations ==== financial years
    "fin year" => "Financial Year",

    // translations ==== action tooltip
    "list"           => "List",
    "create"         => "Create view",
    "view"           => "View",
    "edit"           => "Edit",
    "remove"         => "Remove",
    "upload"         => "Upload",

    // attachments table
    "ser no"             =>  "Serial No",
    "original name"      =>  "Original Name",
    "user define name"   =>  "User Define Name",
    "type"               =>  "Type",
    "path"               =>  "Path",
    "file"               =>  "File",
    "sequence number"    =>  "Sequence Number",
    "action"             =>  "Action",
    "attachments"        =>  "Attachments",
    "file upload"        =>  "File Upload",
    // Pondit
    "qty"                =>  "Quantity",
    // Baf 
    "Office Name"        =>  "Office Name", 
    "Service Number"     =>  "Service Number", 
    "budgetcode"         =>  "Budget Code", 
    "budgetcode"         =>  "Budget Code", 
    "range"              =>  "Range", 
    "itemCat"            =>  "Item Category", 
    "itemList"            =>  "Item List", 
    
];