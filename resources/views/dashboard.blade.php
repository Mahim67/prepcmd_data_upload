@extends('pondit-limitless::layouts.master')


@section('content')
<x-pondit-card title="Script">
    <div class="row">
        <div class="col-3 bg-primary p-3 mr-2">
            <p >In Budget To Item Table. Set Item Id</p>
            <a href="{{ url('/update-item-id') }}" class="btn btn-dark">Update</a>
        </div>
        <div class="col-4 bg-primary p-3 mr-2">
            <p>In Suply Range Item Table . Set Item Id</p>
            <a href="{{ url('/update-supply-data-item-id') }}" class="btn btn-dark">Update</a>
        </div>
        <div class="col-4 bg-primary p-3">
            <p>In Suply Range Item Table . Set BudgetCode Id</p>
            <a href="{{ url('/update-supply-data-budgetcode-id') }}" class="btn btn-dark">Update</a>
        </div>
    </div>
</x-pondit-card>

@endsection