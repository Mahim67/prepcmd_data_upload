<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }} btn btn-outline bg-success btn-icon btn-sm text-success border-success border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title="{{ $tooltip }}" data-original-title="{{ $tooltip }}">
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>