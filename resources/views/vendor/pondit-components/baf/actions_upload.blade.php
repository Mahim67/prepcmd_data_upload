<a href="{{ $url }}" id="{{ $id }}" class="{{ $class }}btn btn-outline bg-info btn-icon text-info btn-sm border-info border-2 rounded-round legitRipple mr-1"  data-popup="tooltip" title= @lang($tooltip) data-original-title= @lang($tooltip) >
    <i class="fas fa-{{ $icon }}"></i>
    {{ $title }}
</a>
