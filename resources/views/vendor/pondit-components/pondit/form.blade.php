<form action="{{ $action }}" method="{{ $method }}" id="{{ $id }}" class="{{ $class }}" enctype="multipart/form-data">
@csrf
{{ $slot }}
</form>

@push('js')
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/select2/select2.min.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/select2/form_select2.js"></script>
@endpush