
<div class='form-group row {{$topClass}}'>
    <label for="{{$id}}" class="col-sm-2 col-form-label {{$labelClass}}">{{$label}}</label>
    <div class='col-sm-10 {{$selectDivClass}}'>
        <select name="{{$name}}" id="{{$id}}" class="form-control multiselect-select-all-filtering {{$selectClass}}"
            multiple="multiple" data-fouc>
            @if (!empty($selectPlaceholder))
            <option>{{ $selectPlaceholder }}</option>
            @endif
           
            @if (empty($data))
                <option>No Data</option>
            @else
            @foreach ($data as $key=>$item)
            <option value="">{{$item}}</option>
            @endforeach
            @endif

        </select>
    </div>
</div>

@push('js')

<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/multiselect2/bootstrap_multiselect.js"></script>
<script src="{{ asset("") }}vendor/pondit-component/assets/limitless/multiselect2/form_multiselect.js"></script>

@endpush