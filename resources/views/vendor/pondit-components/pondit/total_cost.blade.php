<div class="row">
    <div class="col-md-5">
        {{ $estimatedPrice }}
    </div>
    <div class="col-md-4">
        {{ $qty }}
    </div>
    <div class="col-md-3">
        <div class="form-group">
            {{-- <label class="col-form-label">Total Cost</label> --}}
            <input type="text" name="{{$name}}" id="{{$id}}" value="{{ $value }}" class="form-control calculate-value mt-3" readonly placeholder="{{ $totalCostPlaceholder }}">
        </div>
    </div>
</div>

@push('js')
<script>
    $(document).ready(function() {

$('.calculate-value').keyup(function()
{
    let price   = Number($('input[id="estimated_price"]').val());
    let qty     = Number($('input[id="qty"]').val());
    total       = price * qty;
    $('#total_cost').val(total);
});

});
</script>
@endpush